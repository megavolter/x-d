message (">>> Tests executables")

#TEST::MAIN
include_directories(${XD_INC_DIR})
add_definitions(-DXD_WIN)
add_executable(test_main 
	${XD_DIR}/Tests/Main/main.cpp
)

ADD_MODULE(BASE)
ADD_MODULE(GFX)
ADD_MODULE(GFX_DECODER)
ADD_MODULE(RDL)
ADD_MODULE(OS)

#TEST::LIBS
set(CMAKE_SYSTEM_NAME Windows)
set(XD_INSTALL_DIR 
	${XD_DIR}/Tests/Render/bin/
)

option(MINGW ON)
include_directories(${XD_INC_DIR})
add_definitions(-DXD_WIN)
remove_definitions(-fpermissive -dfpermissive)
add_executable(test_libs 
	${XD_DIR}/Tests/Render/halo.cpp 
	${MODULES_SRC}
	${MODULES_INC}
)
target_link_libraries(test_libs ${MODULES_LIB})

set(CMAKE_INSTALL_PREFIX bin)
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

set(CMAKE_SYSTEM_NAME Windows)
set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG}")
set(XD_INSTALL_DIR 
	${XD_DIR}/Tests/Render/bin/
)
option(MINGW ON)
include_directories(${XD_INC_DIR} ${MODULES_DIR})
add_definitions(-DXD_WIN)
add_executable(test_render 
	${XD_DIR}/Tests/Render/render.cpp 
	${XD_SRC} 
	${XD_INC}
	${XD_BASE_INC}
	${XD_BASE_SRC}
	${XD_OS_SRC}
	${XD_OS_INC}
	${XD_GFX_INC}
	${XD_GFX_SRC}
	${XD_RDL_INC}
	${XD_RDL_SRC}
	${XD_GFX_DECODER_INC}
	${XD_GFX_DECODER_SRC}
)
install (TARGETS test_main test_libs test_render RUNTIME DESTINATION bin)
target_link_libraries(test_render ${XD_GFX_LIB} ${XD_GFX_DLIB})
