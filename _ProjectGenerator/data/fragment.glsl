varying vec2 v_tex;
uniform sampler2D s_0;

void main()
{
   gl_FragColor = texture2D(s_0, v_tex);
}