attribute vec3 a_pos;
attribute vec2 a_tex;

varying vec2 v_tex;
uniform mat4 _camera;

void main()
{
   v_tex = a_tex;
   gl_Position = _camera * vec4(a_pos, 1.0);
}