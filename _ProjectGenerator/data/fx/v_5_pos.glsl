﻿attribute vec3 a_pos;
attribute vec2 a_tex;

varying vec2 v_tex;
uniform mat4 u_vwp;

void main()
{
	v_tex = a_tex;
	gl_Position = u_vwp * vec4(a_pos, 1.0);
}
