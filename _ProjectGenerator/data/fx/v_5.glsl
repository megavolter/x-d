attribute vec3 a_pos;
attribute vec2 a_tex;

varying vec2 v_tex;

void main()
{
	v_tex = a_tex;
	gl_Position = vec4(a_pos, 1.0);
}