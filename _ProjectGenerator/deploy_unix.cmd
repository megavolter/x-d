echo Unix install

cd ../../

cmake "./xd/_ProjectGenerator/" -G "MinGW Makefiles" -DPROJECTS="IntriguePuzzle" -DPLATFORM_NAME="WIN" -DARK_TYPE="x64" -DCMAKE_TOOLCHAIN_FILE="./xd/_ProjectGenerator/mingw64_toolchain.cmake" 

xcopy "xd\_ProjectGenerator\data" /E .\data
xcopy "xd\_ProjectGenerator\glew32.dll"

TIMEOUT /T 100

exit 0