echo Eclipse install

cd ../../

cmake "./xd/CMAKE/" -G "Eclipse CDT4 - MinGW Makefiles" -DNevosoft=ON -DCMAKE_TOOLCHAIN_FILE="./xd/_ProjectGenerator/mingw64_toolchain.cmake" -DPLATFORM_NAME="WIN" -DARK_TYPE="x64"

xcopy xd\_ProjectGenerator\data .\data
xcopy xd\_ProjectGenerator\glew32.dll

TIMEOUT /T 100

exit 0