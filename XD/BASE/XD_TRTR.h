#ifndef _XD_TRTR_H_INCLUDED
#define _XD_TRTR_H_INCLUDED
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Autor: UCKUHT
// Date: 2013-05-19
// Desc: TRansformaToR. Float math based.
//============================================================================//
#include "BASE/TYPE.h"
#include "BASE/CODE.h"
#include "BASE/XD_F44.h"

namespace nXD{

struct XD_F4;
struct TransformF
{
	TransformF();
	TransformF(fl4 _scale);

	void fClear();
	TransformF& fPositionSetA(fl4 _x, fl4 _y, fl4 _z = 0);
	TransformF& fPositionSetA(const F4& _pos);

	TransformF& fRotationSetA(fl4 _x, fl4 _y, fl4 _z);
	TransformF& fRotationSetA(fl4 _z);
	TransformF& fRotationSetA(const F4& _rot);

	TransformF& fScaleSetA(fl4 _x, fl4 _y, fl4 _z = 0);
	TransformF& fScaleSetA(const F4& _scale);

	TransformF& fCenterSetA(fl4 _x, fl4 _y, fl4 _z = 0);
	TransformF& fCenterSetA(const F4& _center);

	TransformF& fMoveA(fl4 _x, fl4 _y, fl4 _z = 0);
	TransformF& fRotateA(fl4 _x, fl4 _y, fl4 _z = 0);
	TransformF& fRotateA(fl4 _z = 0);
	TransformF& fScaleA(fl4 _x, fl4 _y, fl4 _z = 0);
	TransformF& fScaleAddA(fl4 _x, fl4 _y, fl4 _z = 0);
	TransformF& fCenterA(fl4 _x, fl4 _y, fl4 _z = 0);

	// Make matrix with parents or not.
	nXD::F44 fAsF44(bln _abs = true);
	TransformF& fMakeF44A( out nXD::F44& _transformer, bln _abs = true);

	TransformF& fParentSetA(TransformF* _value);
	TransformF* fParentP();

	void fAbsolute(out TransformF& _abs) const;
	void fAdd(out TransformF& _oper) const;

	nXD::F4 fPositionXYZW() const;

private:
	fl4 px, py, pz; // Position. 1/1 for point.
	fl4 rx, ry, rz; // Rotation. 0/1 for round.
	fl4 sx, sy, sz; // Scaling.  1/1 for ethalon point.
	fl4 cx, cy, cz; // Center.   1/1 for point.

	// Parent node
	TransformF* parent;
	// Owner object for access from scene graph
	void*    owner;
	// Number of frame when object was changed
	szt      frame;
	// What is it for?
	szt      state;
};
}// nXD

#endif // _XD_TRTR_H_INCLUDED
