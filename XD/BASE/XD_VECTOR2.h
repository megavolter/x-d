#ifndef _XD_VECTOR2_H_INCLUDED
#define _XD_VECTOR2_H_INCLUDED
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Autor: UCKUHT
// Date: 2013-05-19
// Desc: 2d space vector. Out dated file.
//============================================================================//

#include "BASE/TYPE.h"
#include "BASE/XD_F2.h"

//============================================================================//
struct   XD_VECTOR2 : public XD_F2
{
    void
    fSet(fl4 _x, fl4 _y)
    {
        x = _x;
        y = _y;
    }

    void
    fDrop()
    {
        x = y = 0;
    }

    void
    fMove(fl4 _x, fl4 _y)
    {
        x += _x;
        y += _y;
    }
};
//============================================================================//

#endif // _XD_VECTOR2_H_INCLUDED
