#ifndef _XD_DIFFUSE_H_INCLUDED
#define _XD_DIFFUSE_H_INCLUDED

#include "XD_BT4.h"

struct XD_DIFFUSE : public XD_BT4
{
    XD_DIFFUSE() : XD_BT4(0) {}
    XD_DIFFUSE(ui4 _ini) : XD_BT4(_ini) {}
    XD_DIFFUSE(ui1 _r, ui1 _g, ui1 _b, ui1 _a)
    {
        fSet(_r, _g, _b, _a);
    }

    void
    fSet(ui1 _r, ui1 _g, ui1 _b, ui1 _a)
    {
        r = _r; g = _g; b = _b; a = _a;
    }
};

#endif // _XD_DIFFUSE_H_INCLUDED
