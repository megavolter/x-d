#pragma once

#include <vector>

static void ArraysInitializer()
{
	std::vector<int> v1 {5};
	std::vector<int> v2 (5);
	std::vector<int> v3 ({5});
	std::vector<int> v4 {5};
	std::vector<int> v5 = {5, 6, 7};
}

class DoAllINeed {
	DoAllINeed() {

	}
};
