#ifndef _LOG_H_INCLUDED
#define _LOG_H_INCLUDED
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Autor: UCKUHT
// Date: 2013-05-19
// Desc: logging tools.
//============================================================================//
#include <iostream>
// Formaters.
#define mXD_DEBUG_STOP() 		asm("int3")

#ifdef XD_BREAK_ERRORS
#define mBREAK_ERR()			mXD_DEBUG_STOP
#else
#define mBREAK_ERR()
#endif

#define mSPASED(text)		" "<<text<<" "
#define mBRACED(text)		" <"<<text<<"> "
#define mDBRACED(text)		" <<"<<text<<">> "
#define mSBRACED(text)		" ["<<text<<"] "
#define mRBRACED(text)		" ("<<text<<") "
#define mQUOTED(text)		'\"'<< text << '\"'
#define mLINE				" line: ["<<__LINE__<<"] "
#define mFUNC				" ["<<__FUNCTION__<<"] "
#define mPFUNC				" ["<<__PRETTY_FUNCTION__<<"] "

#define mLOG(text)			std::cout<<text
#define mNOTE(text)			mLOG(">>>"<<text<<'\n')

#define mDEV(text)			mNOTE(text<<mLINE)
#define mDEV_VAR(text)		mDEV(#text<<" = "<<text)
#define mDEV_TRACE(text)	mDEV(text<<mPFUNC<<mLINE)
#define mDEV_FUNC(text)		mDEV(mFUNC)
#define mDEV_PFUNC(text)	mDEV(mPFUNC)

#define mERROR(text)\
	{mBREAK_ERR(); mNOTE(mBRACED("!Error:")<<text<<mBRACED(mLINE));}

#define mERRORF(text)		mERROR(mFUNC<<mQUOTED(text))
#define mERRORP(text)		mERROR(mPFUNC<<mQUOTED(text))
#define mFAIL(text)			{mERRORP(text); return X_X;}

#define mFAIL_SEARCH(text) \
	mERRORF("search failed: "<<text)

#define mFORBIDDEN_OPERATON() \
	{mERRORP("This operation is forbidden"); exit(0);}

#define mWARNING(text)		//mNOTE("!Warn "<<text<<mLINE)
#define mWARNINGF(text)		mNOTE("!Warning: "<<text<<mPFUNC<<mLINE)
#define mFUNC_IMPL_ERR()	mWARNINGF("This function doesn't completed")
#define mSNIPLET()			if(false)
#define mTODO(text)
//============================================================================//
#endif // _LOG_H_INCLUDED
