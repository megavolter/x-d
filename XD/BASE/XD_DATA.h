#ifndef _XD_DATA_H_INCLUDED__
#define _XD_DATA_H_INCLUDED__
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Autor: UCKUHT
// Date: 2013-05-19
// Desc: data keeper struct.
//============================================================================//
#include "BASE/TYPE.h"
#include "BASE/CAST.h"
//============================================================================//
namespace nXD {
struct DATA
{
    ui1*    data;
    szt     data_size;
public:
    DATA():data(0),data_size(0){}

    void
    fAlloc(szt size) {
        data = new ui1[size];
        data_size = size;
    }

    void
    fFree() {
        delete[] data;
        data_size = 0;
    }

    szt
    fSize() {
        return data_size;
    }

    ui1*
	fDataP() {
    	return data;
    }

    str
	fStrGetP(){
    	return RCAST(str, data);
    }
};
}// nXD
//============================================================================//

#endif // XD_DATA_H_INCLUDED
