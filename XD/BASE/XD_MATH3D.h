#pragma once

#include "BASE/XD_MATH.h"
#include "BASE/XD_F44.h"
#include "BASE/CODE.h"

namespace nXD{

void
mat4_set_identity( F44& out _self );

void
mat4_set_zero( F44& out _self );

void
mat4_multiply( F44& out _self, const F44& _other );

void
mat4_set_orthographic(	F44& out _self,
						float left,   float right,
						float bottom, float top,
						float znear,  float zfar
						);

void
mat4_set_perspective(	F44& out _self,
						float fovy,  float aspect,
						float zNear, float zFar
						);

void
mat4_set_frustum(	F44& out _self,
					float left,   float right,
					float bottom, float top,
					float znear,  float zfar
					);

void
mat4_set_rotation(	F44& out _self,
					float angle,
					float x, float y, float z
					);

void
mat4_set_rotation_rad(	F44& out _self,
						float angle,
						float x, float y, float z
						);

void
mat4_set_translation(	F44& out _self,
						float x, float y, float z
						);

void
mat4_set_scaling(	F44& out _self,
					float x, float y, float z
					);

void
mat4_rotate(	F44& out _self,
				float angle,
				float x, float y, float z
				);

void
mat4_rotate_rad( F44& out _self,
				 float angle,
				 float x, float y, float z
				 );

void
mat4_translate( F44& out _self,
				float x, float y, float z
				);

void
mat4_scale( F44& out self,
			float x, float y, float z
			);
}// nXD
