#ifndef _ES_STRING_H_INCLUDED
#define _ES_STRING_H_INCLUDED
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Autor: UCKUHT
// Date: 2013-05-19
// Desc: my own bicycle. I like it.
//============================================================================//
#include <cstring>

#include "BASE/TYPE.h"

struct ES_WSTRING
{
    wchar_t* data;
    szt      data_size;
public:
    ES_WSTRING();
    ~ES_WSTRING();
    
    bool        fEmpty();
    bool        empty();
    
    bool        fSet(wchar_t* str);
    wchar_t*    fGet();
};

#endif // ES_STRING_H_INCLUDED
