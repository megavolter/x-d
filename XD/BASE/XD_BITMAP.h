#pragma once

#include "BASE/LANG.h"
#include "BASE/TYPE.h"
#include "BASE/XD_POINT2.h"
#include "BASE/LOG.h"

namespace nXD{

struct BITMAP
{
	// Raw
	void*           data;
	szt             data_size;
	// Result
	ui1*            bmp;
	szt             bmp_size;
	XDE_PIXEL       format;
	XD_POINT2       size;
public:
	BITMAP(void* _data, szt _data_size)
		: data(_data), data_size(_data_size),
		  bmp(0), bmp_size(0), format(XDE_PIXEL_NONE)
	{}

	~BITMAP()
	{
		if(bmp)
			delete[] bmp;
	}

	BITMAP& fSetA(void* _data, szt _data_size){
		data = _data; data_size = _data_size;
		return *this;
	}

	bool fEmpty()
	{
		return bmp_size == 0;
	}

	void fFree()
	{
		mTODO();
	}
};

}// nXD
