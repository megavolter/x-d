#ifndef _XD_TRPR_H_INCLUDED
#define _XD_TRPR_H_INCLUDED
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Autor: UCKUHT
// Date: 2013-05-19
// Desc: TRansPlantatoR.
//============================================================================//
#include "BASE/CODE.h"
#include "BASE/XD_F4.h"
#include "BASE/XD_F44.h"

struct XD_TRPR
{
xd_data:
    XD_F44 data;
    XD_F4 pivot;
    bln use_pivot;
    XD_TRPR* parent;

xd_functional:
    XD_TRPR& fClearA();

    XD_TRPR&	fPositionSetA();
    XD_TRPR&	fRotationSetA();
    XD_TRPR&	fScaleSetA();
    XD_TRPR&	fPivotSetA();

    XD_TRPR&	fMoveA();
	XD_TRPR&	fRotateA();
    XD_TRPR&	fScaleA();
    XD_TRPR&	fPivotMoveA();

    XD_TRPR*	fParentP();
    XD_F44&		fDataA();
    void fFill(out XD_F44& transformer);
};

#endif // _XD_TRPR_H_INCLUDED
