#ifndef GEOMETRY_FIGURES3D_H_
#define GEOMETRY_FIGURES3D_H_

#include "BASE/TYPE.h"
#include "BASE/CODE.h"

namespace nXD{

namespace Figures3D
{
	static const szt quad_vx_cnt = 4;
	static const szt quad_ix_cnt = quad_vx_cnt + 2;
	static const fl4 quad_pos[quad_vx_cnt][3] = {
		{0,0,0},
		{1,0,0},
		{1,1,0},
		{0,1,0},
	};
	static const fl4 quad_pos_n[quad_vx_cnt][3] = {
		{-0.5,-0.5,0},
		{-0.5,0.5,0},
		{0.5,0.5,0},
		{0.5,-0.5,0},
	};
	static const fl4 quad_tex[quad_vx_cnt][2] = {
		{0,0},
		{1,0},
		{1,1},
		{0,1},
	};
	static const ui2 quad_ix[quad_ix_cnt] = {
		0,1,2,0,2,3
	};
};

}


#endif /* GEOMETRY_FIGURES3D_H_ */
