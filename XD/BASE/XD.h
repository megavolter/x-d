#ifndef _XD_H_INCLUDED
#define _XD_H_INCLUDED

#include "BASE/TYPE.h"
#include "BASE/LANG.h"
#include "BASE/XD_F2.h"
#include "BASE/XD_F4.h"

#define XD_DT gl_dt
namespace nXD{

inline
void
fCentreLP(F2& _to, F4& _size, XDE_LP _mode, fl4 _x = 0, fl4 _y = 0)
{// Size to local point.
//    fl4 x[3] = {0,0.5,1};
//    fl4 y[4] = {1,0.5,0};
    fl4 x[3] = {0.5,0,-0.5};
    fl4 y[4] = {-0.5,0,0.5};
    si4 dy = (_mode + 3)/3 - 1;
    si4 dx = _mode%3;
    _to.fSet(x[dx] * _size.x + _size.z + _x,
             y[dy] * _size.y + _size.w + _y);
}

}//ns
#endif // _XD_H_INCLUDED
