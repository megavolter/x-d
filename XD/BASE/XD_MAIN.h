#ifndef _XD_MAIN_H_INCLUDED
#define _XD_MAIN_H_INCLUDED

#define XD_MAIN_DECLARE() int main()
#define XD_WWW_DECLARE(name) sip name(void* who, void* why)

#endif // XD_MAIN_H_INCLUDED
