#include "XD_TRTR.h"
#include "XD_MATH3D.h"
#include "XD_F4.h"
using namespace nXD;
//============================================================================//
TransformF::TransformF()
{
	fClear();
	fScaleSetA(1,1,1);
}
//============================================================================//
TransformF::TransformF(fl4 _scale)
{
	fClear();
	fScaleSetA(_scale, _scale, _scale);
}
//============================================================================//
void TransformF::fClear()
{
	memset(this, 0, sizeof(TransformF));
}
//============================================================================//
TransformF&
TransformF::fPositionSetA(fl4 _x, fl4 _y, fl4 _z)
{
	px = _x; py = _y; pz = _z;
	return *this;
}
//============================================================================//
TransformF&
TransformF::fPositionSetA(const nXD::F4& _pos)
{
	fPositionSetA( _pos.x, _pos.y, _pos.z);
	return *this;
}
//============================================================================//
TransformF&
TransformF::fRotationSetA(fl4 _x, fl4 _y, fl4 _z)
{
	rx = _x; ry = _y; rz = _z;
	return *this;
}
//============================================================================//
TransformF&
TransformF::fRotationSetA(fl4 _z)
{
	rz = _z;
	return *this;
}
//============================================================================//
TransformF&
TransformF::fRotateA(fl4 _x, fl4 _y, fl4 _z)
{
	rx += _x; ry += _y; rz += _z;
	return *this;
}
//============================================================================//
TransformF&
TransformF::fRotateA(fl4 _z)
{
	rz += _z;
	return *this;
}
//============================================================================//
TransformF&
TransformF::fScaleSetA(fl4 _x, fl4 _y, fl4 _z)
{
	sx = _x; sy = _y; sz = _z;
	return *this;
}
//============================================================================//
TransformF&
TransformF::fScaleA(fl4 _x, fl4 _y, fl4 _z)
{
	sx *= _x; sy *= _y; sz *= _z;
	return *this;
}
//============================================================================//
TransformF&
TransformF::fScaleAddA(fl4 _x, fl4 _y, fl4 _z)
{
	sx += _x; sy += _y; sz += _z;
	return *this;
}
//============================================================================//
TransformF&
TransformF::fCenterSetA(fl4 _x, fl4 _y, fl4 _z)
{
	cx = _x; cy = _y; cz = _z;
	return *this;
}
//============================================================================//
TransformF&
TransformF::fCenterA(fl4 _x, fl4 _y, fl4 _z)
{
	cx += _x;
	cy += _y;
	cz += _z;
	return *this;
}

//============================================================================//
TransformF&
TransformF::fMoveA(fl4 _x, fl4 _y, fl4 _z)
{
	px += _x; py += _y; pz += _z;
	return *this;
}
//============================================================================//
nXD::F44
TransformF::fAsF44(bln _abs)
{
	nXD::F44 result;
	fMakeF44A(result, _abs);
	return result;
}
//============================================================================//
TransformF&
TransformF::fMakeF44A(out nXD::F44& _transform, bln _abs)
{
	if(_abs == false || 0x0 == parent)
	{//1
		if(sx != 1 || sy != 1 || sz != 1)
		{// Scale.
			mat4_scale(_transform, sx, sy, sz);
		}
		if(cx || cy || cz)
		{// Pivot.
			mat4_translate(_transform, cx, cy, cz);
		}
		if(rx)
		{// Rotate.
			mat4_rotate_rad(_transform, rx, 1, 0, 0);
		}
		if(ry)
		{
			mat4_rotate_rad(_transform, ry, 0, 1, 0);
		}
		if(rz)
		{
			mat4_rotate_rad(_transform, rz, 0, 0, 1);
		}
		mat4_translate(_transform, px, py, pz);
	}//1
	else
	{//1 Have parent logic.
		TransformF absolute(1);
		fAbsolute(absolute);
		absolute.fMakeF44A(_transform);
	}//1
	return *this;
}
//============================================================================//
TransformF&
TransformF::fParentSetA(TransformF* _value)
{
	parent = _value;
	return *this;
}
//============================================================================//
TransformF*
TransformF::fParentP()
{
	return parent;
}
//============================================================================//
void
TransformF::fAbsolute(out TransformF& _abs) const
{
 	if(parent)
	{
		parent->fAbsolute(_abs);
	}
	fAdd(_abs);
}
//============================================================================//
void
TransformF::fAdd(TransformF& _oper) const
{
	_oper.fMoveA(px,py,pz);
	_oper.fCenterA(cx,cy,cz);
	_oper.fRotateA(rx,ry,rz);
	_oper.fScaleA(sx,sy,sz);
}
//============================================================================//
nXD::F4
TransformF::fPositionXYZW() const
{
	return nXD::F4(px, py, pz, 0);
}
//============================================================================//






