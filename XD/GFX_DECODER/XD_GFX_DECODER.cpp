#include "zlibpng/png.h"

#include <string.h>

#include "XD_GFX_DECODER.h"
#include "BASE/TYPE.h"
#include "BASE/LOG.h"
#include "BASE/CAST.h"

namespace nXD{

static const ui1 png_header[] = { 0x89, 0x50, 0x4E, 0x47 };
static const szt png_header_size = 4;// Its enough.
static const szt sig_bytes = 8;
//============================================================================//
class LocalPngDecoder
{
public:
	png_bytep   data;
	png_size_t  cursor;
	png_uint_32 w;// Width.
	png_uint_32 h;// Height.
	png_int_32  d;// Depth.
	png_int_32  c;// Color.

public:
	LocalPngDecoder(png_bytep data_, png_size_t cursor_)
		: data(data_), cursor(cursor_) {}

	static void fRead(png_structp s_png, png_bytep data, png_size_t data_size)
	{
		LocalPngDecoder* l_d = SCAST(LocalPngDecoder*, png_get_io_ptr(s_png));
		memcpy(data, l_d->data + l_d->cursor, data_size);
		l_d->cursor += data_size;
	}
};
//============================================================================//
si4
GFX_DECODER::iInitX(nXD::BITMAP& out_bmp)
{
	if(out_bmp.data)
	{//1
		ui1* data = SCAST(ui1*, out_bmp.data);
		if( W_W == memcmp(png_header, data, png_header_size ))
		{//2 Check header
			return A_A;
		}//2
		else
		{//2
			mERRORP("Data is not supported");
		}//2
	}//1
	else
	{//1
		mERRORP("Bitmap is empty");
	}//1
	return X_X;
}
//============================================================================//
si4
GFX_DECODER::iDecodeX(nXD::BITMAP& _bitmap)
{

	png_structp s_png = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
	png_infop   i_png = png_create_info_struct(s_png);
	LocalPngDecoder d_png(SCAST(png_bytep, _bitmap.data), sig_bytes);

	png_set_read_fn(s_png, &d_png, LocalPngDecoder::fRead);
	png_set_sig_bytes(s_png, sig_bytes);
	png_read_info(s_png, i_png);
	png_get_IHDR(s_png, i_png, &d_png.w, &d_png.h, &d_png.d, &d_png.c, 0,0,0);

	if(d_png.d == 16)
	{
		png_set_strip_16(s_png);
	}

	if(PNG_COLOR_TYPE_RGBA != d_png.c )
	{
		mWARNING(" png format is not suported ");
	}

	if(png_get_valid(s_png, i_png, PNG_INFO_tRNS) != 0)
	{
		png_set_tRNS_to_alpha(s_png);
	}

	png_read_update_info(s_png, i_png);
	png_get_IHDR(s_png, i_png, &d_png.w, &d_png.h, &d_png.d, &d_png.c, 0,0,0);

	png_size_t row_size = png_get_rowbytes(s_png, i_png);
	png_bytepp row_array = new png_bytep[d_png.h];
	png_bytep  bmp = new png_byte[row_size * d_png.h];

	_bitmap.bmp = bmp;
	_bitmap.bmp_size = row_size * d_png.h;
	_bitmap.size.fSet(d_png.w, d_png.h);
	_bitmap.format = XDE_PIXEL_r8g8b8a8;

	for(ui4 i = 0; i < d_png.h; ++i)
	{//1 Setup row pointers.
		row_array[d_png.h - i - 1] = bmp + i * row_size;
	}//1

	png_read_image(s_png, row_array);
	png_destroy_read_struct(&s_png, &i_png, 0);
	delete[] row_array;

	return A_A;
}
//============================================================================//
void
GFX_DECODER::iClear(nXD::BITMAP& bitmap)
{
   if(bitmap.bmp)
	   delete[] bitmap.bmp;
   bitmap.bmp = 0x0;
}
//============================================================================//


















}//ns
