#ifndef _XD_GFX_DECODER_H_INCLUDED
#define _XD_GFX_DECODER_H_INCLUDED
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Autor: UCKUHT
// Date: 2013-05-19
// Desc: decode from raw to bitmaps. Formats png.
//============================================================================//
#include "BASE/LANG.h"
#include "BASE/XD_POINT2.h"
#include "BASE/XD_BITMAP.h"

namespace nXD{

class GFX_DECODER
{
public:
	static si4
	iInitX(nXD::BITMAP& out_bmp);

	static si4
	iDecodeX(nXD::BITMAP& out_bmp);

	static void
	iClear(nXD::BITMAP& bitmap);
};

}//ns
//============================================================================//

#endif // _XD_GFX_DECODER_H_INCLUDED
