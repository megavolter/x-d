#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/2131571004/XD_STRING.o \
	${OBJECTDIR}/_ext/1852335217/XD.o \
	${OBJECTDIR}/_ext/1852335217/XD_MATH3D.o \
	${OBJECTDIR}/_ext/1852335217/XD_TRTR.o \
	${OBJECTDIR}/_ext/900228903/CASUAL_PROGRAMS_BANK.o \
	${OBJECTDIR}/_ext/900228903/XD_FRAME.o \
	${OBJECTDIR}/_ext/900228903/XD_GAME.o \
	${OBJECTDIR}/_ext/900228903/XD_GFXEX.o \
	${OBJECTDIR}/_ext/900228903/XD_MESH.o \
	${OBJECTDIR}/_ext/1691607279/XD_GFX.o \
	${OBJECTDIR}/_ext/462201610/XD_GFX_DECODER.o \
	${OBJECTDIR}/_ext/1581229447/XD_OS.o \
	${OBJECTDIR}/_ext/1581229447/XD_WDGT.o \
	${OBJECTDIR}/_ext/1691596782/XD_RDL.o \
	${OBJECTDIR}/_ext/663136823/adler32.o \
	${OBJECTDIR}/_ext/663136823/compress.o \
	${OBJECTDIR}/_ext/663136823/crc32.o \
	${OBJECTDIR}/_ext/663136823/deflate.o \
	${OBJECTDIR}/_ext/663136823/gzclose.o \
	${OBJECTDIR}/_ext/663136823/gzlib.o \
	${OBJECTDIR}/_ext/663136823/gzread.o \
	${OBJECTDIR}/_ext/663136823/gzwrite.o \
	${OBJECTDIR}/_ext/663136823/infback.o \
	${OBJECTDIR}/_ext/663136823/inffast.o \
	${OBJECTDIR}/_ext/663136823/inflate.o \
	${OBJECTDIR}/_ext/663136823/inftrees.o \
	${OBJECTDIR}/_ext/663136823/png.o \
	${OBJECTDIR}/_ext/663136823/pngerror.o \
	${OBJECTDIR}/_ext/663136823/pngget.o \
	${OBJECTDIR}/_ext/663136823/pngmem.o \
	${OBJECTDIR}/_ext/663136823/pngpread.o \
	${OBJECTDIR}/_ext/663136823/pngread.o \
	${OBJECTDIR}/_ext/663136823/pngrio.o \
	${OBJECTDIR}/_ext/663136823/pngrtran.o \
	${OBJECTDIR}/_ext/663136823/pngrutil.o \
	${OBJECTDIR}/_ext/663136823/pngset.o \
	${OBJECTDIR}/_ext/663136823/pngtrans.o \
	${OBJECTDIR}/_ext/663136823/pngwio.o \
	${OBJECTDIR}/_ext/663136823/pngwrite.o \
	${OBJECTDIR}/_ext/663136823/pngwtran.o \
	${OBJECTDIR}/_ext/663136823/pngwutil.o \
	${OBJECTDIR}/_ext/663136823/trees.o \
	${OBJECTDIR}/_ext/663136823/uncompr.o \
	${OBJECTDIR}/_ext/663136823/zutil.o \
	${OBJECTDIR}/Code/cGame.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../../MODULES/GFX/add/glext.lib /C/MinGW/lib/libgdi32.a /C/MinGW/lib/libopengl32.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/medical_mechanica.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/medical_mechanica.exe: ../../MODULES/GFX/add/glext.lib

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/medical_mechanica.exe: /C/MinGW/lib/libgdi32.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/medical_mechanica.exe: /C/MinGW/lib/libopengl32.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/medical_mechanica.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/medical_mechanica ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/2131571004/XD_STRING.o: ../../BASE/STRINGS/XD_STRING.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/2131571004
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/2131571004/XD_STRING.o ../../BASE/STRINGS/XD_STRING.cpp

${OBJECTDIR}/_ext/1852335217/XD.o: ../../BASE/XD.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1852335217
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1852335217/XD.o ../../BASE/XD.cpp

${OBJECTDIR}/_ext/1852335217/XD_MATH3D.o: ../../BASE/XD_MATH3D.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1852335217
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1852335217/XD_MATH3D.o ../../BASE/XD_MATH3D.cpp

${OBJECTDIR}/_ext/1852335217/XD_TRTR.o: ../../BASE/XD_TRTR.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1852335217
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1852335217/XD_TRTR.o ../../BASE/XD_TRTR.cpp

${OBJECTDIR}/_ext/2084822207/C4.h.gch: ../../EXT/C4/C4.h 
	${MKDIR} -p ${OBJECTDIR}/_ext/2084822207
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o "$@" ../../EXT/C4/C4.h

${OBJECTDIR}/_ext/900228903/CASUAL_PROGRAMS_BANK.o: ../../EXT/CASUAL/CASUAL_PROGRAMS_BANK.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/900228903
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/900228903/CASUAL_PROGRAMS_BANK.o ../../EXT/CASUAL/CASUAL_PROGRAMS_BANK.cpp

${OBJECTDIR}/_ext/900228903/XD_FRAME.o: ../../EXT/CASUAL/XD_FRAME.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/900228903
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/900228903/XD_FRAME.o ../../EXT/CASUAL/XD_FRAME.cpp

${OBJECTDIR}/_ext/900228903/XD_GAME.o: ../../EXT/CASUAL/XD_GAME.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/900228903
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/900228903/XD_GAME.o ../../EXT/CASUAL/XD_GAME.cpp

${OBJECTDIR}/_ext/900228903/XD_GFXEX.o: ../../EXT/CASUAL/XD_GFXEX.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/900228903
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/900228903/XD_GFXEX.o ../../EXT/CASUAL/XD_GFXEX.cpp

${OBJECTDIR}/_ext/900228903/XD_MESH.o: ../../EXT/CASUAL/XD_MESH.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/900228903
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/900228903/XD_MESH.o ../../EXT/CASUAL/XD_MESH.cpp

${OBJECTDIR}/_ext/1691607279/XD_GFX.o: ../../MODULES/GFX/XD_GFX.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1691607279
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1691607279/XD_GFX.o ../../MODULES/GFX/XD_GFX.cpp

${OBJECTDIR}/_ext/462201610/XD_GFX_DECODER.o: ../../MODULES/GFX_DECODER/XD_GFX_DECODER.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/462201610
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/462201610/XD_GFX_DECODER.o ../../MODULES/GFX_DECODER/XD_GFX_DECODER.cpp

${OBJECTDIR}/_ext/1581229447/XD_OS.o: ../../MODULES/OS/win/XD_OS.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1581229447
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1581229447/XD_OS.o ../../MODULES/OS/win/XD_OS.cpp

${OBJECTDIR}/_ext/1581229447/XD_WDGT.o: ../../MODULES/OS/win/XD_WDGT.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1581229447
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1581229447/XD_WDGT.o ../../MODULES/OS/win/XD_WDGT.cpp

${OBJECTDIR}/_ext/1691596782/XD_RDL.o: ../../MODULES/RDL/XD_RDL.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/1691596782
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/1691596782/XD_RDL.o ../../MODULES/RDL/XD_RDL.cpp

${OBJECTDIR}/_ext/663136823/adler32.o: ../../SOLUTIONS/zlibpng/adler32.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/adler32.o ../../SOLUTIONS/zlibpng/adler32.c

${OBJECTDIR}/_ext/663136823/compress.o: ../../SOLUTIONS/zlibpng/compress.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/compress.o ../../SOLUTIONS/zlibpng/compress.c

${OBJECTDIR}/_ext/663136823/crc32.o: ../../SOLUTIONS/zlibpng/crc32.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/crc32.o ../../SOLUTIONS/zlibpng/crc32.c

${OBJECTDIR}/_ext/663136823/deflate.o: ../../SOLUTIONS/zlibpng/deflate.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/deflate.o ../../SOLUTIONS/zlibpng/deflate.c

${OBJECTDIR}/_ext/663136823/gzclose.o: ../../SOLUTIONS/zlibpng/gzclose.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/gzclose.o ../../SOLUTIONS/zlibpng/gzclose.c

${OBJECTDIR}/_ext/663136823/gzlib.o: ../../SOLUTIONS/zlibpng/gzlib.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/gzlib.o ../../SOLUTIONS/zlibpng/gzlib.c

${OBJECTDIR}/_ext/663136823/gzread.o: ../../SOLUTIONS/zlibpng/gzread.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/gzread.o ../../SOLUTIONS/zlibpng/gzread.c

${OBJECTDIR}/_ext/663136823/gzwrite.o: ../../SOLUTIONS/zlibpng/gzwrite.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/gzwrite.o ../../SOLUTIONS/zlibpng/gzwrite.c

${OBJECTDIR}/_ext/663136823/infback.o: ../../SOLUTIONS/zlibpng/infback.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/infback.o ../../SOLUTIONS/zlibpng/infback.c

${OBJECTDIR}/_ext/663136823/inffast.o: ../../SOLUTIONS/zlibpng/inffast.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/inffast.o ../../SOLUTIONS/zlibpng/inffast.c

${OBJECTDIR}/_ext/663136823/inflate.o: ../../SOLUTIONS/zlibpng/inflate.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/inflate.o ../../SOLUTIONS/zlibpng/inflate.c

${OBJECTDIR}/_ext/663136823/inftrees.o: ../../SOLUTIONS/zlibpng/inftrees.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/inftrees.o ../../SOLUTIONS/zlibpng/inftrees.c

${OBJECTDIR}/_ext/663136823/png.o: ../../SOLUTIONS/zlibpng/png.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/png.o ../../SOLUTIONS/zlibpng/png.c

${OBJECTDIR}/_ext/663136823/pngerror.o: ../../SOLUTIONS/zlibpng/pngerror.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/pngerror.o ../../SOLUTIONS/zlibpng/pngerror.c

${OBJECTDIR}/_ext/663136823/pngget.o: ../../SOLUTIONS/zlibpng/pngget.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/pngget.o ../../SOLUTIONS/zlibpng/pngget.c

${OBJECTDIR}/_ext/663136823/pngmem.o: ../../SOLUTIONS/zlibpng/pngmem.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/pngmem.o ../../SOLUTIONS/zlibpng/pngmem.c

${OBJECTDIR}/_ext/663136823/pngpread.o: ../../SOLUTIONS/zlibpng/pngpread.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/pngpread.o ../../SOLUTIONS/zlibpng/pngpread.c

${OBJECTDIR}/_ext/663136823/pngread.o: ../../SOLUTIONS/zlibpng/pngread.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/pngread.o ../../SOLUTIONS/zlibpng/pngread.c

${OBJECTDIR}/_ext/663136823/pngrio.o: ../../SOLUTIONS/zlibpng/pngrio.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/pngrio.o ../../SOLUTIONS/zlibpng/pngrio.c

${OBJECTDIR}/_ext/663136823/pngrtran.o: ../../SOLUTIONS/zlibpng/pngrtran.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/pngrtran.o ../../SOLUTIONS/zlibpng/pngrtran.c

${OBJECTDIR}/_ext/663136823/pngrutil.o: ../../SOLUTIONS/zlibpng/pngrutil.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/pngrutil.o ../../SOLUTIONS/zlibpng/pngrutil.c

${OBJECTDIR}/_ext/663136823/pngset.o: ../../SOLUTIONS/zlibpng/pngset.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/pngset.o ../../SOLUTIONS/zlibpng/pngset.c

${OBJECTDIR}/_ext/663136823/pngtrans.o: ../../SOLUTIONS/zlibpng/pngtrans.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/pngtrans.o ../../SOLUTIONS/zlibpng/pngtrans.c

${OBJECTDIR}/_ext/663136823/pngwio.o: ../../SOLUTIONS/zlibpng/pngwio.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/pngwio.o ../../SOLUTIONS/zlibpng/pngwio.c

${OBJECTDIR}/_ext/663136823/pngwrite.o: ../../SOLUTIONS/zlibpng/pngwrite.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/pngwrite.o ../../SOLUTIONS/zlibpng/pngwrite.c

${OBJECTDIR}/_ext/663136823/pngwtran.o: ../../SOLUTIONS/zlibpng/pngwtran.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/pngwtran.o ../../SOLUTIONS/zlibpng/pngwtran.c

${OBJECTDIR}/_ext/663136823/pngwutil.o: ../../SOLUTIONS/zlibpng/pngwutil.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/pngwutil.o ../../SOLUTIONS/zlibpng/pngwutil.c

${OBJECTDIR}/_ext/663136823/trees.o: ../../SOLUTIONS/zlibpng/trees.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/trees.o ../../SOLUTIONS/zlibpng/trees.c

${OBJECTDIR}/_ext/663136823/uncompr.o: ../../SOLUTIONS/zlibpng/uncompr.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/uncompr.o ../../SOLUTIONS/zlibpng/uncompr.c

${OBJECTDIR}/_ext/663136823/zutil.o: ../../SOLUTIONS/zlibpng/zutil.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/663136823
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/663136823/zutil.o ../../SOLUTIONS/zlibpng/zutil.c

${OBJECTDIR}/Code/cGame.o: Code/cGame.cpp 
	${MKDIR} -p ${OBJECTDIR}/Code
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Code/cGame.o Code/cGame.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../MODULES -I../../ -I../../EXT/CASUAL -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/medical_mechanica.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
