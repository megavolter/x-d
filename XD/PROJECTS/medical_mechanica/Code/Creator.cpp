#include "Creator.h"
#include "BASE/XD_ATL_FRM.h"

//============================================================================//
CREATOR::CREATOR(XD::GFX& _gfx, XD::RDL _rdl)
    : m_gfx(_gfx), m_rdl(_rdl)
{}
//============================================================================//
#define CREATOR_TRANSLATE_FUNC(out) \
    C4_PARSER::VALUES& out = *RCAST(C4_PARSER::VALUES*, _why);\
    CREATOR* thiz = RCAST(CREATOR*, _who); \
    mNOTE("Run cb "<<mPFUNC);\
    thiz->f();
//============================================================================//
void
CREATOR::fTextureAdd(const XD_STRING& _tex_name, XD::GFX::TEXTURE* _texture)
{
    m_texture_man[_tex_name] = _texture;
}
//============================================================================//
XD::GFX::TEXTURE*
CREATOR::fTextureGet(const char* _tex_name)
{
    XD_STRING cmpr(_tex_name, false);
    texture_map_iter_t result = m_texture_man.find(cmpr);
    if(result == m_texture_man.end())
        return 0x0;
    else
        return result->second;
}
//============================================================================//
void
gObjectInit(const pugi::xml_node& _data, C4* _object)
{
    const char* name = _data.attribute("n").as_string("unnamed");
    fl4 layer = _data.attribute("l").as_float(0);
    _object->fName().fSet(name);
    _object->fLayer() = layer;
}
//============================================================================//
static XD_SPELL g_spell_default("default");
//============================================================================//
sip
CREATOR::cbCreateGeometry(void* _who, void* _why)
{
CREATOR_TRANSLATE_FUNC(inn);
    C4* parent = RCAST(C4*, inn.parent);
    XD::GFX::DATA5* geometry = RCAST(XD::GFX::DATA5*,
                                     parent->fvAccessP(C4::iSpellGeometry()));
    if(0x0 == geometry)
    {
        mFAIL_SEARCH("object " << parent->fName().fGet()
                     << mSPASED("has not geometry access"));
    }

    XD_SPELL type = inn.in.attribute("t").as_string(0x0);
    if(type.fIsEmpty())
    {
        mERRORP("geometry type is empty");
        return X_X;
    }

    if(type.fAsNumber() == g_spell_default.fAsNumber())
    {
        XD_ATL_FRM* frame = RCAST(XD_ATL_FRM*,
                                  parent->fvAccessP(C4::iSpellAtlFrame()));
        if(0x0 == frame)
        {
            mERRORP("object "<<parent->fName().fGet()
                    << mSPASED("has not atlass frame"));
            return X_X;
        }

        if(frame->fIsEmpty())
        {
            mERRORP("object "<<parent->fName().fGet()
                    << mSPASED("has empty frame"));
            return X_X;
        }

        fl4 img_size_x = frame->texture->size.x * frame->fSizeX();
        fl4 img_size_y = frame->texture->size.y * frame->fSizeY();
        XD_F4 size(img_size_x, img_size_y,
                   -img_size_x * 0.5, -img_size_y * 0.5);
        geometry->fInitQuadX(size, frame->frame);
        thiz->m_gfx.fData5ComposeX(geometry);
    }

    return A_A;
}
//============================================================================//
si4
CREATOR::cbCreateImage(void* _who, void* _why)
{
CREATOR_TRANSLATE_FUNC(inn);

mBLOCK("Image construction");
    C4_IMG* img = C4_IMG::iNewP();
    if(0x0 != inn.parent)
    {
        XD_TRTR* parent_tr =
            SCAST(XD_TRTR*, inn.parent->fvAccessP(C4::iSpellTransform()));

        if(0x0 != parent_tr)
            img->fTransform().fParentSet(parent_tr);
    }

    gObjectInit(inn.in, img);
    img->fRenderSet(&thiz->fGfx());
mBLOCK("Image ready");
    inn.result = img;

    return A_A;
}
//============================================================================//
si4
CREATOR::cbCreateImageOld(void* _who, void* _why)
{
CREATOR_TRANSLATE_FUNC(inn);
mOLDFUNC();
mBLOCK("Parsing");
    const char* name = inn.in.name();
    if(true != C4_IMG::iSpell().fEql(name))
    {// Not correct object.
        mERRORF("image creation fail: bad object name "<< name << "expected "
                << C4_IMG::iSpell().fGetP() );
    }
    const char* image = inn.in.attribute("i").as_string();
    if(0x0 == image)
    {
        mERRORF("image have no resource");
        return 0x0;
    }

mBLOCK("Image frame");
    XD::GFX::TEXTURE* texture = thiz->fTextureGet(image);
    if(0x0 == texture)
    {
        mERRORF("image texture not exists " << mQUOTED(image));
        return 0x0;
    }
    XD_ATL_FRM frame(texture);

mBLOCK("Creation");
    si4 layer = inn.in.attribute("l").as_int(0);
    name = inn.in.attribute("n").as_string("unnamed");
    C4_IMG* result = C4_IMG::iNewP();
    inn.result = result;
    result->fLayer() = layer;
    result->fName().fSet(name);
    mXD_RESULTA(result->fInitX(frame));
    mXD_RESULTA(result->fGeometryReg(thiz->m_gfx));

    inn.result = result;

mBLOCK("Parent");
    if(0x0 != inn.parent)
    {//1
        XD_TRTR* parent_tr = RCAST(XD_TRTR*,
                                inn.parent->fvAccessP(C4::iSpellTransform()));
        if(0x0 != parent_tr)
        {//2
            result->fTransform().fParentSet(parent_tr);
        }//2
    }//1

return A_A;
}
//============================================================================//
sip
CREATOR::cbCreateTransform(void* _who, void* _why)
{
CREATOR_TRANSLATE_FUNC(inn);
    XD_TRTR* tr_ptr = RCAST(XD_TRTR*,
                            inn.parent->fvAccessP(C4::iSpellTransform()));
    if(0x0 == tr_ptr) return O_O;

    pugi::xml_node  l_trans = inn.in;


    tr_ptr->fPositionSet(   l_trans.attribute("x").as_float(0),
                            l_trans.attribute("y").as_float(0),
                            l_trans.attribute("z").as_float(0));

    tr_ptr->fScaleSet(      l_trans.attribute("sx").as_float(1),
                            l_trans.attribute("sy").as_float(1),
                            l_trans.attribute("sz").as_float(1));

    tr_ptr->fRotationSet(   l_trans.attribute("rx").as_float(0),
                            l_trans.attribute("ry").as_float(0),
                            l_trans.attribute("rz").as_float(0));

    tr_ptr->fCenterSet(     l_trans.attribute("hx").as_float(0),
                            l_trans.attribute("hy").as_float(0),
                            l_trans.attribute("hz").as_float(0));

    return A_A;
}
//============================================================================//
sip
CREATOR::cbCreateFrame(void* _who, void* _why)
{
CREATOR_TRANSLATE_FUNC(inn);
    pugi::xml_node frame = inn.in;

mBLOCK("Check that object have frame component");
    XD_ATL_FRM* target = RCAST(XD_ATL_FRM*,
                               inn.parent->fvAccessP(C4::iSpellAtlFrame()));
    if(0x0 == target)
    {
        mERRORP("parent has not an atlas frame component");
        return O_O;
    }

mBLOCK("Get the texture");
    const char* image = frame.attribute("i").as_string(0x0);
    if(0x0 == image)
    {
        mERRORP("frame node has not attribute"<<mQUOTED("i")<<mQUOTED(image));
        return O_O;
    }

    XD::GFX::TEXTURE* tex_ptr = thiz->fTextureGet(image);
    if(0x0 == tex_ptr)
    {
        mFAIL_SEARCH(image);
        return O_O;
    }

    XD_F4 frame_bounds(
                    frame.attribute("l").as_float(0),
                    frame.attribute("u").as_float(0),
                    frame.attribute("r").as_float(1),
                    frame.attribute("d").as_float(1)
                    );


mBLOCK("Setup");
    target->fSet(tex_ptr, frame_bounds);

    return 0x0;
}
//============================================================================//
sip
CREATOR::cbCreateButton(void* _who, void* _why)
{
mBLOCK("Init");
    return 0x0;

}
//============================================================================//
//============================================================================//
