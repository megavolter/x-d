#ifndef _CURSOR_H_INCLUDED
#define _CURSOR_H_INCLUDED
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Author: UCKUHT
// Date: 2014-08-25
// Desc: game cursor.
//============================================================================//
#include "COMMON/C4/Samples/C4_FRAME2.h"
//============================================================================//
class GC_CURSOR : public C4_IMG
{
public:
    static GC_CURSOR* iNewP();
    virtual sip fvPostInit(void* _in) override;
};
//============================================================================//

#endif // _CURSOR_H_INCLUDED
