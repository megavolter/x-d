#include "cGame.hpp"

#include "BASE/CODE.h"
#include "BASE/XD_MATH.h"
#include "BASE/CAST.h"

#include "BASE/XD_I4.h"
#include "BASE/XD.h"
#include "BASE/XD_SPELL.h"

#include "C4/C4.h"
#include "C4/samples/c4_frame.h"
#include "C4/samples/C4_BTN_CLR.h"
#include "C4/samples/C4_POS.h"

#include "C4/Parser/xml/C4_PARSER.h"
#include "BASE/XD_WWW.h"
#include "BASE/STRINGS/XD_STRING.h"

const char g_name_game[] = "x/game.xml";

//============================================================================//
cGame::cGame() :
    m_transform(0x0), m_arrow_trtr(0x0), m_root(), m_game_c(), m_texture(0x0),
    m_scr_size(), m_app_size(1024,768),
    m_creator(m_game_c.fGfx(), m_game_c.fRdl()),
    m_cursor(0x0)
{}
//============================================================================//
si4
cGame::fInitX()
{
    mXD_RESULTA(XD::OS::fSystemCurentDirChange("Resources/"));
    return A_A;
}
//============================================================================//
si4
cGame::fStartX()
{
mBLOCK("System start");
    XD::OS::fSystemScreenSizeGetX(&m_scr_size);
    m_aspect = SCAST(fl4, m_app_size.y) / m_app_size.x;

mBLOCK("Initialization");
    XD::GAME_C::INITIAL init;
    init.draw_lsnr.fSet(this, cbDrawListener, 0x0);
    init.key_lsnr.fSet(this, cbKeyListener, 0x0);
//    init.mouse_lsnr.fSet(this, cbMouseListener, 0x0);
    init.wdgt_frame.fSet(
                         m_scr_size.x/2 - m_app_size.x/2,
                         m_scr_size.y/2 - m_app_size.y/2,
                         m_app_size.x,
                         m_app_size.y
    );
    init.wdgt_name = "medical mechanica";
    mXD_RESULTA(m_game_c.fInitX(init));
    mXD_RESULTA(fResReqX());
    mXD_RESULTA(m_game_c.fStartX());

    return O_O;
}
//============================================================================//
si4
cGame::fEndX()
{
    m_game_c.fStop();
    return A_A;
}
//============================================================================//
si4
cGame::fDrawX()
{
    XD_F4 m_pos;
    XD::OS::fWdgtCursorGet(m_game_c.widget, m_pos);
    fMouseX(m_pos.x, m_pos.y, m_pos.z, m_pos.w);
    ptr result = C4::iForward(  &m_root,
                                C4::TASK_DEFAULT,
                                C4::TASK_TYPE_DRAW,
                                &m_game_c.fGfx()
    );

    return A_A;
}
//============================================================================//
si4
cGame::fKeyX(XD::OS::INPUT_KEY* event)
{
    if(event->type == XDE_INPUT_DOWN)
    {
        XD_F2 repos(0,0);
        switch(event->key)
        {
        case XDE_KEY_ESC:
            fEndX();
            break;
        case XDE_KEY_UP: repos.fSet(0,1); break;
        case XDE_KEY_DOWN: repos.fSet(0,-1); break;
        case XDE_KEY_LEFT: repos.fSet(-1,0); break;
        case XDE_KEY_RIGHT: repos.fSet(1,0); break;
        case XDE_KEY_SPACE: fWorldTrReset(); break;
        default:
            mTRACE_FUNC();
            break;
        }
        if(0x0 != m_transform)
        {
            m_transform->fMove(repos.x, repos.y, 0);
        }
    }
    return A_A;
}
//============================================================================//
si4
cGame::fMouseX(si4 _x, si4 _y, si4 _l, si4 _t)
{
    if(m_arrow_trtr)
    {
        XD_F2 l_pos(_x - _l, (_y - _t - 768) * -1);
        m_arrow_trtr->fPositionSet(l_pos.x, l_pos.y);
        /*C4* result = C4::iFwdSearch(    &m_root,
                                        C4::TASK_TYPE_INPUT_MOVE,
                                        &l_pos);

        if(result->fSpell() == C4_IMG::iSpell())
        {
            mNOTE("Btn detected");
        }*/
    }


    return A_A;
}
//============================================================================//
si4
cGame::fImageLoad(const char* _path)
{
    m_game_c.fRdl().fDataLoad( _path, XD_WWW(cbImgLoad, this, 0x0) );
    return A_A;
}
//============================================================================//
si4
cGame::fGameLoadConfig()
{
    // Just load data, converting and parsing later.
    m_game_c.fRdl().fDataLoad(g_name_game, XD_WWW());
    return A_A;
}
//============================================================================//
si4
cGame::fResReqX()
{
    mDEV("Req");
    XD_WWW sync_lsnr(cbResComplete, this, 0x0);
    mXD_RESULTA(m_game_c.fRdl().fSyncListenerSetX( sync_lsnr ));

    fImageLoad("g/map/tree.png");
    fImageLoad("g/ui/arrow.png");
    fGameLoadConfig();

    return A_A;
}
//============================================================================//
/*struct GS_IMG_FACTOR
{
    const char* name;
    fl4 size;
    fl4 ofsx;
    fl4 ofsy;

    GS_IMG_FACTOR(const char* n, fl4 s, fl4 x, fl4 y)
        : name(n), ofsx(x), ofsy(y) {}
};*/
//============================================================================//
si4
cGame::fParserPrepareX(C4_PARSER* _parser)
{
    _parser->fDisplayMissingObjects(mISDEVELOP);

    C4_PARSER::CUSTOM custom_fabric;

    custom_fabric.fSet(XD_SPELL("image"),
                XD_WWW(CREATOR::cbCreateImage, &m_creator, 0x0));
    _parser->fAddX( custom_fabric );

    custom_fabric.fSet(XD_SPELL("transform"),
                XD_WWW(CREATOR::cbCreateTransform, &m_creator, 0x0));
    _parser->fAddX(custom_fabric);

    custom_fabric.fSet(XD_SPELL("frame"),
                XD_WWW(CREATOR::cbCreateFrame, &m_creator, 0x0));
    _parser->fAddX(custom_fabric);

    custom_fabric.fSet(XD_SPELL("geometry"),
                XD_WWW(CREATOR::cbCreateGeometry, &m_creator, 0x0));

    _parser->fAddX(custom_fabric);

    return A_A;
}
//============================================================================//
si4
cGame::fScene1Create()
{
mBLOCK("Checking resources");
    if( true != m_game_c.fRdl().fDataCheck(g_name_game))
    {// game.xml
        mFAIL_SEARCH(g_name_game);
        return X_X;
    }
    XD_DATA* p_data = m_game_c.fRdl().fDataGetP(g_name_game);
    if(0x0 == p_data)
    {
        mERRORF("Rdl fail method");
        return X_X;
    }
    pugi::xml_document l_doc;
    l_doc.load(RCAST(const char*, p_data->data));
    pugi::xml_node n_game = l_doc.first_child();
    if( !n_game )
    {
        mERRORF("Game xml document not valid");
        return X_X;
    }

mBLOCK("Configuring");
    XD_F4 cl_color;
    XD::gSi4ToF4(0x271e0900, 255, cl_color.fGet());
    m_game_c.fGfx().fClearColor(cl_color);

mBLOCK("Prepare loading conf");
    C4_PARSER l_parser(10);

    fParserPrepareX(&l_parser);

mBLOCK("Parsing");
    C4_PARSER::VALUES target;
    target.parent = &m_root;
    mXD_RESULTA(l_parser.fParseX(n_game, target));
    if(0x0 == target.result)
    {
        mERRORF("while loading the scene elements. Scene is empty");
        return X_X;
    }
    m_root.fChildAdd(target.result);
    m_transform = &m_root.fTransform();

mBLOCK("Prepare scene");
    fWorldTrReset();

    return A_A;
}
//============================================================================//
si4
cGame::fImageLoadedX(const XD::RDL::DATA_ON_LOAD* resource)
{
    if(!resource || !resource->data->data_size)
        mXD_ERROR("loading image", return X_X;);

    XD::GFX_DECODER::BITMAP bm(resource->data->data, resource->data->data_size);
    mXD_RESULTA(m_game_c.fGfxDecoder().fBitmapInitX(bm));
    mXD_RESULTA(m_game_c.fGfxDecoder().fBitmapDecodeX(bm));
    XD::GFX::TEXTURE* texture = m_game_c.fGfx().fTextureCreateP(
        bm.bmp, bm.bmp_size, bm.size.x, bm.size.y, XDE_PIXEL_r8g8b8a8
    );
    if(!texture)
    {
        mERRORF("image decoding");
        return X_X;
    }
    //m_texture_man[resource->path] = texture;
    m_creator.fTextureAdd(resource->path, texture);

    return A_A;
}
//============================================================================//
XD::GFX::TEXTURE*
cGame::fTextureGetP(const char* name)
{
    mOLDFUNC("Function deleted");
    return 0x0;
}
//============================================================================//
si4
cGame::fResourcesCompleteX()
{
    return fScene1Create();
}
//============================================================================//
// Cbacks.
//============================================================================//
sip
cGame::cbDrawListener(void* who, void* why)
{
    cGame* in = SCAST(cGame*, who);
    if(in)
    {
        if(A_A != in->fDrawX())
        {
            mERRORF("drawing");
            in->fEndX();
            return X_X;
        }
    }
    return A_A;
}
//============================================================================//
sip
cGame::cbImgLoad(void* who, void* why)
{
    cGame* to = SCAST(cGame*, who);
    XD::RDL::DATA_ON_LOAD* in = SCAST(XD::RDL::DATA_ON_LOAD*, why);
    if(to && in)
    {
        if(A_A != to->fImageLoadedX(in))
        {
            mERRORF("img loading");
            to->fEndX();
            return X_X;
        }
    }
    return A_A;
}
//============================================================================//
sip
cGame::cbKeyListener(void* who, void* why)
{
    XD::OS::INPUT_KEY* in = SCAST(XD::OS::INPUT_KEY*, why);
    cGame* to = SCAST(cGame*, who);
    if(to && in)
    {
        mXD_RESULT(to->fKeyX(in), mERRORF(""));
    }
    return A_A;
}
//============================================================================//
sip
cGame::cbMouseListener(void* who, void* why)
{
    XD_I4* m_pos = SCAST(XD_I4*, why);
    cGame* owner = SCAST(cGame*, who);
    owner->fMouseX(m_pos->x, m_pos->y, m_pos->z, m_pos->w);
    return A_A;
}
//============================================================================//
sip
cGame::cbResComplete(void* who, void* why)
{
    cGame* in = SCAST(cGame*, who);
    if(in)
    {
        si4 result = in->fResourcesCompleteX();
        if(A_A != result)
            in->fEndX();
    }
    return A_A;
}
//============================================================================//
void
cGame::fChildAdd(C4* _child)
{
    m_root.fChildAdd(_child);
}
//============================================================================//
void
cGame::fWorldTrReset()
{
    if(0x0 != m_transform)
    {
        m_transform->fScaleSet(2./m_app_size.x, 2./m_app_size.y, 1);
        m_transform->fPositionSet(-m_app_size.x/2., -m_app_size.y/2., 0);
    }
}
//============================================================================//
XD_F4
cGame::fHelpScreen4(fl4 _ox, fl4 _oy, fl4 _dx, fl4 _dy)
{
    return XD_F4(m_app_size.x * _ox + _dx, m_app_size.y * _oy + _dy, 0, 0);
}
//============================================================================//
void
cGame::fCursorSet(C4_IMG* _img)
{
    m_cursor = _img;
    m_arrow_trtr = &m_cursor->fTransform();
}
//============================================================================//
sip
cGame::cbCreateGame(void* _who, void* _why)
{
    if(!_why) return 0x0;
mBLOCK("Who");
    cGame* thiz = RCAST(cGame*, _who);
mBLOCK("Why");
    C4_PARSER::VALUES& val = *RCAST(C4_PARSER::VALUES*, _why);

    val.result = C4_POS::iNewP();

    return A_A;
}
//============================================================================//
sip
cGame::cbCreateCursor(void* _who, void* _why)
{// <as_cursor>
    cGame* thiz = RCAST(cGame*, _who);
    C4_PARSER::VALUES& inn = *RCAST(C4_PARSER::VALUES*, _why);

    C4_IMG* image = RCAST(C4_IMG*, inn.parent);
    thiz->fCursorSet(image);

    return A_A;
}
//============================================================================//





