#ifndef _CREATOR_H_INCLUDED
#define _CREATOR_H_INCLUDED

#include "COMMON/C4/C4.h"
#include "COMMON/C4/Samples/C4_FRAME2.h"
#include "COMMON/C4/Parser/xml/C4_PARSER.h"
#include "MODULES/RDL/XD_RDL.h"

class CREATOR {
    typedef std::map<XD_STRING, XD::GFX::TEXTURE*>                texture_map_t;
    typedef std::map<XD_STRING, XD::GFX::TEXTURE*>::iterator texture_map_iter_t;
    typedef std::pair<XD_STRING, XD::GFX::TEXTURE*>              texture_pair_t;

protected:
    XD::GFX&    m_gfx;
    XD::RDL&    m_rdl;

    texture_map_t       m_texture_man;

public:
    CREATOR(XD::GFX& _gfx, XD::RDL _rdl);

    void fTextureAdd(const XD_STRING& _tex_name, XD::GFX::TEXTURE* _texture);
    XD::GFX::TEXTURE* fTextureGet(const char* _tex_name);

    XD::GFX&    fGfx() { return m_gfx; }
    void f() {} // Antiwarning.

    static mWWW_DECLARE(cbCreateButton);
    static mWWW_DECLARE(cbCreateImage);
    static mWWW_DECLARE(cbCreateImageOld);
    static mWWW_DECLARE(cbCreateFrame);
    static mWWW_DECLARE(cbCreateSettings);
    static mWWW_DECLARE(cbCreateTransform);
    static mWWW_DECLARE(cbCreateGeometry);
};

#endif // _CREATOR_H_INCLUDED
