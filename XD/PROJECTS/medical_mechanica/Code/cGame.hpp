#ifndef   _CGAME_H
#define   _CGAME_H
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Author: UCKUHT
// Date: 07 01 14
// Desc: Main game class.
//============================================================================//
#include "BASE/XD_I2.h"

#include <map>

#include "EXT/CASUAL/XD_GAME.h"
#include "BASE/XD_MAIN.h"
#include "CASUAL/XD_FRAME.h"
#include "COMMON/C4/samples/C4_FRAME2.h"
#include "COMMON/C4/samples/C4_GIGLET.h"
#include "COMMONC4/samples/C4_POS.h"

#include "Code/Creator.h"
//============================================================================//
class cGame
{
    XD_TRTR*            m_transform;
    XD_TRTR*            m_arrow_trtr;
    C4_POS              m_root;
    XD::GAME_C          m_game_c;
    XD::GFX::TEXTURE*   m_texture;
    XD_I2               m_scr_size;
    XD_I2               m_app_size;
    fl4                 m_aspect;
    CREATOR             m_creator;
    C4_IMG*             m_cursor;

public:// Manag.
             cGame();
    si4      fInitX();
    si4      fStartX();
    si4      fEndX();
    si4      fDrawX();
    si4      fKeyX(XD::OS::INPUT_KEY* event);
    si4      fMouseX(si4 _x, si4 _y, si4 _l, si4 _t);
    XD::GFX::TEXTURE* fTextureGetP(const char* name);
    void     fChildAdd(C4* _child);
    void     fWorldTrReset();
    void     fTransformSet( XD_TRTR* _transform ) { m_transform = _transform; }
    XD_TRTR* fTransformGet() { return m_transform; }
    void     fCursorSet(C4_IMG* _IMG);
    CREATOR& fCreator() { return m_creator; }

public:// Helpers.
    XD_F4   fHelpScreen4(fl4 _ox, fl4 _oy, fl4 _dx = 0, fl4 _dy = 0);
    si4     fParse(void* _data, szt _size);
    si4     fParserPrepareX(C4_PARSER* _parser);

public:// Resources.
    si4     fResReqX();
    si4     fImageLoadedX(const XD::RDL::DATA_ON_LOAD* res);
    si4     fResourcesCompleteX();
    si4     fScene1Create();
    C4_IMG* fStaticImageCreateP(const char* name);
    si4     fImageLoad(const char* _path);
    si4     fGameLoadConfig();

private:// Cb.
    static sip cbKeyListener(void* who, void* why);
    static sip cbMouseListener(void* who, void* why);
    static sip cbDrawListener(void* who, void* why);
    static sip cbImgLoad(void* who, void* why);
    static sip cbResComplete(void* who, void* why);

private:// Fabric.
    static sip cbCreateGame(void* _who, void* _why);
    static sip cbCreateCursor(void* _who, void* _why);
};
//============================================================================//
//============================================================================//



#endif	/* CGAME_H */

