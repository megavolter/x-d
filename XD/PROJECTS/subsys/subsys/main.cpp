#include <stdlib.h>

#include "OS/XD_OS.h"
#include "BASE/LANG.h"
#include "BASE/CODE.h"
#include "BASE/LOG.h"
#include "BASE/CAST.h"
#include "GFX/XD_GFXEX.h"
#include "GFX_DECODER/XD_GFX_DECODER.h"
#include "RDL/XD_RDL.h"
#include "BASE/XD_MATH.h"
#include "BASE/XD_TRTR.h"
#include "BASE/XD_MATH3D.h"

//============================================================================//
XD::WDGT*           w_t = 0x0;
XD::OS              g_o;
XD::GFXEX           g_g;
XD::GFX_DECODER     g_d;
XD::GFX::TEXTURE*   g_t = 0x0;
XD::RDL             g_l;

XD::GFX::DATA5*      raster = 0x0;
XD::GFX::TEXTURE*    texture = 0x0;
XD::GFX::PROGRAM*    shade = 0x0;
XD::GFX::PROGRAM*    shade1 = 0x0;
XD::GFXEX::SETTINGS  settings;

XD_TRTR              trtr;
XD_F44               global;

const char resource[] = "resources/image.png";
//============================================================================//
sip
gKeyDown(void* object, void* data)
{
    XD::OS::XS_INPUT_KEY* key = SCAST(XD::OS::XS_INPUT_KEY*, data);
    if((key) && ((key->key == XDE_KEY_SPACE) || (key->key == XDE_KEY_ESC)) )
    {//1
        mNOTE("Application exit by hot key.");
        exit(O_O);
    }//1
    return A_A;
}
//============================================================================//
sip
gInit()
{// All resources ready.
    XD_DATA* d_p = g_l.fDataGet(resource);
    if(!d_p)
    {//1
        mERRORF("data not ready");
        return X_X;
    }//1
    else
    {//1
        XD::GFX_DECODER::BITMAP b_p(d_p->data, d_p->data_size);
        if(A_A == g_d.fBitmapInit(b_p))
        {//2
           // mDEV("bmp format = "<<b_p.format<<" bmp size "<<b_p.size.x<<" "<<b_p.size.y));
            if(A_A == g_d.fBitmapDecode(b_p))
            {//3
                mDEV("bitmap decoded");
            }//3
        }//2
        g_l.fDataUnload(d_p);
        if(b_p.fEmpty())
        {//2
            mERRORF("bitmap is not decoded");
            return X_X;
        }//2
        else
        {//2
            mDEV_VAR(b_p.size.x);
            mDEV_VAR(b_p.size.y);
            g_t = g_g.fTextureCreate(b_p.bmp, b_p.bmp_size,
                                     b_p.size.x, b_p.size.y,
                                     b_p.format);
            if(g_t)
            {//3
                mDEV("texture create correct");
                g_g.fTextureSet(g_t);
                return A_A;
            }//3
            else
            {//3
                mDEV("texture create uncorrect");
                return X_X;
            }//3
        }//2
    }//1
    return X_X;
}
//============================================================================//
sip
gResLoad(void* object, void* data)
{
mBLOCK("Texture create");
    XD_DATA*  d_t = SCAST(XD_DATA*, data);
    if(d_t && d_t->fSize())
    {//1
        mDEV("loading correct");
        if(A_A != gInit())
        {//2
            mERRORF("initialization error");
            exit(X_X);
        }//2
    }//1
    else
    {//1
        mDEV("loading is incorrect");
    }//1
    return A_A;
}
//============================================================================//
si4
gShaderInit()
{
    const char vertex[] =
        "attribute vec3 a_pos; \n"
        "attribute vec2 a_tex; \n"
        "varying vec2 v_tex; \n"
        "\n"
        "void main() \n"
        "{ \n"
        "   v_tex = a_tex; \n"
        "   gl_Position = vec4(a_pos, 1.0); \n"
        "} \n"
        ;

    const char vert_vwp[] =
        "attribute vec3 a_pos; \n"
        "attribute vec2 a_tex; \n"
        "varying vec2 v_tex; \n"
        "uniform mat4 u_vwp; \n"
        "\n"
        "void main() \n"
        "{ \n"
        "   v_tex = a_tex; \n"
        "   gl_Position = u_vwp * vec4(a_pos, 1.0) ; \n"
        "} \n"
        ;

    const char fragment[] =
        "varying vec2 v_tex; \n"
        "uniform sampler2D s_0;\n"
        "void main() \n"
        "{ \n"
        "   gl_FragColor = texture2D(s_0, v_tex); \n"
        "} \n"
        ;

    shade = g_g.fProgramCreate(vertex, fragment);
    if(!shade)
        return X_X;
    shade1 = g_g.fProgramCreate(vert_vwp, fragment);
    if(!shade1)
        return X_X;

    return A_A;
}
//============================================================================//
si4
gRasterInit()
{
    static const fl4 side = 1.0f;
    static const fl4 tside = 1.f;
    static XD::GFX::V5 vertexes[4] =
    {
        {-side,     side, 0,     0,       0,      },
        {side,      side, 0,     tside,   0,      },
        {side,     -side, 0,     tside,   tside,  },
        {-side,    -side, 0,     0,       tside   },
    };

    static ui2 indeces[6] =
    {
        0,1,2,
        0,2,3,
    };

    raster = new XD::GFX::DATA5;
    raster->sSetLocal(vertexes, 4, indeces, 6);
    if(X_X == g_g.fData5Compose(raster))
    {//2
        mERRORF("data compose");
        return X_X;
    }//2

    return A_A;
}
//============================================================================//
si4
gTextureGen()
{
    const szt edge = 128;
    const szt pix_cnt = edge*edge;
    ui4 bmp[edge*edge];
    szt bmp_size = pix_cnt * sizeof(ui4);
    for(szt i =0; i < pix_cnt; i++)
    {
        //bmp[i] = i*i*64;
        //bmp[i] = i*i*4;
        //bmp[i] = i*i*8;
        //bmp[i] = i*i*16;
        //bmp[i] = 0xaa332211 + i*i*32;
        bmp[i] = 0x00888888 + 63.5*sin( i ) + 63.5*sin( i );
    }
    texture = g_g.fTextureCreate(bmp, bmp_size, edge, edge, XDE_PIXEL_r8g8b8a8);
    if(!texture)
        return X_X;

    return A_A;
}
//============================================================================//
si4
gWidgetInit()
{
    g_o.fInputKeyListenerSet(gKeyDown, NULL);

    w_t = XD::OS::fWdgtCreate();
    if(!w_t)
        return X_X;

    return XD::OS::fWdgtInit("wdgt name", 0, 0, 1024, 768, w_t);
}
//============================================================================//
fl4 updater = 1;
si4
gStep()
{
    mXDRESULTA(g_l.fSync());
    mBLOCK("one");
        mXDRESULTA(g_g.fProgramSet(shade));
        mXDRESULTA(g_g.fData5Set(raster));
        if(g_t){
            mXDRESULTA(g_g.fTextureSet(g_t));
        }
        else{
            mXDRESULTA(g_g.fTextureSet(texture));
        }
        mXDRESULTA(g_g.fSettingsSet(&settings));
       // mXDRESULTA(g_g.fRasterize());
    mBLOCK("two");
        mXDRESULTA(g_g.fProgramSet(shade1));
        mXDRESULTA(g_g.fData5Set(raster));
        // Same texture.
        mXDRESULTA(g_g.fSettingsSet(&settings));
        mat4_translate(&global, 0.1,0,0);
        mXDRESULTA(g_g.fProgramVF44(global, "u_vwp", 0));
        mXDRESULTA(g_g.fRasterize());

    return A_A;
}
//============================================================================//
si4
gGraphicsInit()
{
    si4 d_c = XD::OS::fWdgtGetDescriptor(w_t);
    if(A_A != g_g.fInit(d_c))
        return (X_X);
    XD_COLOR clear(0,0.5,0.5,0.5);
    g_g.fClearColor(clear);

    return A_A;
}
//============================================================================//
si4
gResourcesLoad()
{
    g_l.fDataLoad(resource, XD_WWW(gResLoad,0x0));
    return A_A;
}
//============================================================================//
int
main()
{
    settings.blend_mode = XDE_BLEND_NORMAL;
    mat4_set_identity(RCAST(mat4*, &global));
    g_g.fDepth(false);

    mNOTE("Starting");
    if(A_A == gWidgetInit())
    {//1 Widget.
        if(A_A == gGraphicsInit())
        {//2 Graphics.
            if(A_A == gShaderInit())
            {//3 Shader.
                if(A_A == gRasterInit() && A_A == gTextureGen())
                {//4 Texture.
                    if(A_A == gResourcesLoad())
                    {//5 Resouces.
                        while(true)
                        {//6 Main loop.
                            g_l.fSync();
                            g_g.fSceneBegin();
                            g_g.fSceneClear();
                            mXDRESULTA(gStep());
                            g_g.fSceneEnd();
                            g_o.fSync();
                        }//6
                        return A_A;
                    }//5
                    else
                    {//5
                        mERRORF("loading resources");
                    }//5
                }//4
                else
                {//4
                    mERRORF("raster or texture generating");
                }//4
            }//3
            else
            {//3
                mERRORF("shader creation");
            }//3
        }//2
        else
        {//2
            mERRORF("render init");
        }//2
    }//1
    else
    {//1
        mERRORF("widget init");
    }//1
    return X_X;
}
//============================================================================//
