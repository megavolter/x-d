TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += ../../../ \
	../../../MODULES \
	../../../EXT \

SOURCES += main.cpp \
	../../../BASE/STRINGS/XD_STRING.cpp \
	../../../BASE/XD.cpp \
	../../../BASE/XD_F44.cpp \
	../../../BASE/XD_MATH3D.cpp \
	../../../BASE/XD_TRTR.cpp \
	../../../GAMES/Match3/TheMatch3.cpp \
	../../../MODULES/OS/win/XD_OS.cpp \
	../../../MODULES/OS/win/XD_WDGT.cpp \
	../../../EXT/CASUAL/XD_GFXEX.cpp \
	../../../MODULES/GFX/XD_GFX.cpp \
	../../../EXT/CASUAL/CASUAL_PROGRAMS_BANK.cpp \
	../../../GAMES/Match3/TheMainScreen.cpp \
	../../../GAMES/Match3/TheTextureManager.cpp \
	../../../MODULES/RDL/XD_RDL.cpp \
	../../../BASE/XD_COLOR.cpp \
	../../../MODULES/GFX_DECODER/XD_GFX_DECODER.cpp \
	../../../SOLUTIONS/zlibpng/adler32.c \
	../../../SOLUTIONS/zlibpng/compress.c \
	../../../SOLUTIONS/zlibpng/crc32.c \
	../../../SOLUTIONS/zlibpng/deflate.c \
	../../../SOLUTIONS/zlibpng/gzclose.c \
	../../../SOLUTIONS/zlibpng/gzlib.c \
	../../../SOLUTIONS/zlibpng/gzread.c \
	../../../SOLUTIONS/zlibpng/gzwrite.c \
	../../../SOLUTIONS/zlibpng/infback.c \
	../../../SOLUTIONS/zlibpng/inffast.c \
	../../../SOLUTIONS/zlibpng/inflate.c \
	../../../SOLUTIONS/zlibpng/inftrees.c \
	../../../SOLUTIONS/zlibpng/png.c \
	../../../SOLUTIONS/zlibpng/pngerror.c \
	../../../SOLUTIONS/zlibpng/pngget.c \
	../../../SOLUTIONS/zlibpng/pngmem.c \
	../../../SOLUTIONS/zlibpng/pngpread.c \
	../../../SOLUTIONS/zlibpng/pngread.c \
	../../../SOLUTIONS/zlibpng/pngrio.c \
	../../../SOLUTIONS/zlibpng/pngrtran.c \
	../../../SOLUTIONS/zlibpng/pngrutil.c \
	../../../SOLUTIONS/zlibpng/pngset.c \
	../../../SOLUTIONS/zlibpng/pngtrans.c \
	../../../SOLUTIONS/zlibpng/pngwio.c \
	../../../SOLUTIONS/zlibpng/pngwrite.c \
	../../../SOLUTIONS/zlibpng/pngwtran.c \
	../../../SOLUTIONS/zlibpng/pngwutil.c \
	../../../SOLUTIONS/zlibpng/trees.c \
	../../../SOLUTIONS/zlibpng/uncompr.c \
	../../../SOLUTIONS/zlibpng/zutil.c \
    ../../../EXT/C4/Samples/C4_IMG.cpp \
    ../../../EXT/C4/C4.cpp \
    ../../../EXT/C4/Samples/C4_POS.cpp \
    ../../../GAMES/Match3/TheM3Shaders.cpp \
    ../../../GAMES/Match3/TheShaderManager.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
	../../../BASE/STRINGS/STRINGS.h \
	../../../BASE/STRINGS/XD_STRING.h \
	../../../BASE/CAST.h \
	../../../BASE/CODE.h \
	../../../BASE/LANG.h \
	../../../BASE/LOG.h \
	../../../BASE/TYPE.h \
	../../../BASE/VECTOR.h \
	../../../BASE/XD.h \
	../../../BASE/XD_ATL_FRM.h \
	../../../BASE/XD_BT4.h \
	../../../BASE/XD_COLOR.h \
	../../../BASE/XD_D3.h \
	../../../BASE/XD_DATA.h \
	../../../BASE/XD_DIFFUSE.h \
	../../../BASE/XD_F2.h \
	../../../BASE/XD_F4.h \
	../../../BASE/XD_F44.h \
	../../../BASE/XD_I2.h \
	../../../BASE/XD_I4.h \
	../../../BASE/XD_MAIN.h \
	../../../BASE/XD_MATH.h \
	../../../BASE/XD_MATH3D.h \
	../../../BASE/XD_POINT2.h \
	../../../BASE/XD_POINT4.h \
	../../../BASE/XD_SPELL.h \
	../../../BASE/XD_STRING.h \
	../../../BASE/XD_TRPR.h \
	../../../BASE/XD_TRTR.h \
	../../../BASE/XD_VECTOR2.h \
	../../../BASE/XD_WWW.h \
	../../../GAMES/Match3/TheMatch3.h \
	../../../MODULES/OS/XD_OS.h \
	../../../MODULES/OS/win/XD_WDGT.h \
	../../../EXT/CASUAL/XD_GFXEX.h \
	../../../EXT/C4/Parser/xml/C4_PARSER.h \
	../../../EXT/C4/Parser/xml/C4_PARSER_IMPL.h \
	../../../EXT/C4/Samples/C4_BTN_CLR.h \
	../../../EXT/C4/Samples/C4_FRAME.h \
	../../../EXT/C4/Samples/C4_GIGLET.h \
	../../../EXT/C4/Samples/C4_POS.h \
	../../../EXT/C4/C4.h \
	../../../EXT/CASUAL/CASUAL_PROGRAMS_BANK.h \
	../../../EXT/CASUAL/XD_FRAME.h \
	../../../EXT/CASUAL/XD_GAME.h \
	../../../EXT/CASUAL/XD_MESH.h \
	../../../MODULES/GFX/add/glcorearb.h \
	../../../MODULES/GFX/add/glext.h \
	../../../MODULES/GFX/add/wglext.h \
	../../../MODULES/GFX/XD_GFX.h \
	../../../GAMES/Match3/TheMainScreen.h \
	../../../GAMES/Match3/TheTextureManager.h \
	../../../MODULES/RDL/XD_RDL.h \
	../../../MODULES/GFX_DECODER/XD_GFX_DECODER.h \
	../../../SOLUTIONS/zlibpng/crc32.h \
	../../../SOLUTIONS/zlibpng/deflate.h \
	../../../SOLUTIONS/zlibpng/gzguts.h \
	../../../SOLUTIONS/zlibpng/inffast.h \
	../../../SOLUTIONS/zlibpng/inffixed.h \
	../../../SOLUTIONS/zlibpng/inflate.h \
	../../../SOLUTIONS/zlibpng/inftrees.h \
	../../../SOLUTIONS/zlibpng/png.h \
	../../../SOLUTIONS/zlibpng/pngconf.h \
	../../../SOLUTIONS/zlibpng/pngdebug.h \
	../../../SOLUTIONS/zlibpng/pnginfo.h \
	../../../SOLUTIONS/zlibpng/pnglibconf.h \
	../../../SOLUTIONS/zlibpng/pngpriv.h \
	../../../SOLUTIONS/zlibpng/pngstruct.h \
	../../../SOLUTIONS/zlibpng/trees.h \
	../../../SOLUTIONS/zlibpng/zconf.h \
	../../../SOLUTIONS/zlibpng/zlib.h \
	../../../SOLUTIONS/zlibpng/zutil.h \
    ../../../EXT/C4/Samples/C4_IMG.h \
    ../../../GAMES/Match3/TheM3Shaders.h \
    ../../../GAMES/Match3/TheShaderManager.h \
    ../../../MODULES/GFX_V2/GFX_RENDER.h \
    ../../../MODULES/GFX_V2/XD_GFX.h \
    ../../../MODULES/GFX_V2/XD_GFX_SHADER.h


win32: LIBS += -L$$PWD/../../../MODULES/GFX/add/ -lglext

INCLUDEPATH += $$PWD/../../../MODULES/GFX/add
DEPENDPATH += $$PWD/../../../MODULES/GFX/add

win32: LIBS += -lgdi32

win32: LIBS += -lglu32

win32: LIBS += -lopengl32
