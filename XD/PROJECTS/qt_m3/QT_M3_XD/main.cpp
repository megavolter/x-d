#include "GAMES/Match3/TheMatch3.h"
#include "BASE/LANG.h"
//============================================================================//
int
main()
{
mBLOCK("Prepare to start");
	TheMatch3::Config config;
	config.type = TheMatch3::Config::Stand_by;
	TheMatch3 match3;

mBLOCK("Initializing game");
	mXD_RESULTA(match3.fInitX(config));

mBLOCK("Start");
	if(A_A != match3.fRunX())
	{
		mERRORF("Start game failed!");
		return X_X;
	}
	return 0x0;
}
//============================================================================//
