#include "XD_GAME.h"
#include "BASE/XD_MAIN.h"
#include "BASE/CAST.h"
#include "XD_FRAME.h"

//============================================================================//
// Data block.
XD::GAME_C*         game = 0;
XD::GFX::TEXTURE*   image = 0;
XD::FRAME5          frame;
//============================================================================//
XD_WWW_DECLARE(gDrawFunc)
{
    frame.fDraw(game->fGfx());
    return A_A;
}
//============================================================================//
XD_WWW_DECLARE(gKeyFunc)
{
    XD::OS::INPUT_KEY* in = SCAST(XD::OS::INPUT_KEY*, why);
    if(in)
    {
        switch(in->key)
        {
        case XDE_KEY_ESC:
            mNOTE("game stoped by hot key");
            game->fStop();
            break;
        case XDE_KEY_LEFT:
            frame.fTransform().fMove(-0.1, 0, 0);
            break;
        }
    }

    return A_A;
}
//============================================================================//
si4
gMeshGenerate()
{
    frame.fTextureSet(image, true);
    mXDRESULTA(frame.fInit(0,0,1,1));
    mXDRESULTA(game->fGfx().fData5Compose(&frame.fRaster()));
    game->fGfx().fClearColor(XD_F4(0.2,0.2,0.2,0.2));
    frame.fTransform().fPositionSet(0.5,0,0);
    return A_A;
}
//============================================================================//
si4
gResComplite()
{
    XD_DATA* data = game->fRdl().fDataGet("Resources/image.png");
    if(data && data->data_size)
    {
        XD::GFX_DECODER::BITMAP bm(data->data, data->data_size);
        mXDRESULTA(game->fGfxDecoder().fBitmapInit(bm));
        mXDRESULTA(game->fGfxDecoder().fBitmapDecode(bm));
        image = game->fGfx().fTextureCreate(bm.bmp, bm.bmp_size,
                                            bm.size.x, bm.size.y,
                                            XDE_PIXEL_r8g8b8a8);
        mXDRESULTA(gMeshGenerate());
        return A_A;
    }
    mERRORF("bad data loaded");
    return X_X;
}
//============================================================================//
XD_WWW_DECLARE(gRdlSyncListener)
{
    if(why)
    {
        XD::RDL::DATA_ON_LOAD* don = SCAST(XD::RDL::DATA_ON_LOAD*, why);
        const char* res_name = don->path.fGet();
        mDEV_VAR(res_name);
    }
    else
    {
        return gResComplite();
    }
    return A_A;
}
//============================================================================//
si4
gGameInit()
{
    XD_WWW www(gRdlSyncListener, 0x0);
    game->fRdl().fSyncListenerSet( www );
    game->fRdl().fDataLoad("Resources/image.png", www);
    game->fRdl().fDataLoad("Resources/half.png", www);
    return A_A;
}
//============================================================================//
XD_MAIN_DECLARE()
{
    mNOTE("Start up.");
    delete game;
    game = new XD::GAME_C;

    XD::GAME_C::INITIAL ini_struct;
    ini_struct.wdgt_name = "sampler";
    ini_struct.wdgt_frame.fSet(100,100,1024,768);
    ini_struct.draw_listener.fSet(0x0, gDrawFunc, 0x0);
    ini_struct.key_listener.fSet(0x0, gKeyFunc, 0x0);

    mXDRESULTA(game->fInit(ini_struct));
    mXDRESULTA(gGameInit());
    mXDRESULTA(game->fStart());
    mNOTE("Shutdown.");
    return A_A;
}
//============================================================================//

















