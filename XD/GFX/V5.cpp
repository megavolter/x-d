#include "V5.h"

using namespace nXD;

V5&
V5::fSetA(V5& _value)
{
	*this = _value;
	return *this;
}

V5&
V5::fPositionSetA(fl4 _x, fl4 _y, fl4 _z)
{
	x = _x;
	y = _y;
	z = _z;
	return *this;
}

V5&
V5::fTexelSetA(fl4 _u, fl4 _v)
{
	u = _u;
	v = _v;
	return *this;
}
