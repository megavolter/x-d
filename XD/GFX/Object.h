#pragma once
#include "GFX/XD_GFX.h"
#include "GFX/V5.h"
#include "BASE/XD_TRTR.h"
namespace nXD{

struct Object
{
	nXD::GFX::TEXTURE	texture;
	nXD::GFX::PROGRAM	program;
	V5O					object;
	nXD::TransformF		transform;
	nXD::GFX::SETTINGS	settings;

	void fDrop() {
		program.fClear();
		settings.blend = XDE_BLEND_NONE;
		settings.use_alpha = false;
		settings.sample = XDE_SAMPLE_NONE;
	}
};
}// nXD
