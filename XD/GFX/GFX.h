#pragma once

#include "BASE/LANG.h"
#include "BASE/CODE.h"
#include "BASE/XD_COLOR.h"
#include "BASE/STRINGS/XD_STRING.h"
#include "BASE/XD_F2.h"
#include "BASE/XD_I2.h"
#include "BASE/XD_BITMAP.h"
#include "BASE/XD_SPELL.h"

#include "V5.h"

namespace nXD{

class GFX : public Platform::GFX_IMPL
{

public xd_types:

	class MODULE
	{
	public:
		Platform::GFX_IMPL* m_impl;

		public xd_functional:

		x_result fInit(Platform::GFX_IMPL* _init) {
				m_impl = _init; return A_A;
			}
	};

	class FRAME : public MODULE
	{
	public:
		x_result fOpenX();
		x_result fCloseX();
		x_result fShowX() const;
		x_result fClearX(bln _color, bln _stencil, bln _depth) const;
		x_result fClearColorSetX(const XD_COLOR& _color);
	};

	struct SETTINGS
	{
		XDE_BLEND blend;
		bln use_alpha;
		XDE_SAMPLE sample;
		bln two_side;
	};

	class RENDERER : public MODULE
	{
		mXD_WTF("Rm mutable after newosoft review");
		mutable num m_elements_num;
	public xd_functional:
		x_result fCompileX(V5O& _target);
		x_result fRasterizeX() const;
		x_result fSetGeometryX(const V5O& _target) const;
		x_result fTestDraw();
	};

	struct PROGRAM : public Platform::GF_PROGRAM_IMPL
	{
		XD_SPELL id;
		XD_STRING vertex;
		XD_STRING fragment;
	public xd_functional:
		bln fIsValid(){
			return id.value != 0 && vertex.fSize() != 0 && fragment.fSize() != 0;
		}
	};

	class SHADER : public MODULE
	{
		bln m_enable;
		PROGRAM m_curent;
	public xd_functional:
		~SHADER() {m_curent.fClear();}
		bln& fEnableA() {return m_enable;}
		x_result fCompileX(PROGRAM& _program);
		x_result fSetX(PROGRAM& _program);
		// fVarX functions must be used AFTER fSet(programm).
		x_result fVarX(nXD::F44 _var, str _name, si4 _id);
	};

	struct TEXTURE : public Platform::GF_TEXTURE_IMPL
	{
		// Size in pixels.
		nXD::I2			size;
		// Real used size in texture. Sometimes not equal whith size.
		nXD::I2			real;
		// Effective space into texture.
		nXD::F2			uses;
		// Texture pixel format
		XDE_PIXEL		format;
		// The bitmap data. Only for access to pixels. Default NULL.
		void*			bitmap;
		szt				bitmap_size;
	};

	class SAMPLER : public MODULE
	{
		TEXTURE& m_curent;
		TEXTURE m_default_texture;
	public xd_functional:
		SAMPLER() : m_curent(m_default_texture) {}
		TEXTURE* fCreateP(void* _data, szt _size, si4 _w, si4 _h, XDE_PIXEL _format);
		x_result fCompileX(nXD::BITMAP& _bmp, out TEXTURE& _result);
		x_result fSetX(const TEXTURE& _texture, szt _chanel) const;
	};

public xd_functional:
	x_result fInitX(sip _handler);
	x_result fDestroyX();

public xd_accessors:
	FRAME& fFrameA() {return m_frame;}
	const FRAME& fFrameCA() const {return m_frame;}
	RENDERER& fRenderA() {return m_render;}
	const RENDERER& fRenderCA() const {return m_render;}
	SHADER& fShaderA() {return m_shader;}
	SAMPLER& fSamplerA() {return m_sampler;}
	const SAMPLER& fSamplerCA() const {return m_sampler;}

private xd_data:
	FRAME m_frame;
	RENDERER m_render;
	SHADER m_shader;
	SAMPLER m_sampler;
};

} // ns nXD nGFX
