#pragma once

#include <windows.h>
#include <GL/glew.h>

#include "BASE/CODE.h"
#include "BASE/TYPE.h"
#include "BASE/XD_COLOR.h"
#include "BASE/CAST.h"
#include "BASE/LANG.h"

namespace nXD{

namespace Platform{

	struct GFX_IMPL
	{
	public xd_data:
		friend class GFX;
		HWND _hwnd;
		HDC _hdc;
		HGLRC _glc;
		XD_COLOR _clear_color;

	public xd_functional:
		static GFX_IMPL* iCreateP() { return new GFX_IMPL; }
		static void iDestroy(GFX_IMPL* _target) { delete _target; }
	};

	struct GF_OBJECT_IMPL
	{
		GLuint buffer[2];
		static GF_OBJECT_IMPL* iCreate() { return new GF_OBJECT_IMPL; }
		static void iDestroy(GF_OBJECT_IMPL* _target) { delete _target; }
		GF_OBJECT_IMPL() {buffer[0] = -1; buffer[1] = -1;}
	};

	struct GF_PROGRAM_IMPL
	{
		GLuint _program;
		GLuint _vertex;
		GLuint _fragment;
	public xd_functional:
		void
		fClear() {
			_program = _vertex = _fragment = 0;
		}

		static GF_PROGRAM_IMPL*
		iCreateP(){
			return new GF_PROGRAM_IMPL;
		}

		static void
		iDestroy(GF_PROGRAM_IMPL* _target) {
			delete _target;
		}
	};

	struct GF_TEXTURE_IMPL
	{
		GLuint _texture;

		static GF_TEXTURE_IMPL*
		iCreateP(){
			return new GF_TEXTURE_IMPL;
		}

		static void
		iDestroy(GF_TEXTURE_IMPL* _target) {
			delete _target;
		}

	};

	static str
	iCheckError()
	{
		GLenum gl_error = glGetError();
		switch(gl_error)
		{
		case GL_NO_ERROR: return 0x0;
		mXD_CASE_TO_STR(GL_INVALID_ENUM);
		mXD_CASE_TO_STR(GL_INVALID_VALUE);
		//mXD_CASE_TRANS(GL_INVALID_INDEX);
		mXD_CASE_TO_STR(GL_INVALID_OPERATION);
		mXD_CASE_TO_STR(GL_STACK_OVERFLOW);
		mXD_CASE_TO_STR(GL_OUT_OF_MEMORY);
		//mXD_CASE_TRANS(GL_INVALID_FRAMEBUFFER_OPERATION);
		default: mERRORP(mBRACED("Unknown OpenGL error: ") << mBRACED(gl_error) );
		}
		return 0x0;
	}

	static x_result
	iErrorX()
	{
		str error = iCheckError();
		if( error != 0x0) {
			mERROR(mBRACED("OpenGL: ")<<error);
			return X_X;
		}
		return A_A;
	}

	static x_result
	iErrorX(si4 _line, str _function)
	{
		str error = iCheckError();
		if( error != 0x0) {
			mERROR("Open gl error "<<error<<" in "<<_function<<mBRACED(_line));
			return X_X;
		}
		return A_A;
	}

	static si4
	gGetCompileStatus(ui4 compila)
	{
		GLint status;
		glGetShaderiv(compila, GL_COMPILE_STATUS, &status);

		if(GL_FALSE == status)
		{
			GLint len;
			glGetShaderiv(compila, GL_INFO_LOG_LENGTH, &len);
			if(len)
			{
				static char log_text[256];
				glGetShaderInfoLog(compila, 256, &len, SCAST(GLchar*, log_text));
				mERROR(log_text);
			}
			return X_X;
		}
		return A_A;
	}

}}// ns
