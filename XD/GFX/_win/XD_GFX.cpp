#ifndef XD_WIN
#   error("Platform not supported")
#endif

#include "XD_GFX.h"
#include "XD_GFX_IMPL.hpp"
#include "BASE/CAST.h"
#include "BASE/CODE.h"

#define CHECK_ERROR() Platform::iErrorX(__LINE__,__FUNCTION__)
#define CHECK_ERROR_X() if(X_X == Platform::iErrorX(__LINE__,__FUNCTION__)) \
        { return X_X; }
using namespace nXD;

x_result
getSystemErrorX()
{
	LPVOID lpMsgBuf = 0x0;
	DWORD dw = GetLastError();
	if(!dw)
		return A_A;

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR) &lpMsgBuf,
		0, NULL );

	mERRORP(RCAST(str, lpMsgBuf));

	LocalFree(lpMsgBuf);
	return X_X;
}

x_result
GFX::fInitX(sip _handler)
{
	_hwnd = RCAST(HWND, _handler);
	_hdc = GetDC(_hwnd);
	PIXELFORMATDESCRIPTOR pfd = {
	sizeof(PIXELFORMATDESCRIPTOR),
	1,
	PFD_SUPPORT_OPENGL |
	PFD_DRAW_TO_WINDOW |
	PFD_DOUBLEBUFFER,
	PFD_TYPE_RGBA,
	16,
	0,0,0,0,0,0,
	0,0,0,
	0,0,0,0,
	16,
	0,0,
	PFD_MAIN_PLANE,
	0,0,0,0,
	};
	int pfmt = ChoosePixelFormat(_hdc, &pfd);
	WINBOOL wresult = SetPixelFormat(_hdc, pfmt, &pfd);
	mXD_CHECK(TRUE == wresult);

	_glc = wglCreateContext(_hdc);
	wresult = wglMakeCurrent(_hdc, _glc);
	if(TRUE != wresult){
		mERRORP("wglMakeCurrent fails ");
		mXCALL(getSystemErrorX());
	}

	m_frame.fInit(this);
	mXD_CHECK(glewInit() == GLEW_OK);

	return Platform::iErrorX();
}

x_result
GFX::FRAME::fOpenX()
{
	return A_A;
}

x_result
GFX::FRAME::fCloseX()
{
	return A_A;
}

x_result
GFX::FRAME::fShowX() const
{
	HDC hdc = m_impl->_hdc;
	BOOL res = SwapBuffers(hdc);
	if(TRUE != res)
	{
		mERRORF("Present frame");
		return X_X;
	}
	return Platform::iErrorX();
}

x_result
GFX::FRAME::fClearX(bln _color, bln _stencil, bln _depth) const
{
	ui4 mask =
			(_color)	? GL_COLOR_BUFFER_BIT	: 0	|
			(_stencil)	? GL_STENCIL_BUFFER_BIT : 0 |
			(_depth)	? GL_DEPTH_BUFFER_BIT	: 0	;
	glClear(mask);
	return Platform::iErrorX();
}

x_result
GFX::FRAME::fClearColorSetX(const XD_COLOR& _color)
{
	m_impl->_clear_color = _color;
	glClearColor(_color.r, _color.g, _color.b, _color.a);
	return Platform::iErrorX();
}

static bln
validate(nXD::V5O& _target)
{
	if(	_target.vx_num == 0	|| _target.ix_num == 0)
		return false;
	return true;
}

x_result
GFX::RENDERER::fCompileX(V5O& _target)
{
	if(mISDEBUG() && !validate(_target)){
		mERRORP("Vertex object is not valid");
		return X_X;
	}

	if(_target.buffer[0] == -1)
	{
		glGenBuffers(2, _target.buffer);
	}
	glBindBuffer(GL_ARRAY_BUFFER, _target.buffer[0]);
	glBufferData(GL_ARRAY_BUFFER, _target.fDataSize(), _target.fVertexP(),
			GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _target.buffer[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, _target.fIdxSize(),
			_target.fIndexP(), GL_DYNAMIC_DRAW);
	return Platform::iErrorX();
}

x_result
GFX::RENDERER::fRasterizeX() const
{
	mXD_WTF("Settings must be in settings function");
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);

	glDrawElements( GL_TRIANGLES, m_elements_num, GL_UNSIGNED_SHORT, 0);
	return Platform::iErrorX();
}

x_result
GFX::RENDERER::fSetGeometryX(const V5O& _target) const
{
	glBindBuffer(GL_ARRAY_BUFFER, _target.buffer[0]);
	m_elements_num = _target.ix_num;

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(
	   0,
	   3,
	   GL_FLOAT,
	   GL_FALSE,
	   sizeof(V5),
	   0
	);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(
	   1,
	   2,
	   GL_FLOAT,
	   GL_FALSE,
	   sizeof(V5),
	   RCAST(void*, sizeof(fl4) * 3)
	);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _target.buffer[1]);

	return Platform::iErrorX();
}

x_result
GFX::RENDERER::fTestDraw()
{

	static const GLfloat g_vertex_buffer_data[] = {
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		0.0f,  1.0f, 0.0f,
	};

	GLuint vertexbuffer;

	glGenBuffers(1, &vertexbuffer);

	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);

	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
	   0,
	   3,
	   GL_FLOAT,
	   GL_FALSE,
	   0,
	   (void*)0
	);

	glDrawArrays(GL_TRIANGLES, 0, 3);

	glDisableVertexAttribArray(0);

	return Platform::iErrorX();
}

static si4
getCompileStatus(ui4 shader)
{
	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	if(GL_FALSE == status)
	{
		GLint len;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
		if(len)
		{
			static char log_text[256];
			glGetShaderInfoLog(shader, 256, &len, SCAST(GLchar*, log_text));
			mERROR(log_text);
		}
		return X_X;
	}
	return true;
}

x_result
programError(GLuint shader)
{
	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	if(status == GL_FALSE)
	{
		GLint log_length;
		glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &log_length );

		if( log_length > 0 )
		{
			unsigned char *log = new unsigned char[log_length];
			glGetShaderInfoLog( shader, log_length, &log_length,
								(GLchar*)&log[0] );
			mERRORF("text: "<<mBRACED(log));
			delete[] log;
		}
		else mERRORF("Unknown error");
		return X_X;
	}
	return A_A;
}

x_result
GFX::SHADER::fCompileX(PROGRAM& _program)
{
	if(mISDEBUG() && (!_program.fIsValid())){
		mERRORP("Compilation error. Program isn't valid");
		return X_X;
	}

	str ppv = _program.vertex.fGet();
	GLuint v = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(v, 1, &ppv, 0);
	glCompileShader(v);
	mXCALL(programError(v));

	str ppf = _program.fragment.fGet();
	GLuint f = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(f, 1, &ppf, 0);
	glCompileShader(f);
	mXCALL(programError(f));

	GLuint p = glCreateProgram();
	glAttachShader(p, v);
	glAttachShader(p, f);
	glLinkProgram(p);

	glBindAttribLocation(p, 0, "a_pos");
	glBindAttribLocation(p, 1, "a_tex");

	CHECK_ERROR_X();

	_program._vertex = v;
	_program._fragment = f;
	_program._program = p;

	return CHECK_ERROR();
}

x_result
GFX::SHADER::fSetX(PROGRAM& _program)
{
	if(mISDEBUG() && (!_program.fIsValid())){
		mERRORP("Program not valid");
		glUseProgram(0);
		return X_X;
	}
	glUseProgram(_program._program);
	m_curent = _program;

	return CHECK_ERROR();
}

x_result
GFX::SHADER::fVarX(nXD::F44 _var, str _name, si4 _id)
{
	GLuint location = glGetUniformLocation(m_curent._program, _name);
	if(location >= 0)
		//glUniform1fv(location, XD_F44::iSizeInF4(), var.fDataP());
		glUniformMatrix4fv(location, 1, false, _var.fDataP());
	else
		mERRORP("Can't find uniform "<<mBRACED(_name));
	return CHECK_ERROR();
}

x_result
GFX::SAMPLER::fSetX(const TEXTURE& _texture, szt _chanel) const
{
	if(_texture._texture == 0x0){
		mERRORP("Invalid texture parameter");
		return X_X;
	}
	//m_curent = _texture;

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _texture._texture);
	glEnable(GL_ALPHA_TEST);

	return CHECK_ERROR();
}

static GLenum
translatePixelFormat(XDE_PIXEL fmt)
{
	switch(fmt)
	{
	case XDE_PIXEL_NONE:
	case XDE_PIXEL_:
		mWARNINGF("translated none pixel format");
		break;
	case XDE_PIXEL_r8g8b8a8:
		return GL_RGBA;
		break;
	case XDE_PIXEL_r8g8b8:
		return GL_RGB;
	}
	mERRORF("translated bad pixel format");
	return 0;
}

x_result
GFX::SAMPLER::fCompileX(nXD::BITMAP& _bmp, TEXTURE& _result)
{
	GLuint texture = 0;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	if(A_A != CHECK_ERROR()){
		mERRORP("Failed binding texture");
		return 0x0;
	}

	glTexImage2D(
			mVAR("Target", GL_TEXTURE_2D),
			mVAR("Level", 0),
			mVAR("InternalFormat", translatePixelFormat(XDE_PIXEL_r8g8b8a8)),
			mVAR("Width", _bmp.size.x),
			mVAR("Height", _bmp.size.y),
			mVAR("Border", 0),
			mVAR("Format", translatePixelFormat(XDE_PIXEL_r8g8b8a8)),
			mVAR("Type", GL_UNSIGNED_BYTE),
			mVAR("Data", _bmp.bmp)
	);

	if(A_A != CHECK_ERROR()){
		mERRORP("Texture creation");
		mXD_WTF("Release texture when error!");
		return 0x0;
	}

	_result.size.fSet(_bmp.size.x, _bmp.size.y);
	_result.real.fSet(_bmp.size.x, _bmp.size.y);
	_result.uses.fSet(1.f,1.f);
	_result._texture = texture;
	return A_A;
}

GFX::TEXTURE*
GFX::SAMPLER::fCreateP(void* _data, szt _size, si4 _w, si4 _h, XDE_PIXEL _format)
{
	mOLDFUNC("This method is deprecated now, use fTextureCompileX instead!");
	GLuint texture = 0;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	if(A_A != CHECK_ERROR()){
		mERRORP("Failed binding texture");
		return 0x0;
	}

	glTexImage2D(
			mVAR("Target", GL_TEXTURE_2D),
			mVAR("Level", 0),
			mVAR("InternalFormat", translatePixelFormat(_format)),
			mVAR("Width", _w),
			mVAR("Height", _h),
			mVAR("Border", 0),
			mVAR("Format", translatePixelFormat(_format)),
			mVAR("Type", GL_UNSIGNED_BYTE),
			mVAR("Data", _data)
	);
	if(A_A != CHECK_ERROR()){
		mERRORP("Texture creation");
		mXD_WTF("Release texture");
		return 0x0;
	}

	GFX::TEXTURE* result = new GFX::TEXTURE;
	result->size.fSet(_w, _h);
	result->real.fSet(_w, _h);
	result->uses.fSet(1.f,1.f);
	result->_texture = texture;

	return result;
}


