#pragma once

#include "BASE/TYPE.h"
#include "BASE/CODE.h"
#include "BASE/XD_TRTR.h"

#include "_win/XD_GFX_IMPL.hpp"

namespace nXD{
struct V5
{
	fl4 x,y,z,u,v;

public xd_functional:
	void fClear() {x=y=z=u=v=0;}
	static V5* fCreateP(szt _cnt);
	static void fDestroy(V5* _target);
	V5& fSetA(V5& _value);
	V5& fPositionSetA(fl4 _x, fl4 _y, fl4 _z);
	V5& fTexelSetA(fl4 _u, fl4 _v);
};


struct V5O : public Platform::GF_OBJECT_IMPL
{
private xd_data:
	V5* vx;
	ui2* ix;
public xd_data:
	num vx_num;
	num vx_alc;
	num ix_num;
	num ix_alc;

	V5O() { fClear(); }
	~V5O() {
		delete[] ix;
		delete[] vx;
	}

	V5* fVertexP() {return vx;}
	ui2* fIndexP() {return ix;}

	void
	fClear() {
		vx = 0x0;
		ix = 0x0;
		vx_alc = vx_num = ix_alc = ix_num = 0;
	}

	V5O&
	fSetA(V5* _vx, szt _vx_cnt, ui2* _ix, szt _ix_cnt){
		mASSERT(mISDEBUG() && (ix_alc || vx_alc), "Mem not allocated");
		memcpy(vx, _vx, _vx_cnt * sizeof(V5));
		vx_num = _vx_cnt;
		memcpy(ix, _ix, _ix_cnt * sizeof(ui2));
		ix_num = _ix_cnt;
		return *this;
	}

	V5O&
	fAllocateA(szt _vx_cnt, szt _ix_cnt){
		vx = new V5[_vx_cnt];
		vx_alc = _vx_cnt*sizeof(V5);
		vx_num = _vx_cnt;
		ix = new ui2[_ix_cnt];
		ix_alc = _ix_cnt*sizeof(ui2);
		ix_num = _ix_cnt;
		return *this;
	}

	V5O&
	fMakeQuadA(const nXD::F4& _v, const nXD::F4& _t, fl4 _z)
	{
		fAllocateA(4,6);
		fVertexA(0).fPositionSetA(_v.left, _v.top, _z)
				.fTexelSetA(_t.left, _t.top);
		fVertexA(1).fPositionSetA(_v.right, _v.top ,_z)
				.fTexelSetA(_t.right, _t.top);
		fVertexA(2).fPositionSetA(_v.right, _v.bot,_z)
				.fTexelSetA(_t.right, _t.bot);
		fVertexA(3).fPositionSetA(_v.left,_v.bot, _z)
				.fTexelSetA(_t.left, _t.bot);

		static ui2 idx[] = {0,2,3,0,1,2};
		fIndecesCpyA(idx, 6);
		return *this;
	}

	V5&
	fVertexA(si4 _i)
	{
		mXD_WTF("Unsafe access");
		mASSERT(mISDEBUG() && (_i >= 0 || _i < vx_num), "Array bounds error");
		return vx[_i];
	}

	V5O&
	fIndecesCpyA(const ui2* _ix, szt _cnt, szt _offs = 0)
	{
		mXD_WTF("Unsafe access");
		mASSERT(mISDEBUG() && (ix_alc >= _cnt*sizeof(ui2)), "Index array lesser that data");
		memcpy(ix + _offs, _ix, _cnt * sizeof(ui2));
		return *this;
	}

	szt
	fDataSize(){
		return sizeof(V5) * vx_num;
	}

	szt
	fIdxSize(){
		return sizeof(ui2) * ix_num;
	}

};

}// nXD
