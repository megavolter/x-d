#pragma once
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Autor: UCKUHT
// Date: 2014-10-12
// Desc: main class of match3 game.
//============================================================================//

#include "EXT/C4/Samples/C4_POS.h"

class M3Greed : public C4
{
xd_open_types:


xd_functional:
	virtual sip     fvTask(si4 _task, si4 _type, void* _data) override;
	virtual void*   fvAccessP(const XD_SPELL& _request) override;
	virtual si4     fvPostInitX(void* _in) override;
	virtual void    fvDestroy() override;

	x_result	fInitX(C4_IMG* _greed, C4_IMG* _chips[9]);

xd_data:

};
