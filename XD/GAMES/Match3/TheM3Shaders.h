#ifndef _THEM3SHADERS_H
#define _THEM3SHADERS_H

struct TheM3Shaders
{
	struct Vertex
	{
		static const char* image();
	};

	struct Pixel
	{
		static const char* image();
	};
};

#endif // THEM3SHADERS_H
