#ifndef _THEMATCH3_H
#define _THEMATCH3_H
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Autor: UCKUHT
// Date: 2014-10-12
// Desc: main class of match3 game.
//============================================================================//
#include "BASE/CODE.h"
#include "BASE/TYPE.h"

#include "MODULES/OS/XD_OS.h"
#include "MODULES/RDL/XD_RDL.h"

#include "GAMES/Common/TheTextureManager.h"
#include "TheShaderManager.h"
#include "ISceneContext.h"
//============================================================================//
struct XD_I4;
//============================================================================//
class TheMatch3 : public IScene
{
xd_open_types:
	struct UserActivity
	{
		struct KeyAction
		{
			XDE_KEY key;
			XDE_INPUT type;

			KeyAction(XDE_INPUT _type, XDE_KEY _key)
				: key(_key), type(_type) {}
		};
	xd_free:
		typedef std::vector<KeyAction> Keys;
		Keys keys;

	xd_functional:
		void fActivityAdd(KeyAction _action) { keys.push_back(_action); }
		void fClear() { keys.clear(); }
	};

	struct GameConfig
	{
		XD_SPELL game_state;
		XD_I2 wdgt_size;
		fl4 fov;
	};

	struct GameState
	{
	xd_data:
		bool		runing;
		const char* exit_reason;

	xd_functional:
		void
		fClear() {
			runing = false;
			exit_reason = 0x0;
		}

		void
		fExit(str _exit_reason) {
			mASSERT(_exit_reason, "You must specify exit reson");
			runing = false;
			exit_reason = _exit_reason;
		}

		void
		fStart() {
			runing = true;
		}

		bln
		fIsRusning() {
			return runing;
		}

		str
		fExitReason() {
			return exit_reason;
		}
	};

	struct Scene
	{
		C4_POS	root;
		C4_IMG	cursor;

		Scene() {}
	};

xd_functional:
	TheMatch3();
	si4		fInitX(const GameConfig& _conf);
	si4		fClearX();
	si4		fRunX();
	si4		fStopX(const char* _reason);
	si4		fKeyHandleX(XDE_INPUT _type, XDE_KEY _key);
	si4		fMousePosX(XD_I2 _pos);
	si4		fDrawTickX();
	void	fSpellHandle(const XD_SPELL& _spell);
	x_result	fSceneLoadX(const XD_SPELL& _scene_id);
	x_result 	fActionsUpdateX();
	x_result	fActionHandleX(const XD_SPELL& _action);

xd_interface:
	static sip	cbInput(TheMatch3* _this, const XD::OS::INPUT_KEY* _key);
	static sip	cbMouse(TheMatch3* _this, const XD_I4* _pos);
	static sip	doStart(TheMatch3* _this, void* _pos);
	static sip	cbResourcesLoaded(TheMatch3* _this, ptr);
	static sip	cbTextureLoaded(TheMatch3* _this, ptr);

xd_special:
	si4		sInitSubSystems();
	si4		sInitGame(const GameConfig& _conf);
	void 	sTasksUpdate();
	void	sPreRender();
	void	sRender();

xd_accessors:
	XD::GFXEX&			fGfxExA();
	TheTextureManager&	fTextureManagerA();
	TheShaderManager&	fShaderManagerA();
	XD_I2&				fInputPointA();
	XD::OS::WDGT_T&     fWdgtA(si4 _id);
	Scene&				fSceneA();
	UserActivity&		fUserActivityA() { return m_user_activity; }

xd_data:
	GameConfig			m_game_config;
	XD::OS::WDGT_T		m_main_wdgt;
	XD::OS				m_os;
	GameState			m_game_state;
	XD::GFXEX			m_gfx;

	TheTextureManager	m_texture_manager;
	TheShaderManager	m_shader_manager;
	XD_I2				m_onscreen_mouse_pos;
	Scene				m_scene;
	ISceneContext*		m_scene_context;

	std::vector<XD_WWW> m_tasks;
	UserActivity		m_user_activity;

xd_free:
	static const XD_SPELL s_res_ready;
	static const XD_SPELL s_game_start;
	static const XD_SPELL s_game_quit;
	static const XD_SPELL s_play;
	static const XD_SPELL s_scene_main;
	static const XD_SPELL s_scene_game;
};

//============================================================================//
#endif // THEMATCH3_H
