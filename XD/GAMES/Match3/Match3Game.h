#pragma once
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Autor: UCKUHT
// Date: 2014-10-12
// Desc: main class of match3 game.
//============================================================================//
#include "ISceneContext.h"

class C4_IMG;
class TheMatch3;
class M3Greed;

class M3Game : public ISceneContext
{
xd_data:
	TheMatch3&	m_game;
	C4_IMG*		m_ui;
	M3Greed*	m_greed;
	C4_IMG*		m_cheeps[9];

xd_functional:
	M3Game(TheMatch3& _game);
	virtual x_result fvInitX() override;
	virtual x_result fvPreRenderX() override;
	virtual x_result fvReleaseX() override;

	x_result fActionHandeX(const XD_SPELL& _action);

xd_special:
	static sip cbTexturesLoaded(M3Game* _this, void*);
};
