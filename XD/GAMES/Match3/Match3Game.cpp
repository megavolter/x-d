#include "Match3Game.h"
#include "TheMatch3.h"

#include "BASE/LANG.h"
#include "BASE/XD_WWW.h"

static const char*	g_textures [] = {
	"g/c/c.png",
};

M3Game::M3Game(TheMatch3& _game)
	: m_game(_game), m_ui(0x0), m_greed(0x0)
{}

x_result M3Game::fvInitX()
{
	XD_WWW tex_load_cb(RCAST(XD_WWW::www, cbTexturesLoaded), this, 0x0);
	m_game.fTextureManagerA().fLoadTexturesX(g_textures, 1, tex_load_cb);
	return A_A;
}

x_result M3Game::fvPreRenderX()
{
	mCOMMENT("Actions");
	if(!fActionsA().empty())
	{
		for(szt i = 0 ; i < fActionsA().size(); ++i)
		{
			fActionHandeX(fActionsA()[i]);
		}
		fActionsA().clear();
	}
	return A_A;
}

x_result M3Game::fvReleaseX()
{
	return A_A;
}

sip
M3Game::cbTexturesLoaded(M3Game* _this, void*)
{
	_this->fActionsA().push_back(TheMatch3::s_res_ready);
	return A_A;
}

x_result
M3Game::fActionHandeX(const XD_SPELL& _action)
{
	if(_action.fEql(TheMatch3::s_res_ready))
	{
        XD::GFX::TEXTURE* texture = m_game.fTextureManagerA()
			.fTextureGet(g_textures[0]);
		if(!texture) return X_X;
		XD_F2 size(texture->size.x, texture->size.y);
		m_cheeps[0] = C4_IMG::iCreateP(		texture, size,
											XD::GFXEX::INTERIOR_WORLD_LOCAL,
											&m_game.fGfxExA(), &m_game.fSceneA().root,
											&m_game.fSceneA().root.fTransformA());

	}
	return A_A;
}
