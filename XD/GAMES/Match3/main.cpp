#include "TheMatch3.h"

#include <math.h>

int main()
{
	mNOTE("Game started!");
	TheMatch3 m3;
	TheMatch3::GameConfig m3c;
	m3c.game_state = "Start";
	if(false) m3c.wdgt_size.fSet(1024, 768);
	if(true) m3c.wdgt_size.fSet(1280, 720);
	m3c.fov = M_PI_2;
	si4 result = 0;
	if(A_A == m3.fInitX(m3c))
	{
		result = m3.fRunX();
	}
	mNOTE("Game finished!");
	mXD_DEBUG_STOP;
	return result;
}
