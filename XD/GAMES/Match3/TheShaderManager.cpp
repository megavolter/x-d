#include "TheShaderManager.h"
//============================================================================//
si4
TheShaderManager::fInitX(XD::GFX *_gfx)
{
	m_gfx = _gfx;
	return A_A;
}
//============================================================================//
si4
TheShaderManager::fShaderAddX(const XD_SPELL &_spell, str _vertex, str _pixel)
{
	mXD_CHECK_ERR(
			0x0 == m_gfx,
			"TheShaderManager not initialised",
			return X_X);

	XD::GFX::PROGRAM* program = m_gfx->fProgramCreateP(_vertex, _pixel);
	mXD_CHECK_ERR(
			0x0 == program,
			"Programm creation failed.",
			return X_X);

	m_programs[_spell] = program;
	return A_A;
}
//============================================================================//
XD::GFX::PROGRAM*
TheShaderManager::fShaderGetP(const XD_SPELL &_spell)
{
	programs_t::iterator iter = m_programs.find(_spell);
	if(iter != m_programs.end())
		return iter->second;

	return 0x0;
}
//============================================================================//
