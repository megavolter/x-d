#ifndef _THEMAINSCREEN_H
#define _THEMAINSCREEN_H

#include "BASE/CODE.h"
#include "BASE/XD_I2.h"

#include "EXT/C4/C4.h"
#include "EXT/C4/Samples/C4_IMG.h"
#include "ISceneContext.h"
#include "TheShaderManager.h"

#include "EXT/C4/Samples/C4_IMG.h"

#include <vector>

class TheMatch3;
//============================================================================//
class TheMainScreen : public ISceneContext
{
xd_open_types:

xd_functional:
    TheMainScreen(TheMatch3& _game);
	si4		fInitX();

	virtual x_result fvInitX() override;
	virtual x_result fvPreRenderX() override;
	virtual x_result fvReleaseX() override;

	x_result	fSceneCreateX();
	void		fUpdate();
	void		fInput(XDE_INPUT _i, XDE_KEY _k);

xd_interface:
	static x_result	cbTexturesLoad(TheMainScreen* _this, ptr);

xd_special:
	x_result sSpellsFlushX();
	x_result sSpellX(const XD_SPELL& _action);

xd_data:
	TheMatch3&	m_game;
	bln			m_res_loaded;

	C4_IMG*		m_btn_start;
	C4_IMG*		m_btn_quit;
	C4_IMG*		m_current;
};

#endif // THEMAINSCREEN_H
