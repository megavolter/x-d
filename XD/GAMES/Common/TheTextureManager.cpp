#include "TheTextureManager.h"
#include "MODULES/GFX_DECODER/XD_GFX_DECODER.h"
#include "BASE/XD_WWW.h"
#include "BASE/CAST.h"
//============================================================================//
si4
TheTextureManager::fInitX(XD::GFX* _gfx)
{
	mASSERT(_gfx, "Invalid parameter");
	m_proccess = false;
	m_gfx = _gfx;

	return A_A;
}
//============================================================================//
si4
TheTextureManager::fLoadTexturesX(const char* _names[],
								  szt _count, XD_WWW& _cback)
{
mBLOCK("Create item");
	Item item(_names, _count, _cback);
	m_item_list.push_back(item);

	return A_A;
}
//============================================================================//
si4
TheTextureManager::fUpdateX()
{
	mXD_RESULTA(m_raw_data_loader.fSyncX());
	if(false == m_raw_data_loader.fIsReady())
		return A_A;

mCOMMENT("Loading next item");
	if(m_item_list.empty())
		return O_O;

	Item& item = m_item_list[m_item_list.size()-1];
	for(szt i = 0; i < item.paths.size(); ++i)
	{
		const char* path = (item.paths[i]).fGet();
		XD_WWW texture_decoder(RCAST(XD_WWW::www, cbOnLoadTextureData), this);
		m_raw_data_loader.fDataLoad(path, texture_decoder);
	}
	XD_WWW list_load_listener(RCAST(XD_WWW::www, cbOnLoadList), this, &item);
	m_raw_data_loader.fSyncListener() = list_load_listener;

	return A_A;
}
//============================================================================//
void
TheTextureManager::fTextureAdd(const char* _path, XD::GFX::TEXTURE* _texture)
{
	mASSERT(_path && _texture, "Invalid parameters");
	ManagedTexture texture(_path, _texture);
	m_managed_textures.push_back(texture);
}
//============================================================================//
TheTextureManager::item_list_t&
TheTextureManager::fItemList()
{
	return m_item_list;
}

//============================================================================//
sip
TheTextureManager::cbOnLoadList(TheTextureManager* _this, Item* _item)
{
	mASSERT(_this && _item, "Invalid parameters");
	_item->cback.fRunX();
	// Erase first element.
	_this->fItemList().erase(_this->fItemList().begin());
	return A_A;
}
//============================================================================//
sip
TheTextureManager::cbOnLoadTextureData(TheTextureManager *_this,
									   XD::RDL::DATA_ON_LOAD *_data)
{
	XD::GFX_DECODER decoder;
	XD::GFX_DECODER::BITMAP bitmap(_data->data->data, _data->data->data_size);

	mXD_RESULTA(decoder.fBitmapInitX(bitmap));
	mXD_RESULTA(decoder.fBitmapDecodeX(bitmap));

	XD::GFX::TEXTURE* texture = _this->fGfxP()->fTextureCreateP(
				bitmap.bmp, bitmap.bmp_size, bitmap.size.x, bitmap.size.y,
				bitmap.format);
	if(texture)
		_this->fTextureAdd(_data->path.fGet(), texture);

	decoder.fBitmapClear(bitmap);
	mDEV("Loade texture"<<_data->path.fGet());
	return A_A;
}
//============================================================================//
XD::GFX*
TheTextureManager::fGfxP()
{
	return m_gfx;
}
//============================================================================//
XD::GFX::TEXTURE*
TheTextureManager::fTextureGet(const XD_STRING& _path)
{
	for(szt i = 0; i < m_managed_textures.size(); ++i)
	{
		ManagedTexture& curent = m_managed_textures[i];
		if(curent.path == _path)
			return curent.texture;
	}
	return 0x0;
}
//============================================================================//

