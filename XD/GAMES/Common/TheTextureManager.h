#ifndef _THETEXTUREMANAGER_H
#define _THETEXTUREMANAGER_H

#include <vector>

#include "MODULES/RDL/XD_RDL.h"
#include "MODULES/GFX_V2/XD_GFX.h"

#include "BASE/CODE.h"
#include "BASE/XD_WWW.h"
#include "BASE/CODE.h"
#include "BASE/STRINGS/XD_STRING.h"

class TheTextureManager
{
xd_open_types:
	struct Item
	{
		typedef std::vector<XD_STRING> str_list_t;
		str_list_t  paths;
		XD_WWW		cback;

		Item(const char* _array[], szt _count, XD_WWW& _cback)
		{
			for(szt i = 0; i < _count; ++i)
			{
				const char* next_path = _array[i];
				paths.push_back(next_path);
				cback = _cback;
			}
		}
	};

	struct ManagedTexture
	{
		XD_STRING			path;
		XD::GFX::TEXTURE*	texture;

		ManagedTexture(const char* _path, XD::GFX::TEXTURE* _texture)
			: path(_path), texture(_texture)
		{}
	};

	typedef std::vector<Item>				item_list_t;
	typedef std::vector<ManagedTexture>		texture_list_t;

xd_functional:
	si4		fInitX(XD::GFX* _gfx);
	si4		fLoadTexturesX(const char* _names[], szt _count, XD_WWW& _cback);
	si4		fUpdateX();
	void	fTextureAdd(const char* _path, XD::GFX::TEXTURE* _texture);

	XD::GFX::TEXTURE* fTextureGet(const XD_STRING& _path);

xd_accessors:
	XD::GFX*		fGfxP();
	item_list_t&	fItemList();

xd_interface:
	static	sip		cbOnLoadList(TheTextureManager* _this, Item* _item);
	static  sip
		cbOnLoadTextureData(
				TheTextureManager* _this, XD::RDL::DATA_ON_LOAD* _data);

xd_data:
	XD::RDL			m_raw_data_loader;
	XD::GFX*		m_gfx;
	item_list_t		m_item_list;
	bool			m_proccess;
	texture_list_t	m_managed_textures;
};

#endif // THETEXTUREMANAGER_H
