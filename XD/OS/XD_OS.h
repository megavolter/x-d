#ifndef _XD_OS_H_INCLUDED
#define _XD_OS_H_INCLUDED
// LICENSE.txt

#include "BASE/CODE.h"
#include "BASE/LANG.h"
#include "BASE/TYPE.h"
#include "BASE/XD_F4.h"
#include "BASE/XD_WWW.h"
#include "BASE/STRINGS/XD_STRING.h"
#include "BASE/XD_I2.h"
#include "BASE/XD_I4.h"
#include "BASE/XD_F2.h"

namespace nXD{

struct WDGT;

class OS
{
public xd_types:

	struct INPUT_KEY
	{
		XDE_INPUT	type;
		XDE_KEY		key;
	};
	
	enum HandleResult {
		HandleResult_Error = -1,
		HandleResult_Ok = 0,
		HandleResult_Interupt = 1,
		HandleResult_,
	};
			
	class KeyListener {
	public:
		virtual HandleResult onKey(const INPUT_KEY& _key) {
			return HandleResult_Error;
		}
	};
	
	class PointerListener{
	public:
		virtual HandleResult onPointer(si4 _id, const I4& _pos) {
			return HandleResult_Error;
		};
	};

	typedef	WDGT* WDGT_T;

public xd_functional:
	OS();

	si4 fSyncX();
	si4 fRunX()
	{
		mFUNC_IMPL_ERR();
		mSNIPLET()
		{
			while(true)
				fSyncX();
		}
		return X_X;
	}
	x_result fDestroyX() { /*TODO:fDestroyX*/ return A_A;}
	
	OS& fKeyListenerSetA(KeyListener* _value){
		m_key_lsnr = _value;
		return *this;
	}
	
	OS& fPointerListenerSetA(PointerListener* _value){
		m_pointer_lsnr = _value;
		return *this;
	}

public xd_interface:
	static x_result iScreenSizeGetX(nXD::I2& out result);
	static x_result iCurentDirectorySetX(str _path);
	static x_result iWorkingDirectoryGetX(XD_STRING& _result);
	static x_result iGetSystemErrorX(XD_STRING& _err_str);

public xd_func_block("Widget"):
	/// Create widget context.
	static WDGT_T iWdgtCreateP();
	static x_result iWdgtDestroyX(WDGT_T _wdgt);

	/// Initialize widget context.
	static x_result iWdgtInitX(const char* _utf8_name,
							   nXD::I4 _border,
							   WDGT_T _wdgt );
	/// Get cursor position relative widget.
	static void iWdgtCursorGet(WDGT_T _wdgt, nXD::F4& _cur);
	// Show/hide widget.
	static si4 iWdgtShowX(bool _do, WDGT_T _wdgt);
	/// Getting widget descriptor.
	static sip iWdgtDescriptorGet(WDGT_T wdgt);
	static nXD::I2& iWdgtSizeA(WDGT_T _wdgt);

private xd_data:
	KeyListener* m_key_lsnr;
	PointerListener* m_pointer_lsnr;
};

}//ns

#endif // _EC_OS_H_INCLUDED
