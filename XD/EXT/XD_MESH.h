#ifndef _XD_MESH_H_INCLUDED
#define _XD_MESH_H_INCLUDED
// LISENCE.txt
#include "BASE/XD_TRTR.h"
#include "XD_GFXEX.h"
namespace nXD{

class MESH5
{
    XD_TRTR             m_transform;
    GFX::DATA5          m_raster;
    GFXEX::SETTINGS     m_settings;
    GFX::TEXTURE*       m_texture;
    XD_F4               m_frame;

public:
    MESH5();
    ~MESH5();

public:// Management.
    si4                 fDrawX(GFXEX& context);
    si4                 fDrawBatchX(GFXEX& context);
    bool                fEmpty();

    XD_TRTR&            fTransform();
    GFX::DATA5&         fRaster();
    GFXEX::SETTINGS&    fSettings();
    GFX::TEXTURE*       fTextureGetP();
    XD_F4               fFrame();

    void                fTextureSet(GFX::TEXTURE* texture, bool use);

};

}//ns
//============================================================================//

#endif // _XD_MESH_H_INCLUDED
