#ifndef _XD_FRAME_H_INCLUDED
#define _XD_FRAME_H_INCLUDED
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Author: UCKUHT
// Date: 2014-01-05
// Desc: mesh extention to frame logical object.
//============================================================================//
#include "XD_MESH.h"
//============================================================================//
namespace nXD{

class FRAME5 : public MESH5
{
public:
    si4 fInitX(fl4 addx, fl4 addy, fl4 scalex, fl4 scaley);
};

}//ns
//============================================================================//

#endif // _XD_FRAME_H_INCLUDED
