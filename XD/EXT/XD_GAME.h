#ifndef _XD_GAME_H_INCLUDED
#define _XD_GAME_H_INCLUDED
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Author: UCKUHT
// Date: 2014-01-04
// Desc: sybsys holder interface.
//============================================================================//
#include "XD_GFXEX.h"
#include "MODULES/RDL/XD_RDL.h"
#include "MODULES/OS/XD_OS.h"
#include "MODULES/GFX_DECODER/XD_GFX_DECODER.h"
#include "BASE/XD_F4.h"

namespace nXD{

struct GAME_C
{
    struct INITIAL
    {
        const char* wdgt_name;
        XD_F4       wdgt_frame;
        XD_WWW      draw_lsnr;
        XD_WWW      key_lsnr;
        XD_WWW      mouse_lsnr;
    };

    GFXEX           gfx;
    RDL             rdl;
    OS              os;
    WDGT*           widget;
    GFX_DECODER     decoder;
    XD_WWW          draw_lsnr;
    bool            in_play;

public:
    GAME_C();
    ~GAME_C();

public:
    si4     fInitX(const INITIAL& ini_str);
    si4     fStartX();
    void    fStop();

public:
    GFXEX&          fGfx();
    RDL&            fRdl();
    OS&             fOS();
    WDGT*           fWdgtP();
    GFX_DECODER&    fGfxDecoder();

};

}//ns

//============================================================================//
#endif // _XD_GAME_H_INCLUDED
