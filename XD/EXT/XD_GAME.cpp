#include "XD_GAME.h"
/*
namespace nXD{
//============================================================================//
GAME_C::GAME_C()
    :   gfx(), rdl(), os(), widget(0x0), decoder(),
        draw_lsnr(), in_play(false)
{}

GAME_C::~GAME_C()
{}

//============================================================================//
si4
GAME_C::fInitX(const INITIAL& ini_str)
{
mBLOCK("Widget");
    mXD_RESULTA(os.fInitX());
    os.fInputKeyListener() = ini_str.key_lsnr;
    os.fInputMouseListener() = ini_str.mouse_lsnr;
    draw_lsnr = ini_str.draw_lsnr;

    widget = OS::fWdgtCreateP();
    if(!widget)
        return X_X;

    mXD_RESULTA(
        OS::fWdgtInitX(
            ini_str.wdgt_name,
            ini_str.wdgt_frame.x,
            ini_str.wdgt_frame.y,
            ini_str.wdgt_frame.z,
            ini_str.wdgt_frame.w,
            widget
        )
    );

mBLOCK("Render");
    si4 scriptor = OS::fWdgtDescriptorGet(widget);
    mXD_RESULTA(gfx.fInitX(scriptor));

    return A_A;
}
//============================================================================//
si4
GAME_C::fStartX()
{
    in_play = true;
    mXD_RESULTA(OS::fWdgtShowX(true, widget));
    while(in_play)
    {
        mXD_RESULTA(rdl.fSyncX());

        mXD_RESULTA(gfx.fSceneBeginX());
        mXD_RESULTA(gfx.fSceneClearX());
        mXD_RESULTA(draw_lsnr.fRunX());
        mXD_RESULTA(gfx.fSceneEndX());

        mXD_RESULTA(os.fSyncX());
    }
    return A_A;
}
//============================================================================//
void
GAME_C::fStop()
{
    in_play = false;
}
//============================================================================//
GFXEX&
GAME_C::fGfx()
{
    return gfx;
}
//============================================================================//
RDL&
GAME_C::fRdl()
{
    return rdl;
}
//============================================================================//
OS&
GAME_C::fOS()
{
    return os;
}
//============================================================================//
WDGT*
GAME_C::fWdgtP()
{
    return widget;
}
//============================================================================//
GFX_DECODER&
GAME_C::fGfxDecoder()
{
    return decoder;
}
//============================================================================//

}//ns
*/
