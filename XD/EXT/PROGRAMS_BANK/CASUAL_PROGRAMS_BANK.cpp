#include "CASUAL_PROGRAMS_BANK.h"
//============================================================================//
static const char vertex_iminimum_data[] =
		"attribute vec3 a_pos;                	\n"
		"attribute vec2 a_tex;                	\n"
		"\n"
		"varying vec2 v_tex;                  	\n"
		"                                     	\n"
		"void main()                          	\n"
		"{                                    	\n"
		"   v_tex = a_tex;                    	\n"
		"   gl_Position = vec4(a_pos, 1.0); 	\n"
		"}                                    	\n" ;
const char*
EXT_PROGRAMS_BANK::VERTEX::iMinimum()
{
	return vertex_iminimum_data;
}
//============================================================================//
static const char vertex_ivwp5_data[] =
		"attribute vec3 a_pos; \n"
		"attribute vec2 a_tex; \n"
		"varying vec2 v_tex; \n"
		"uniform mat4 u_vwp; \n"
		"\n"
		"void main() \n"
		"{ \n"
		"   v_tex = a_tex; \n"
		"   gl_Position = u_vwp * vec4(a_pos, 1.0) ; \n"
		"} \n"
		;
const char*
EXT_PROGRAMS_BANK::VERTEX::iVWP5()
{
	return vertex_ivwp5_data;
}
//============================================================================//
static const char fragment_iminimum_data[] =
		"varying vec2 v_tex;                                \n"
		"void main()                                        \n"
		"{                                                  \n"
		"   gl_FragColor = vec4(1 , 0, 1, 1);                   \n"
		"}                                                  \n"
		;
const char *
EXT_PROGRAMS_BANK::FRAGMENT::iMinimum()
{
	return fragment_iminimum_data;
}
//============================================================================//
static const char fragment_ivwp5_data[] =
		"varying vec2 v_tex;                                \n"
		"uniform sampler2D s_0;                             \n"
		"void main()                                        \n"
		"{                                                  \n"
		"   gl_FragColor = texture2D(s_0, v_tex);           \n"
		"}                                                  \n"
		;
const char*
EXT_PROGRAMS_BANK::FRAGMENT::iVWP5()
{
	return fragment_ivwp5_data;
}
//============================================================================//
