#ifndef _CASUAL_PROGRAMS_BANK_H_INCLUDED
#define _CASUAL_PROGRAMS_BANK_H_INCLUDED
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Author: UCKUHT
// Date: 2014-01-06
// Desc: shader models data bank.
//============================================================================//
#include "BASE/TYPE.h"
#include "BASE/LANG.h"
#include "BASE/CODE.h"

struct EXT_PROGRAMS_BANK
{
xd_open_types:
	struct VERTEX
	{
		static const char* iMinimum();
		static const char* iVWP5();
	};

	struct FRAGMENT
	{
		static const char* iMinimum();
		static const char* iVWP5();
	};
};
//============================================================================//

#endif // CASUAL_PROGRAMS_BANK_H_INCLUDED
