#ifndef _XD_GFXEX_H_INCLUDED
#define _XD_GFXEX_H_INCLUDED
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Autor: UCKUHT
// Date: 2013-05-19
// Desc: simplest crossplatform graphics extention.
//============================================================================//
#include "MODULES/GFX/XD_GFX.h"

namespace nXD{

class GFXEX : public GFX
{
public:
	enum INTERIOR{
			INTERIOR_UNDEFINED,// As is.
			INTERIOR_WORLD,
			INTERIOR_LOCAL,
			INTERIOR_WORLD_LOCAL,
			INTERIOR_VPORT,
			INTERIOR_
	};

public:
	enum PROGRAM{
			PROGRAM_MINIMAL,
			PROGRAM_TEX_MIN,
			PROGRAM_WVPT,// world local textured
			PROGRAM_
	};

public:
	enum QUALITY{
			QUALITY_LOW,
			QUALITY_MID,
			QUALITY_HIGH,
			QUALITY_
	};

public:
	struct SETTINGS
	{
		XDE_BLEND   blend_mode;
		bool        unic_program;
		bool        use_texture;
		INTERIOR    interior;
		QUALITY     quality;

		SETTINGS()
			: blend_mode(XDE_BLEND_NONE), unic_program(false),
			  use_texture(false), interior(INTERIOR_UNDEFINED),
			  quality(QUALITY_LOW)
			{}

		SETTINGS& fSetBlendModeA(XDE_BLEND _value) {
			blend_mode = _value;
			return *this;
		}

		SETTINGS& fSetIsProgrammUnicA(bln _value) {
			unic_program = _value;
			return *this;
		}

		SETTINGS& fSetIsTextureUsesA(bln _value) {
			use_texture = _value;
			return *this;
		}

		SETTINGS& fSetInteriorA(INTERIOR _value) {
			interior = _value;
			return *this;
		}

		SETTINGS& fSetQualityA(QUALITY _value) {
			quality = _value;
			return *this;
		}
	};

public:
	GFXEX();
	~GFXEX();

public:
	si4 fInitX(si4 draw_descriptor);
	si4 fRasterizeX();
	si4 fSetUpProgramX(PROGRAM program);
	si4 fRestoreDefaultsX();

	XD_F44& fTransformGlobalA();
	XD_F44& fTransformLocalA();
	XD_F4& fVportA();
	XD_F4& fTportA();

public:// Texture.
	si4 fTextureSetX(const GFX::TEXTURE* texture);

public:// Settings.
	si4 fSettingsSetX(const GFXEX::SETTINGS* settings);

public:// Raster.
	si4 fData5SetX(const GFX::DATA5* raster);

public:// Program.
	si4 fProgramSetX(const GFX::PROGRAM* program);

private:
	GFX::PROGRAM* CreateSubProgramP(PROGRAM prog);

private:
	const GFX::DATA5*       m_cur_raster5;
	const GFX::PROGRAM*     m_cur_program;
	const GFX::TEXTURE*     m_cur_texture;
	const GFXEX::SETTINGS*  m_cur_settings;
	GFX::PROGRAM*           m_programs[PROGRAM_];

	// World global transformation.
	XD_F44 m_global;
	bool   m_global_mod;
	//  Object local transformation.
	XD_F44 m_local;
	bool   m_local_mod;
	//  Onscreen viewport.
	XD_F4  m_vport;
	bool   m_vport_mod;
	//  Textels port.
	XD_F4  m_tport;
	bool   m_tport_mod;
};

}//ns

#endif // _XD_GFXEX_H_INCLUDED
