#include "XD_FRAME.h"
namespace nXD{
//============================================================================//
static const szt frame_vertex_cnt = 4;
static const szt frame_index_cnt = 6;
si4
FRAME5::fInitX(fl4 addx, fl4 addy, fl4 scalex, fl4 scaley)
{
    GFX::V5 quad[frame_vertex_cnt] =
    {
        {-scalex + addx,  scaley + addy, 0, 0,0},
        { scalex + addx,  scaley + addy, 0, 1,0},
        { scalex + addx, -scaley + addy, 0, 1,1},
        {-scalex + addx, -scaley + addy, 0, 0,1},
    };

    static ui2 array[frame_index_cnt] = {0,1,2,0,2,3,};


    mXD_RESULTA(fRaster().fInitX(quad, frame_vertex_cnt, array, frame_index_cnt));
    fSettings().interior = GFXEX::INTERIOR_LOCAL;
    return A_A;
}
//============================================================================//
}//ns
