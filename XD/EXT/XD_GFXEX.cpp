#include "XD_GFXEX.h"
#include "CASUAL_PROGRAMS_BANK.h"
#include "BASE/XD_MATH3D.h"
EXT_PROGRAMS_BANK   g_bank;

namespace nXD{
//============================================================================//
// Data block.
	static GFXEX::SETTINGS defset;
//============================================================================//
GFXEX::GFXEX() : GFX(),
	m_cur_raster5(0x0), m_cur_program(0x0), m_cur_texture(0x0),
	m_cur_settings(0x0),
	m_global(1),        m_global_mod(false),
	m_local(1),         m_local_mod(false),
	m_vport(0,0,1,1),   m_vport_mod(false),
	m_tport(0,0,1,1),   m_tport_mod(false)
{
	memset(m_programs, 0, PROGRAM_ * sizeof(PROGRAM*));
}
//============================================================================//
GFXEX::~GFXEX()
{}
//============================================================================//
si4
GFXEX::fInitX(si4 draw_descriptor)
{
	mXD_RESULTA(GFX::fInitX(draw_descriptor));

	 defset.interior = INTERIOR_;
	 m_cur_settings = &defset;

	return fRestoreDefaultsX();
}
//============================================================================//
si4
GFXEX::fRestoreDefaultsX()
{
	fDepthX(false);
	fBlendX(XDE_BLEND_NONE);
	mXD_RESULTA(fSetUpProgramX(PROGRAM_MINIMAL));


	return A_A;
}
//============================================================================//
si4
GFXEX::fRasterizeX()
{
	if(m_cur_raster5)
	{
		return GFX::fRasterizeX(m_cur_raster5);
	}
	else
	{
		mERROR("no raster avalible");
	}
	return X_X;
}
//============================================================================//
si4
GFXEX::fTextureSetX(const GFX::TEXTURE* texture)
{
	if(m_cur_texture != texture)
	{
		mXD_RESULTA(GFX::fTextureSetX(texture, 0x0, 0));
		m_cur_texture = texture;
	}
	return A_A;
}
//============================================================================//
// Setup driver. All preferencies mus be adjust before.
si4
GFXEX::fSettingsSetX(const GFXEX::SETTINGS* settings)
{
	mASSERT(m_cur_settings, "Wrong settings");
	if(!settings)
	{
		return fRestoreDefaultsX();
	}
	if(m_cur_settings != settings)
	{//1 Applying.
		mCOMMENT("Shader");
		if(!settings->unic_program)
		{//2 Autoselect program.
			if(m_cur_settings->interior != settings->interior)
			{//3
				if(INTERIOR_WORLD_LOCAL == settings->interior ||
					INTERIOR_LOCAL == settings->interior ||
					INTERIOR_WORLD == settings->interior)// Bitset needed?
				{//4 Wvp program.
					mXD_RESULTA(fSetUpProgramX(PROGRAM_WVPT));
				}//4
				else
				{//4
					if(true == settings->use_texture)
					{
						mXD_RESULTA(fSetUpProgramX(PROGRAM_TEX_MIN));
					}
					else
					{
						mXD_RESULTA(fSetUpProgramX(PROGRAM_MINIMAL));
					}
				}//4
			}//3
		}//2

		mCOMMENT("Driver");
		if(m_cur_settings->blend_mode != settings->blend_mode)
		{//2
			mXD_RESULTA(GFX::fBlendX(settings->blend_mode));
		}//2
		m_cur_settings = settings;
	}//1

	if(INTERIOR_WORLD_LOCAL == m_cur_settings->interior)
	{
		XD_F44 result = m_local;
		mat4_multiply(&result, &m_global);
		mXD_RESULTA(fProgramVf44X(result, "u_vwp", 0));
	}
	else if(INTERIOR_LOCAL == m_cur_settings->interior)
	{
		mXD_RESULTA(fProgramVf44X(m_local, "u_vwp", 0));
	}
	else if(INTERIOR_WORLD == m_cur_settings->interior)
	{
		mXD_RESULTA(fProgramVf44X(m_global, "u_vwp", 0));
	}
	else if(INTERIOR_VPORT || INTERIOR_UNDEFINED)
	{
		mXD_FAIL("TODO: INTERIOR_VPORT");
	}

	return A_A;
}
//============================================================================//
si4
GFXEX::fData5SetX(const GFX::DATA5* raster)
{
	if(m_cur_raster5 != raster)
	{
		mXD_RESULTA(GFX::fData5SetX(raster));
		m_cur_raster5 = raster;
	}
	return A_A;
}
//============================================================================//
si4
GFXEX::fProgramSetX(const GFX::PROGRAM* program)
{
	if(m_cur_program != program)
	{
		mXD_RESULTA(GFX::fProgramSetX(program));
		m_cur_program = program;
	}
	return A_A;
}
//============================================================================//
GFX::PROGRAM*
GFXEX::CreateSubProgramP(PROGRAM prog)
{
	GFX::PROGRAM* result = 0x0;
	str vertex = 0x0;
	str fragment = 0x0;
	switch(prog)
	{
	case GFXEX::PROGRAM_TEX_MIN:
		vertex = g_bank.f_vmin5();
		fragment = g_bank.f_tex5();
		break;
	case GFXEX::PROGRAM_MINIMAL:
		vertex = g_bank.f_vmin5();
		fragment = g_bank.f_min5();
		break;
	case GFXEX::PROGRAM_WVPT:
		vertex = g_bank.v_vwp5();
		fragment = g_bank.f_tex5();
		break;
	default:
		mERRORF("No program avaliable!");
		break;
	}
	result = fProgramCreateP(vertex, fragment);
	if(0x0 == result)
	{
		mERRORF("Creation shader program failed!\nVertex:\n" << vertex
				<< "Fragment:\n"<<fragment);
	}
	return result;
}
//============================================================================//
si4
GFXEX::fSetUpProgramX(PROGRAM program)
{
	if(0x0 == m_programs[program])
	{
		GFX::PROGRAM* prog = CreateSubProgramP(program);
		if(!prog)
			mXD_FAIL("Subprogram creation");
		m_programs[program] = prog;
		return fSetUpProgramX(program);
	}
	return fProgramSetX(m_programs[program]);
}
//============================================================================//
XD_F44&
GFXEX::fTransformGlobalA()
{
	m_global_mod = true;
	return m_global;
}
//============================================================================//
XD_F44&
GFXEX::fTransformLocalA()
{
	m_local_mod = true;
	return m_local;
}
//============================================================================//
XD_F4&
GFXEX::fVportA()
{
	m_vport_mod = true;
	return m_vport;
}
//============================================================================//
XD_F4&
GFXEX::fTportA()
{
	m_tport_mod = true;
	return m_tport;
}
//============================================================================//
}//ns









