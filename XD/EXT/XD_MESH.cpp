#include "XD_MESH.h"
namespace nXD{
//============================================================================//
MESH5::MESH5() : m_transform(1), m_raster(), m_settings(), m_texture(), m_frame()
{}
//============================================================================//
MESH5::~MESH5()
{
    if(!m_raster.fIsEmpty())
    {
        GFX::fDataDestroy(&m_raster);
    }
}
//============================================================================//
bool
MESH5::fEmpty()
{
    return m_raster.fIsEmpty();
}
//============================================================================//
si4
MESH5::fDrawX(GFXEX& context)
{
    if(fEmpty())
    {// Empty draw is normal.
        return A_A;
    }
    if(GFXEX::INTERIOR_LOCAL == m_settings.interior ||
       GFXEX::INTERIOR_WORLD_LOCAL == m_settings.interior)
    {// Locals modify.
        XD_F44& local = context.fTransformLocalA();
        m_transform.fSetToF44(local);
    }
    mXD_RESULTA(context.fSettingsSetX(&m_settings));// Shader first.
    mXD_RESULTA(context.fTextureSetX(m_texture));
    mXD_RESULTA(context.fData5SetX(&m_raster));

    return context.fRasterizeX();
}
//============================================================================//
si4
MESH5::fDrawBatchX(GFXEX& context)
{
    mFUNCIMPL();
    return X_X;
}
//============================================================================//
XD_TRTR&
MESH5::fTransform()
{
    return m_transform;
}
//============================================================================//
GFX::DATA5&
MESH5::fRaster()
{
    return m_raster;
}
//============================================================================//
GFXEX::SETTINGS&
MESH5::fSettings()
{
    return m_settings;
}
//============================================================================//
GFX::TEXTURE*
MESH5::fTextureGetP()
{
    return m_texture;
}
//============================================================================//
void
MESH5::fTextureSet(GFX::TEXTURE* texture, bool use)
{
    m_settings.use_texture = use;
    m_settings.blend_mode = XDE_BLEND_NORMAL;
    m_texture = texture;
}
//============================================================================//
}
