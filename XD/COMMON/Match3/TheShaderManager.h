#ifndef _THESHADERMANAGER_H
#define _THESHADERMANAGER_H

#include <map>
#include "GFX/XD_GFX.h"

#include "BASE/TYPE.h"
#include "BASE/XD_SPELL.h"
#include "BASE/CODE.h"

class TheShaderManager
{
xd_types:
	typedef std::map<XD_SPELL, XD::GFX::PROGRAM*>	programs_t;

xd_functional:
	TheShaderManager() : m_gfx(0x0) {}

	si4 fInitX(XD::GFX* _gfx);
	si4	fShaderAddX(const XD_SPELL& _spell, str _vertex, str _pixel);
	XD::GFX::PROGRAM*	fShaderGetP(const XD_SPELL& _spell);

xd_data:
	programs_t	m_programs;
	XD::GFX*	m_gfx;
};

#endif // THESHADERMANAGER_H
