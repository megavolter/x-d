#include "TheMainScreen.h"
#include "TheMatch3.h"
#include "BASE/XD_COLOR.h"
#include "BASE/CAST.h"
#include "BASE/XD_WWW.h"
#include "BASE/LOG.h"
#include "TheM3Shaders.h"
//============================================================================//
enum TEXTURES
{
	T_CLOUD,
	T_CUBE,
	T_SPHERE_5,
	T_SPHERE_6,
	T_SPHERE_7,
	T_SPHERE_8,
	T_BGR,
	T_OVAL,
};
const char* g_textures[] = {
	"g/main_menu/cloud.png",
	"g/main_menu/cube4.png",
	"g/main_menu/sphere5.png",
	"g/main_menu/sphere6.png",
	"g/main_menu/sphere7.png",
	"g/main_menu/sphere8.png",
	"g/main_menu/Bgr.png",
	"g/main_menu/Oval.png",
};

//============================================================================//
TheMainScreen::TheMainScreen(TheMatch3& _game) :
    m_game(_game), m_res_loaded(false), m_btn_start(0x0),
    m_btn_quit(0x0), m_current(0x0)
{}
//============================================================================//
si4
TheMainScreen::fInitX()
{
	mBLOCK("Loading textures");
	szt tex_count = sizeof(g_textures)/ptr_sz;
	XD_WWW cb_on_load(RCAST(XD_WWW::www, cbTexturesLoad), this);
	m_game.fTextureManagerA().fLoadTexturesX(g_textures, tex_count, cb_on_load);

	mBLOCK("Prepare shaders");
	mXD_RESULT_E(m_game.fShaderManagerA().fShaderAddX("image",
			TheM3Shaders::Vertex::image(), TheM3Shaders::Pixel::image()));
	return A_A;
}
//============================================================================//
x_result
TheMainScreen::cbTexturesLoad(TheMainScreen* _this, ptr)
{
	_this->fActionsA().push_back(TheMatch3::s_res_ready);
    return A_A;
}
//============================================================================//
x_result
TheMainScreen::fvInitX()
{
	mXD_RESULTA( fInitX() );
    return A_A;
}
//============================================================================//
bool gCheckButton(const XD_I2& _point, C4_IMG* _btn)
{
	XD_F4 btn_pos = _btn->fTransformA().fPositionXYZW();
	XD_I2 ui_pos(btn_pos.x, btn_pos.y);
	if(_point.fDistanceQuad(ui_pos) < 900)
		return true;
	return false;
}
//============================================================================//
void
TheMainScreen::fUpdate()
{
	mCOMMENT("UI");
	XD_I2 input_pos = m_game.fInputPointA();

	if(m_current)
	{
		if(!gCheckButton(input_pos, m_current)){
			m_current->fTransformA().fRotationSetA(0,0,0);
			m_current = 0x0;
		}
	}
	if(!m_current)
	{
		if(gCheckButton(input_pos, m_btn_start))
			m_current = m_btn_start;
		else
		if(gCheckButton(input_pos, m_btn_quit))
			m_current = m_btn_quit;
	}

	if(m_current)
		m_current->fTransformA().fRotateA(0, 0, 0.1);

	mCOMMENT("Input");
	if(m_game.fUserActivityA().keys.size())
	{
		for(szt i = 0; i < m_game.fUserActivityA().keys.size(); ++i)
		{
			TheMatch3::UserActivity::KeyAction action =
												m_game.fUserActivityA().keys[i];
			fInput(action.type, action.key);
		}
		m_game.fUserActivityA().keys.clear();
	}
}
//============================================================================//
void
TheMainScreen::fInput(XDE_INPUT _i, XDE_KEY _k)
{
	switch(_k)
	{
	case XDE_KEY_MOUSE_L:
		if(_i == XDE_INPUT_DOWN && m_current)
		{
			m_game.fActionsA().push_back(
					(m_current == m_btn_quit)? TheMatch3::s_game_quit
											 : TheMatch3::s_game_start );
		}
		break;
	default:
		break;
	}
}
//============================================================================//
x_result
TheMainScreen::fvPreRenderX()
{
	mXD_RESULTA( sSpellsFlushX() );

	if(m_res_loaded){
		fUpdate();
	}
	return A_A;
}
//============================================================================//
x_result
TheMainScreen::fvReleaseX()
{
	m_btn_quit->fRemoveFromParent();
	m_btn_start->fRemoveFromParent();
	m_current = 0x0;
    return A_A;
}
//============================================================================//
x_result
TheMainScreen::sSpellsFlushX()
{
	Actions& actions = fActionsA();
    for(szt i = 0; i < actions.size(); ++i)
	{
		sSpellX(actions[i]);
	}
	actions.clear();
	return A_A;
}
//============================================================================//
x_result
TheMainScreen::sSpellX(const XD_SPELL& _action)
{
	if(_action.fEql(TheMatch3::s_res_ready));
	{
		mXD_RESULTA( fSceneCreateX() );
		m_res_loaded = true;
	}
    return A_A;
}
//============================================================================//
x_result
TheMainScreen::fSceneCreateX()
{
	mBLOCK("MainScreen:Bacground");
	XD_I2 scene_size = XD::OS::iWdgtSizeA( m_game.fWdgtA(0) );
	XD::GFX::TEXTURE* texture = m_game.fTextureManagerA().fTextureGet(g_textures[T_BGR]);
	C4_IMG* img = 0x0;
	if(!texture) return X_X;

	img = 	C4_IMG::iCreateP( texture, XD_F2( scene_size.x, scene_size.y ),
			XD::GFXEX::INTERIOR_WORLD_LOCAL, &m_game.fGfxExA(),
			&m_game.fSceneA().root, &m_game.fSceneA().root.fTransformA() );
	mBLOCK("Btn ");
	texture = m_game.fTextureManagerA().fTextureGet(g_textures[T_SPHERE_5]);
	m_btn_start = 	C4_IMG::iCreateP( texture, texture->fSizeF2(),
					XD::GFXEX::INTERIOR_WORLD_LOCAL, &m_game.fGfxExA(),
					img, &img->fTransformA() );
	m_btn_start->fTransformA().fMoveA(350, 100, 0);

	texture = m_game.fTextureManagerA().fTextureGet(g_textures[T_SPHERE_7]);
	m_btn_quit = 	C4_IMG::iCreateP( texture, texture->fSizeF2(),
					XD::GFXEX::INTERIOR_WORLD_LOCAL, &m_game.fGfxExA(),
					img, &img->fTransformA() );
	m_btn_quit->fTransformA().fMoveA(350, -100, 0);
	return A_A;
}
//============================================================================//


