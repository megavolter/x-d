#include "TheMatch3.h"
#include "BASE/LANG.h"
#include "BASE/XD_F4.h"
#include "BASE/CODE.h"
#include "BASE/CAST.h"
#include "BASE/XD_I4.h"
#include "BASE/XD_I2.h"
#include "BASE/XD_MATH3D.h"

#include "TheMainScreen.h"
#include "Match3Game.h"

const XD_SPELL TheMatch3::s_res_ready("ResReady");
const XD_SPELL TheMatch3::s_game_start("GmStart_");
const XD_SPELL TheMatch3::s_game_quit("GmQuit__");
const XD_SPELL TheMatch3::s_play("LoadLVL_");
const XD_SPELL TheMatch3::s_scene_main("Main____");
const XD_SPELL TheMatch3::s_scene_game("Game____");

static str g_textures[] = {
	"g/Arrow.png",
};
//============================================================================//
TheMatch3::TheMatch3()
	: m_scene_context(0x0)
{
	fClearX();
}
//============================================================================//
si4
TheMatch3::fInitX(const GameConfig& _conf)
{
	m_game_config = _conf;
	mXD_RESULTA(sInitSubSystems());
	mXD_RESULTA(sInitGame(_conf));
	mXD_RESULTA(m_shader_manager.fInitX(&m_gfx));

	mCOMMENT("Cursor create");

	XD_WWW on_res_loaded(RCAST(XD_WWW::www, cbResourcesLoaded), this, 0x0);
	fTextureManagerA().fLoadTexturesX(g_textures, 1, on_res_loaded);
	return A_A;
}
//============================================================================//
sip
TheMatch3::cbTextureLoaded(TheMatch3* _this, ptr)
{
	return A_A;
}
//============================================================================//
sip
TheMatch3::cbResourcesLoaded(TheMatch3* _this, ptr)
{
    _this->fActionsA().push_back(TheMatch3::s_res_ready);
	return A_A;
}
//============================================================================//
x_result
TheMatch3::fActionsUpdateX()
{
	Actions& actions = fActionsA();
	if(actions.empty()) return A_A;
	for(szt i = 0; i < actions.size(); ++i)
	{
        mXD_RESULTA(fActionHandleX( actions[i] ));
	}
	actions.clear();
	return A_A;
}
//============================================================================//
//============================================================================//
x_result
TheMatch3::fActionHandleX(const XD_SPELL& _action)
{
	if(_action.fEql(TheMatch3::s_res_ready))
	{
		mBLOCK("Cursor creation");
		XD::GFX::TEXTURE* texture = fTextureManagerA().fTextureGet(g_textures[0]);
		if(0x0 == texture) mFAIL("Load cursor");
		XD_ATL_FRM frame(texture);
		m_scene.cursor.fNameA().fSet("cursor");
		m_scene.cursor.fInitFrameX(frame);
		m_scene.cursor.fTransformA().fCenterA(	-texture->size.x/2,
												-texture->size.y/2, 0);
		m_scene.cursor.fSettingsA().fSetInteriorA(XD::GFXEX::INTERIOR_WORLD_LOCAL)
				.fSetIsTextureUsesA(true);
		m_scene.cursor.fRenderSet(&fGfxExA());
		m_scene.cursor.fvPostInitX(0x0);
		m_scene.cursor.fLayerA() = 1000;
		m_scene.root.fChildAdd(&m_scene.cursor);

		fSceneLoadX(TheMatch3::s_scene_main);
		return A_A;
	}
	else
	if(_action.fEql(TheMatch3::s_game_start))
	{
		mBLOCK("Go to scene game");
		fSceneLoadX(s_scene_game);
	}
	return A_A;
}
//============================================================================//
si4
TheMatch3::sInitSubSystems()
{
mBLOCK("Creating widget");
	m_main_wdgt = XD::OS::iWdgtCreate();
	mXD_RESULTA(XD::OS::iWdgtInitX("match3_",
									0,0,
									m_game_config.wdgt_size.x,
									m_game_config.wdgt_size.y,
									m_main_wdgt));
	mXD_RESULTA(XD::OS::iWdgtShowX(true, m_main_wdgt));

	m_os.fInputKeyListenerA().fMethodSetA(
			RCAST(XD_WWW::www, cbInput)).fObjectSetA(this);
	m_os.fInputMouseListenerA().fMethodSetA(
			RCAST(XD_WWW::www,cbMouse)).fObjectSetA(this);

	si4 draw_d = XD::OS::iWdgtDescriptorGet(m_main_wdgt);

mBLOCK("Initialize graphics subsystem");
	mXD_RESULTA(m_gfx.fInitX(draw_d));
	return A_A;
}
//============================================================================//
si4
TheMatch3::sInitGame(const GameConfig& _conf)
{
	m_game_state.fClear();
	XD::OS::iSystemCurentDirChange("resources/");
	if(false)
	{
		mat4_set_perspective(	&m_gfx.fTransformGlobalA(), m_game_config.fov,
								m_game_config.wdgt_size.x/m_game_config.wdgt_size.y,
								0, 10000);
	}
	else if(true)
	{
		mat4_set_orthographic(	&m_gfx.fTransformGlobalA(),
								m_game_config.wdgt_size.x / -2. ,
								m_game_config.wdgt_size.x / 2. ,
								m_game_config.wdgt_size.y / -2. ,
								m_game_config.wdgt_size.y / 2. ,
								0, 10000);
	}
	return A_A;
}
//============================================================================//
si4
TheMatch3::fClearX()
{
	m_game_config.game_state = "Start";

	return A_A;
}
//============================================================================//
si4
TheMatch3::fDrawTickX()
{
	mXD_RESULTA(m_gfx.fSceneBeginX());
	mXD_RESULTA(m_gfx.fSceneClearX(true, false, false));
	{
		sPreRender();
		if(m_scene_context) mXD_RESULTA(m_scene_context->fvPreRenderX());
		sRender();

	}
	mXD_RESULTA(m_gfx.fSceneEndX());

	return A_A;
}
//============================================================================//
void
TheMatch3::sPreRender()
{
	sTasksUpdate();
	fActionsUpdateX();
	mCOMMENT("Cursor update");
    m_scene.cursor.fTransformA().fPositionSetA(	m_onscreen_mouse_pos.x,
												m_onscreen_mouse_pos.y, 0);
}
//============================================================================//
void
TheMatch3::sRender()
{
	C4::iForward(&m_scene.root, C4::TASK_DEFAULT,
                 C4::TASK_TYPE_DRAW, RCAST(XD::GFX*, &fGfxExA()));

    mTODO("Draw cursor");
}
//============================================================================//
void
TheMatch3::sTasksUpdate()
{
	if(m_tasks.empty()) {
		return;
	}

	for(szt i = 0; i < m_tasks.size(); ++i)
	{
		XD_WWW& task = m_tasks[i];
		task.fRunX();
	}
	m_tasks.clear();
}
//============================================================================//
si4
TheMatch3::fRunX()
{
	m_game_state.fStart();

mBLOCK("Ticks game modules");
	while(true == m_game_state.fIsRusning()){
		mXD_RESULTA( m_os.fSyncX() );
		mXD_RESULT_E( m_texture_manager.fUpdateX() );
		mXD_RESULTA( fDrawTickX() );
	}

mBLOCK("Game loop terminated");
	mNOTE("Game finished correctly! The reason " << m_game_state.fExitReason());

	return A_A;
}

//============================================================================//
si4
TheMatch3::fKeyHandleX(XDE_INPUT _type, XDE_KEY _key)
{
    fUserActivityA().fActivityAdd(UserActivity::KeyAction(_type, _key));
	return A_A;
}
//============================================================================//
si4
TheMatch3::fStopX(const char* _reason)
{
	m_game_state.fExit(_reason);
	return A_A;
}
//============================================================================//
si4
TheMatch3::fMousePosX(XD_I2 _pos)
{
	XD_I2 wdgt_size = XD::OS::iWdgtSizeA(fWdgtA(0));
	m_onscreen_mouse_pos.fSet(	_pos.x - wdgt_size.x / 2.,
								wdgt_size.y - _pos.y - wdgt_size.y / 2.);
	return A_A;
}
//============================================================================//
XD_I2&
TheMatch3::fInputPointA()
{
	return m_onscreen_mouse_pos;
}
//============================================================================//
// Static.
sip
TheMatch3::cbInput(TheMatch3* _this, const XD::OS::INPUT_KEY* _key)
{
	mXD_CHECK_ERR(_key == 0x0, "_key is empty", return X_X );
	mASSERT(_this, "No object to run");
	_this->fKeyHandleX(_key->type, _key->key);

	return A_A;
}
//============================================================================//
// Static.
sip
TheMatch3::cbMouse(TheMatch3* _this, const XD_I4* _pos)
{
	_this->fMousePosX(XD_I2(_pos->x, _pos->y));
	return A_A;
}
//============================================================================//
XD::GFXEX&
TheMatch3::fGfxExA()
{
	return m_gfx;
}
//============================================================================//
TheTextureManager&
TheMatch3::fTextureManagerA()
{
	return m_texture_manager;
}
//============================================================================//
TheShaderManager&
TheMatch3::fShaderManagerA()
{
	return m_shader_manager;
}
//============================================================================//
XD::OS::WDGT_T&
TheMatch3::fWdgtA(si4)
{
    return m_main_wdgt;
}
//============================================================================//
void
TheMatch3::fSpellHandle(const XD_SPELL& _spell)
{
	if(_spell.fEql("Start"))
		m_tasks.push_back(XD_WWW(RCAST(XD_WWW::www,doStart), this));
}
//============================================================================//
sip
TheMatch3::doStart(TheMatch3* _this, void*)
{
	return 0x0;
}
//============================================================================//
x_result
TheMatch3::fSceneLoadX(const XD_SPELL& _scene_id)
{
    if(m_scene_context)
	{
		mXD_RESULTA(m_scene_context->fvReleaseX());
		delete m_scene_context;
		m_scene_context = 0x0;
	}
	if(_scene_id.fEql(s_scene_main))
	{
		m_scene_context = new TheMainScreen(*this);
	}
	if(_scene_id.fEql(s_scene_game))
	{
		m_scene_context = new M3Game(*this);
	}

	if(m_scene_context) m_scene_context->fvInitX();

	return A_A;
}
//============================================================================//
TheMatch3::Scene&
TheMatch3::fSceneA()
{
	return m_scene;
}
//============================================================================//
