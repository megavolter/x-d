//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Autor: UCKUHT
// Date: 2014-10-12
// Desc: main class of match3 game.
//============================================================================//

#pragma once
#include "../../BASE/CODE.h"
#include "../../BASE/TYPE.h"
#include "BASE/XD_SPELL.h"
#include <vector>

class TheMatch3;

class IScene
{
xd_open_types:
	typedef std::vector<XD_SPELL> Actions;

xd_accessors:
	Actions&	fActionsA() { return m_actions; }

xd_data:
	Actions				m_actions;

};

class ISceneContext : public IScene
{
xd_functional:
	virtual x_result fvInitX() = 0;
	virtual x_result fvPreRenderX() = 0;
	virtual x_result fvReleaseX() = 0;
	virtual ~ISceneContext() {}
};
