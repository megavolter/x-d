#include "TheM3Shaders.h"

const char*
TheM3Shaders::Vertex::image()
{
	return	"attribute vec4 a_pos;\n"
			"attribute vec2 a_tex;\n"
			"varying vec2 v_tex;\n"
			"\n"
			"void main()\n"
			"{\n"
			"   v_tex = a_tex;\n"
			"   gl_Position = vec4(a_pos);\n"
			"}\n"
	;
}

const char*
TheM3Shaders::Pixel::image()
{
	return	"varying vec2 v_tex;\n"
			"void main()\n"
			"{\n"
			"   gl_FragColor = vec4(1,1,1,1);\n"
			"}"
	;
}
