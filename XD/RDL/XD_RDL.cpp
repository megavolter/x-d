#include <stdio.h>
#include <sys/stat.h>


#include "BASE/CAST.h"
#include "BASE/LOG.h"
#include "BASE/CODE.h"
#include "BASE/LANG.h"

#include "RDL/XD_RDL.h"

namespace nXD{
//============================================================================//
bool
gDataSyncLoad(RDL::DATA_ON_LOAD& l_data)
{
	mASSERT(l_data.data != 0x0, "Invalid income parameter");
	if(l_data.data->data)
	{//1
		mERRORF("reloading data");
	}//1
	if(l_data.path.fSize())
	{//1
		struct stat fi;
		if(0 != stat(l_data.path.fGet(), &fi)){
			mERRORP("No access to file "<<mBRACED(l_data.path.fGet()));
			return false;
		}
		FILE* file = fopen(l_data.path.fGet(), "rb");
		if(file)
		{//2
			fseek(file, 0, SEEK_END);
			szt file_size = ftell(file);
			if(file_size)
			{//3
				l_data.data->fAlloc(file_size);
				fseek(file, 0, SEEK_SET);
				if(file_size == fread(l_data.data->data, 1, file_size, file))
				{//4 Reading ok.
					l_data.loaded = true;
					return true;
				}//4
				l_data.data->fFree();
			}//3
			else
			{//3
				mERRORF("file is empty "<<l_data.path.fGet());
			}//3
			fclose(file);
		}//2
		else
		{//2
			mERRORF("file not exist "<<l_data.path.fGet());
		}//2
	}//1
	else
	{//1
		mERRORF("resource name is empty");
	}//1
	return false;
}
//============================================================================//
void
RDL::fDataLoad(str _request, const XD_WWW& _customer)
{// Prepare data on load.
	mCODE_BLOCK("Checking loaded data array");
	if(m_data_loaded.size())
	{//1 Need to check if request allready loaded.
		data_list_iter_t iter;
		for(iter = m_data_loaded.begin(); iter != m_data_loaded.end(); iter++)
		{//2
			DATA_ON_LOAD& data_ol = *iter;
			if(data_ol.path.fCompare(_request))
			{//3 This request allready handled.
				mTODO("Check customer matching");
				mTODO("Check data allready loaded");
				data_ol.customers.push_back(_customer);
				return;
			}//3
		}//2
	}//1

	mCODE_BLOCK("Checking todo array");
	if(m_data_on_load.size())
	{//1 May be request allready handled.
		data_list_iter_t iter;
		for(iter = m_data_on_load.begin(); iter != m_data_on_load.end(); iter++)
		{//2
			DATA_ON_LOAD& data_ol = *iter;
			if(data_ol.path.fCompare(_request))
			{//3 Got here.
				data_ol.customers.push_back(_customer);
				return;
			}//3
		}//2
	}//1

	XD_STRING comparator;
	comparator.fSetLocal(_request);//Allocatingless, comparator is locked now.
	if(m_data_bank.size())
	{//1
		data_map_iter_t iter = m_data_bank.find(comparator);
		if(iter != m_data_bank.end())
		{//2 Requested data allready loaded. Copy meta to already loaded.
			const XD_STRING& s_g = (*iter).first;
			DATA_ON_LOAD d_t(s_g);
			mXD_WTF("Why const cast here? Fix it!");
			nXD::DATA* data_ptr = CCAST(nXD::DATA*, &(*iter).second);
			d_t.data = data_ptr;
			m_data_loaded.push_front(d_t);
			DATA_ON_LOAD& data_link = *m_data_loaded.begin();
			data_link.customers.push_back(_customer);
			return;
		}//2
	}//1

	mCOMMENT("Meta data save");
	nXD::DATA l_data;
	mTODO("Rm find(), use pair<> insert()");
	m_data_bank[comparator] = l_data;// Data bank insertion.
	data_map_iter_t iter_map = m_data_bank.find(comparator);// Tmp.
	nXD::DATA* p_data = &(*iter_map).second;
	const XD_STRING& c_sg = (*iter_map).first;
	DATA_ON_LOAD l_data_ol(c_sg);
	l_data_ol.data = p_data;
	m_data_on_load.push_front(l_data_ol);
	data_list_iter_t iter_list = m_data_on_load.begin();
	DATA_ON_LOAD& data_link = *iter_list;
	data_link.customers.push_back(_customer);
}
//============================================================================//
si4
RDL::fSyncX()
{
	if(m_data_loaded.size())
	{//1 Has prepared objects.
		data_list_iter_t iter;
		while(m_data_loaded.size())
		{//2
			iter = m_data_loaded.begin();
			DATA_ON_LOAD& data_ol = *iter;
			while(data_ol.customers.size())
			{//3 Alert.
				XD_WWW& alert = data_ol.customers.front();
				alert.fRunX(&data_ol);
				data_ol.customers.pop_front();
			}//3
			m_data_loaded.pop_front();
		}//2
		if(m_data_on_load.empty())
		{//2
			m_sync_listener.fRunX();
		}//2
	}//1

	if(m_data_on_load.size())
	{//1 Root of evil.
		data_list_iter_t iter;
		for(iter = m_data_on_load.begin(); iter!= m_data_on_load.end(); iter++)
		{//2
			DATA_ON_LOAD& data_ol = *iter;
			if(!gDataSyncLoad(data_ol))
			{//3
				mERRORF("Sync load fail "<<mBRACED(data_ol.path));
			}//3
			m_data_loaded.push_back(data_ol);// In.
			iter = m_data_on_load.erase(iter);// Out
		}//2
	}//1
	return A_A;
}
//============================================================================//
x_result
RDL::fInstantLoadX(out nXD::DATA& _result, str _request)
{
	XD_STRING file_path = _request;
	nXD::RDL::DATA_ON_LOAD load(file_path);
	load.data = &_result;
	if(!gDataSyncLoad(load))
		return nXD::BAD;
	file_path.fDrop();
	return nXD::OK;
}
//============================================================================//
XD_WWW&
RDL::fSyncListenerA()
{
	return m_sync_listener;
}

//============================================================================//
nXD::DATA*
RDL::fDataGetP(str _request)
{
	if(m_data_bank.size())
	{//1
		XD_STRING comparator;
		comparator.fSetLocal(_request);

		data_map_iter_t iter = m_data_bank.find(comparator);
		if(iter != m_data_bank.end())
		{//2
			nXD::DATA* d_p = &(*iter).second;
			return d_p;
		}//2
	}//1
	mWARNINGF("File "<<mBRACED(_request)<<"not loaded");
	return NULL;
}
//============================================================================//
nXD::DATA&
RDL::fDataGetA(str _request)
{
	nXD::DATA* result = fDataGetP(_request);
	mASSERT(result != 0x0, "file not loaded "<<mBRACED(_request));
	return *result;
}
//============================================================================//
si4
RDL::fDataUnloadX(nXD::DATA* _data)
{
	mFUNC_IMPL_ERR();
	mIGNOREP(_data);
	return X_X;
}
//============================================================================//
bool
RDL::fDataCheck(str name)
{
	data_map_iter_t result = m_data_bank.find(name);
	if(result != m_data_bank.end())
		return true;
	return false;
}
//============================================================================//
bool
RDL::fIsReady()
{
	return m_data_on_load.empty();
}
//============================================================================//

//============================================================================//
}//ns



