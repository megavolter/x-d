#ifndef _XD_RDL_H_INCLUDED
#define _XD_RDL_H_INCLUDED
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Autor: UCKUHT
// Date: 2013-05-19
// Desc: raw data loader. Do not use fopen like methods for getting data. This
//       isn't croosplatform way.
//============================================================================//
#include <map>
#include <list>

#include "BASE/XD_WWW.h"
#include "BASE/STRINGS/XD_STRING.h"
#include "BASE/XD_DATA.h"
#include "BASE/CODE.h"

namespace nXD{
//============================================================================//
class RDL
{
public xd_types:
	enum DATA_STATE{
		DATA_STATE_UNKNOWN = 0,
		DATA_STATE_NOT_READY,
		DATA_STATE_READY,
		DATA_STATE_
	};

	typedef std::list<XD_WWW>             customer_list_t;
	typedef std::list<XD_WWW>::iterator   customer_list_iter_t;

	struct DATA_ON_LOAD
	{
		DATA_ON_LOAD(const XD_STRING& cmp) : data(0), path(cmp), loaded(false) {}
	public:
		nXD::DATA*			data;
		customer_list_t		customers;
		const XD_STRING&	path;
		bln					loaded;
	};

	typedef std::list<DATA_ON_LOAD>				data_list_t;
	typedef std::list<DATA_ON_LOAD>::iterator	data_list_iter_t;
	typedef std::map<XD_STRING,nXD::DATA>			data_map_t;
	typedef std::map<XD_STRING,DATA>::iterator	data_map_iter_t;

public xd_functional:
	void		fDataLoad(str _request, const XD_WWW& _customer);
	nXD::DATA*	fDataGetP(str _request);
	x_result	fInstantLoadX(out nXD::DATA& _result, str _request);
	nXD::DATA&	fDataGetA(str _request);
	si4			fDataUnloadX(nXD::DATA* _data);
	bln			fDataCheck(const char* name);
	/// Runs www with DATA_ON_LOAD when finish loading object.
	si4			fSyncX();
	/// Returns listener ref.
	XD_WWW&		fSyncListenerA();
	/// Return true if is nothing to load.
	bool		fIsReady();

private xd_data:
	// Waiting to download resources.
	data_list_t		m_data_on_load;
	// Data loaded.
	data_list_t		m_data_loaded;
	// All resources.
	data_map_t		m_data_bank;
	// Load listener.
	XD_WWW			m_sync_listener;

};

}//ns

//============================================================================//

#endif // _XD_DATA_H_INCLUDED
