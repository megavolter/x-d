#include "MODULES/TIME/XD_TIME.h"
#include "BASE/CAST.h"

#include <windows.h>

namespace nXD{

fl4
TIME::iCurrentSystemTime(si4 _start_counter)
{
	si4 time = iGetCounter();
	return (time - _start_counter)/1000.f;
}

si4
TIME::iGetCounter()
{
	DWORD time = GetTickCount();
	return SCAST(si4, time);
}

}//ns

