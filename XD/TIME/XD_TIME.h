#include "BASE/CODE.h"
#include "BASE/TYPE.h"

namespace nXD
{

class TIME
{
xd_interface:
	static fl4 iCurrentSystemTime(si4 _start_counter = 0);
	static si4 iGetCounter();
};

}// ns

