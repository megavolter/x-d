#ifndef _C4_H_
#define _C4_H_
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Author: UCKUHT
// Date:
// Desc: component model v4. Base class.
//============================================================================//
#include <vector>
#include "BASE/STRINGS/XD_STRING.h"
#include "BASE/TYPE.h"
#include "BASE/XD_SPELL.h"
#include "BASE/LANG.h"
#include "BASE/CODE.h"

namespace nXD{
class C4
{
public:
	typedef C4* RESULT;

	struct COMPONENT
	{
		XD_SPELL component;
		void*    value;
	};

	enum TASK{
	    TASK_UNKNOWN,
		TASK_DEFAULT,
		TASK_SEARCH,
		TASK_
	};

	struct CHILD_SEARCH
	{
	    static const TASK type = TASK_SEARCH;
        C4* result;
	};

	enum TASK_TYPE{
	    TASK_TYPE_UNKNONW,
		// DEFAULTS.
		TASK_TYPE_DRAW,
		TASK_TYPE_UPDATE,
		// SEARCH.
		TASK_TYPE_INPUT_MOVE,
		TASK_TYPE_
	};

	typedef std::vector<C4*>                    c4_vec_t;
	typedef std::vector<C4*>::iterator          c4_vec_iter_t;
	typedef std::vector<C4*>::reverse_iterator  c4_vec_riter_t;

	typedef std::vector<COMPONENT>              c4_com_t;
	typedef std::vector<COMPONENT>::iterator    c4_com_iter_t;

public:
	C4() :  m_spell("c4_object"), m_name("C4"),  m_type("giglet"), m_layer(0),
			m_tag(0), m_childs(), m_components(), m_parent(0x0)
	{}

	virtual sip
	fvTask(TASK _task, TASK_TYPE _type, void* _data) {
		mIGNOREP(_task && _type && _data);
		return O_O;
	}

	virtual void*
	fvAccessP(const XD_SPELL& _request) {
		mIGNOREP(_request.fGetP());
		return 0x0;
	}

	virtual si4
	fvPostInitX(void* _in) {
		mIGNOREP(_in);
		return O_O;
	}

	virtual void    fvDestroy();
	virtual ~C4() {}

public:
	c4_vec_t&   fChilds();
	void        fChildAdd(C4* _child);
	void		fChildDelete(C4* _child);
	C4*         fParentGetP();
	void        fParentSet(C4* _parent) { m_parent = _parent; }
	void		fRemoveFromParent();
	XD_STRING&  fNameA();
	szt         fComponentAdd(COMPONENT _com);
	void*       fComponentGet(const XD_SPELL& _cmp);

	XD_STRING&  fTypeA()  { return m_type;  }
	XD_SPELL&   fSpellA() { return m_spell; }
	si4&        fTagA()   { return m_tag;   }
	fl4&        fLayerA() { return m_layer; }

public:
	static  bool iIsLesser(C4* _lop, C4* _rop);
	static  C4*  iNewP() { return new C4; }

public:// Special interface.
	static sip iForward(C4* _root, TASK _task, TASK_TYPE _type, void* _data);
	static sip iBackward(C4* _root, TASK _task, TASK_TYPE _type, void* _data);

	static C4* iFwdSearch(C4* _root, TASK_TYPE _type, void* _data);
	static C4* iBwdSearch(C4* _root, TASK_TYPE _type, void* _data);

public:// Accessors;
	static XD_SPELL iSpellTransform() { return XD_SPELL("transfrm"); }
	static XD_SPELL iSpellAtlFrame() { return XD_SPELL("atl_frm"); }
	static XD_SPELL iSpellGeometry() { return XD_SPELL("geometry"); }

protected:
	XD_SPELL    m_spell;
	XD_STRING   m_name;
	XD_STRING   m_type;
	fl4         m_layer;
	si4         m_tag;
	c4_vec_t    m_childs;
	c4_com_t    m_components;
	C4*         m_parent;
};
}//nXD
//============================================================================//
#endif
