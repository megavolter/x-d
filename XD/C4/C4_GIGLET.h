#ifndef _C4_GIGLET_H
#define _C4_GIGLET_H
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Autor: UCKUHT
// Date: 2014-01-30
// Desc: layer, just for childs holder.
//============================================================================//
#include "COMMON/C4/C4.h"
//============================================================================//
class C4_GIGLET : public C4
{
public:
    C4_GIGLET();
    virtual sip fvTask(si4 task, si4 type, void* data);
};
//============================================================================//
#endif // _C4_GIGLET_H
