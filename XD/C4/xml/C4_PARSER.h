#pragma once

#include <vector>
#include "C4/C4.h"
#include "pugi/xml.h"
#include "BASE/XD_WWW.h"
#include "BASE/CODE.h"
#include "BASE/XD_DATA.h"

using namespace pugi;

namespace nXD{
class C4_PARSER
{
public:
	struct VALUES
    {
        pugi::xml_node  in;
        C4*             result;
        C4*             parent;

    public:
        VALUES() : in(), result(0x0), parent(0x0) {}
    };

    struct CUSTOM
    {
        XD_SPELL    comparator;
        XD_WWW      fabric;

     public xd_functional:
	 	 virtual x_result fFabricate(const VALUES& _node){
    	 	 return nXD::A_A;
     	 }

    public xd_functional:
        CUSTOM(str _name) : comparator(_name) {}

        CUSTOM( const XD_SPELL& _comparator, const XD_WWW& _constructor) {
            fSet(_comparator, _constructor);
        }

        void
        fSet(const XD_SPELL& _comparator, const XD_WWW& _constructor) {
            comparator = _comparator;
            fabric = _constructor;
        }
    };



    typedef std::vector<CUSTOM> custom_t;

public:
    custom_t mCustomTabe;
    bool     mIsMissDisplay;
    szt      mSize;
    szt      mMaxSize;

public:
    C4_PARSER(si4 _size);
    si4 fAddX(const CUSTOM& _custom);
    template<typename T>
    si4 fAddX(const XD_SPELL& _comparator, const T& _constructor) {
    	return fAddX(_comparator, RCAST(XD_WWW&, _constructor));
    }
    si4 fAddX(const XD_SPELL& _comparator, const XD_WWW& _constructor);// Heap.
    x_result fParseX(nXD::DATA& _data, C4* _parent);
    si4 fParseX(pugi::xml_node& _root, VALUES& _target);
    si4 fParseChildsX(VALUES& _target);
    void fDisplayMissingObjects(bool _value);

private:
    CUSTOM* sConstructorFind(const XD_SPELL& _name);

};
}//nXD

