#include "C4_PARSER.h"
#include "BASE/LOG.h"
#include "BASE/CODE.h"
namespace nXD{
//============================================================================//
C4_PARSER::C4_PARSER(si4 _size)
    : mCustomTabe(), mIsMissDisplay(false)
{
    mCustomTabe.reserve(_size);
    mMaxSize = _size;
    mSize = 0;
}
//============================================================================//
void
C4_PARSER::fDisplayMissingObjects(bool _value)
{
    mIsMissDisplay = _value;
}
//============================================================================//
x_result
C4_PARSER::fParseX(nXD::DATA& _data, C4* _parent)
{
	pugi::xml_document doc;
	pugi::xml_parse_result r = doc.load_buffer_inplace(_data.fDataP(), _data.fSize());
	if(!r) {
		mERRORP(r.description());
		return X_X;
	}
	VALUES init_values;
	init_values.parent = _parent;
	pugi::xml_node f = doc.first_child();
	mXCALL(fParseX(f, init_values));

	return A_A;
}
//============================================================================//
si4
C4_PARSER::fParseX(pugi::xml_node& _root, VALUES& _target)
{
    XD_SPELL root_name(_root.name());
    if(root_name.fEql(""))
    {
        mERRORF("Object must to have name");
        return X_X;
    }

    CUSTOM* constructor = sConstructorFind(root_name);
    if(0x0 == constructor)
    {
        if( true == mIsMissDisplay )
        {
            mNOTE("Fabric not found "<<mQUOTED(root_name.fGetP()));
            return 0x0;
        }
    }

    VALUES target;

    target.in = _root;
    target.result = 0x0;
    target.parent = _target.parent;

    si4 l_res = (constructor->fabric.fIsEmpty())?
    		constructor->fFabricate(target)
    		: constructor->fabric.fRunX(&target);
    C4* curent_object = target.result;

    if (0x0 != curent_object )
    {//2 Object constructed successfuly.
        switch(l_res)
        {//3
        case A_A : // Do parse childs of this node.
            target.parent = curent_object;
            target.result = 0x0;
            target.in = _root;
            l_res = fParseChildsX(target);
            break;
        case O_O : // Do not parse childs.
            _target.result = target.result;
            break;
        case X_X : // Error handled.
            mERRORF(mPFUNC << mLINE);
            return X_X;
            break;
        }//3
    }//2

    if(curent_object)
        curent_object->fvPostInitX(0x0);

    _target.result = curent_object;

    return l_res;
}
//============================================================================//
si4
C4_PARSER::fParseChildsX(VALUES& _target)
{
    pugi::xml_node childs = _target.in.first_child();
    if(!childs)
        return O_O;

    VALUES child;
    child.result = 0x0;
    child.parent = _target.parent;

    for(pugi::xml_node node = childs; node; node = node.next_sibling())
    {//1
        const char* name = node.name();
        if(name[0] == '_')
            continue;

        mXD_RESULT_E(fParseX(node, child));
        if(0x0 != child.result)
        {//2
            _target.parent->fChildAdd(child.result);
        }//2
    }//1
    return A_A;
}
//============================================================================//
C4_PARSER::CUSTOM*
C4_PARSER::sConstructorFind(const XD_SPELL& _name)
{
    for(ui4 i = 0; i < mCustomTabe.size(); ++i)
    {// Searching for constructor.
        CUSTOM& curent = mCustomTabe[i];
        if(true == curent.comparator.fEql(_name))
        {
            return &curent;
        }
    }
    return 0x0;
}
//============================================================================//
si4
C4_PARSER::fAddX(const CUSTOM& _custom)
{
    mCustomTabe.push_back(_custom);
    return A_A;
}
//============================================================================//
si4
C4_PARSER::fAddX(const XD_SPELL& _comparator, const XD_WWW& _constructor)
{
    return fAddX( CUSTOM(_comparator, _constructor) );
}
//============================================================================//
}//nXD





