#include "C4_POS.h"
#include "BASE/CAST.h"
#include "BASE/CODE.h"
//============================================================================//
void*
C4_POS::fvAccessP(const XD_SPELL& _request)
{
	if(_request.fEql(C4::iSpellTransform()))
		return RCAST(void*, &m_pos);
	return 0x0;
}
//============================================================================//
si4
C4_POS::fvPostInitX(void*)
{
	mBLOCK("Parent transform");
	if(0x0 != m_parent)
	{// Has parent.
		XD_TRTR* tr_ptr = SCAST(XD_TRTR*,
								m_parent->fvAccessP(C4::iSpellTransform()));
		if(0x0 != tr_ptr)
		{// Parent has transform.
			fTransformA().fParentSetA(tr_ptr);
		}
	}
	return A_A;
}
//============================================================================//
