#ifndef _C4_POS_H_INCLUDED
#define _C4_POS_H_INCLUDED
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Author: UCKUHT
// Date: 2014-08-03
// Desc: Positionable c4 object.
//============================================================================//
#include "COMMON/C4/C4.h"
#include "BASE/XD_SPELL.h"
#include "BASE/XD_TRTR.h"

class C4_POS : public C4
{
protected:
	XD_TRTR     m_pos;

public:
	C4_POS() : C4(), m_pos(1) {}
	static C4_POS*      iNewP() { return new C4_POS; }

public:
	virtual void*       fvAccessP(const XD_SPELL& _request);
	virtual si4         fvPostInitX(void*);

public:
	XD_TRTR&            fTransformA() { return m_pos; }

};
//============================================================================//
#endif // _C4_POS_H_INCLUDED
