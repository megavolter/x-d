# C4

# Directories.
set( XD_${MODULE_NAME}_DIR
	${XD_DIR}/XD/C4/
)

# Sources.
set( XD_${MODULE_NAME}_SRC
		${XD_${MODULE_NAME}_DIR}C4.cpp
)
# Includes.
set( XD_${MODULE_NAME}_INC
	
)

# Parser.
if( C4_XML )

	set( XD_${MODULE_NAME}_SRC ${XD_${MODULE_NAME}_SRC}
			${XD_${MODULE_NAME}_DIR}xml/C4_PARSER.cpp
			${XD_DIR}/SOLUTIONS/pugi/xml.cpp
	)
	
endif( C4_XML )
