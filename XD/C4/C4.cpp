//============================================================================//
#include "C4.h"
#include "BASE/LANG.h"
#include "BASE/CODE.h"
namespace nXD{
//============================================================================//
C4::c4_vec_t&
C4::fChilds()
{
	return m_childs;
}
//============================================================================//
void
C4::fChildAdd(C4* _child)
{
	if(_child)
    {
        _child->fParentSet(this);
        fChilds().push_back(_child);
    }
}
//============================================================================//
C4*
C4::fParentGetP()
{
	return 0x0;
}
//============================================================================//
bool
C4::iIsLesser(C4* lop, C4* rop)
{
	mIGNOREP(lop && rop);
	return false;
}
//============================================================================//
sip
C4::iForward(C4* _root, TASK _task, TASK_TYPE _type, void* _data)
{
	sip res = _root->fvTask(_task, _type, _data);
	if(A_A == res)
		return RCAST(sip, _root);

	C4::c4_vec_t& vector = _root->fChilds();
	if(vector.size())
	{//1 Childs first/
		C4::c4_vec_iter_t iter;
		C4* prev = 0x0;
		for(iter = vector.begin(); iter != vector.end(); iter ++)
		{
			C4* object = *iter;
			sip result = C4::iForward(object, _task, _type, _data);
			if(result)
			{
				return result;
			}
			if(0x0 != prev)
			{
				if(object->fLayerA() < prev->fLayerA())
				{// Simple sort.
					*iter = prev;
					*(--iter) = object;
				}
			}
			prev = object;
		}
	}//1
	return 0x0;
}
//============================================================================//

//============================================================================//
sip
C4::iBackward(C4* _root, TASK _task, TASK_TYPE _type, void* _data)
{
	C4::c4_vec_t& vector = _root->fChilds();
	if(vector.size())
	{//1 Childs first/
		C4::c4_vec_riter_t iter;
		for(iter = vector.rbegin(); iter != vector.rend(); iter ++)
		{
			C4* object = *iter;
			sip result = C4::iBackward(object, _task, _type, _data);
			if(result)
			{
				return result;
			}
		}
	}//1
	sip res = _root->fvTask(_task, _type, _data);
	if(A_A == res)
		return RCAST(sip, _root);
	return 0x0;
}
//============================================================================//
XD_STRING&
C4::fNameA()
{
	return m_name;
}
//============================================================================//
void
C4::fvDestroy()
{
	delete this;
}
//============================================================================//
C4*
C4::iFwdSearch(C4* _root, TASK_TYPE _type, void* _data)
{
	return RCAST(C4*, iForward(_root, TASK_SEARCH, _type, _data) );
}
//============================================================================//
C4*
C4::iBwdSearch(C4* _root, TASK_TYPE _type, void* _data)
{
	return RCAST(C4*, iBackward(_root, TASK_SEARCH, _type, _data) );
}
//============================================================================//
szt
C4::fComponentAdd(COMPONENT _com)
{
	mOPTIMFUNC("Need using sorted list");
	m_components.push_back(_com);
	return m_components.size();
}
//============================================================================//
void*
C4::fComponentGet(const XD_SPELL& _cmp)
{
	for(szt i = 0; i < m_components.size(); ++i)
	{
		COMPONENT& cur = m_components[i];
		if(cur.component.fAsNumber() == _cmp.fAsNumber())
		{
			return cur.value;
		}
	}
	return 0x0;
}
//============================================================================//
void
C4::fChildDelete(C4* _child)
{
    for (c4_vec_t::iterator c = m_childs.begin(); c != m_childs.end(); ++c)
	{
		C4* obj = *c;
		if(obj == _child)
		{
			obj->fParentSet(0x0);
			m_childs.erase(c);
			return;
		}
	}
}
//============================================================================//
void
C4::fRemoveFromParent()
{
	if(fParentGetP())
		fParentGetP()->fChildDelete(this);
}
//============================================================================//
}//nXD


















