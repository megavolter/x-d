#include "M3_APP.h"
#include "BASE/XD_I2.h"
#include "BASE/CAST.h"
#include "BASE/STRINGS/XD_STRING.h"
#include "ScreenTitle.h"

using nXD::OS;

M3App::M3App()
{}

M3App::~M3App()
{}

x_result
M3App::fInitX()
{
mBLOCK("Prepare to start");
	XD_STRING working_dir;
	OS::iGetWorkingDirectory(working_dir);
	mNOTE(working_dir.fGet());

mBLOCK("App start");
	m_data.wdgt = OS::iWdgtCreate();
	XD_I2 screen_size;
	OS::iScreenSizeGetX(&screen_size);
	mXD_RESULTA(OS::iWdgtInitX("The M3", screen_size.x/4, screen_size.y/4,
				screen_size.x*0.6, screen_size.y*0.6, m_data.wdgt));
	m_input_keys_handler.what = RCAST(XD_WWW::www, iKeyHandle);
	m_input_keys_handler.who = this;
	m_data.os.fInputKeyListenerA() = m_input_keys_handler;
	OS::iWdgtShowX(true, m_data.wdgt);

mBLOCK("Gfx start");
	sip descriptor = OS::iWdgtDescriptorGet(m_data.wdgt);
	mXD_RESULTA( m_data.gfx.fInitX(descriptor) );
	XD_STRING gfx_version;
	m_data.gfx.fVersionX(gfx_version);
	mNOTE(gfx_version.fGet());

mBLOCK("Create scene");
	m_data.scene = ScreenTitle::iCreate(m_data);
	return A_A;
}

x_result
M3App::fPlayX()
{
	mXD_CHECK(m_data.scene);
	m_data.playing = true;
	mBLOCK("M3App Main Loop");
	while(m_data.playing)
	{
		mXD_RESULTA(sUpdateX());
	}
	mNOTE(m_data.reason);
	return A_A;
}

x_result
M3App::fStopX(str _reason)
{
	m_data.playing = false;
	m_data.reason = _reason;
	return A_A;
}

x_result
M3App::sUpdateX()
{
mCOMMENT("Get messages from os");
	m_data.os.fSyncX();

mCOMMENT("Render current frame");
	m_data.gfx.fFrameA().fBeginX();
	m_data.gfx.fFrameA().fClearX(true, false, false);
	m_data.gfx.fFrameA().fClearColorX(XD_COLOR(XD_COLOR::eNiceSky));
	mXD_RESULTA(m_data.scene->fRun());
	m_data.gfx.fFrameA().fEndX();
	m_data.gfx.fFrameA().fDrawX();

	return A_A;
}

x_result
M3App::fDestroyX()
{
	mXD_RESULTA(OS::iWdgtDestroyX(m_data.wdgt));
	mXD_RESULTA(m_data.os.fDestroyX());
	mXD_RESULTA(m_data.gfx.fDestroyX());
	return A_A;
}

sip
M3App::iKeyHandle( M3App* _this, nXD::OS::INPUT_KEY* _key)
{
	if(_key->type == XDE_INPUT_DOWN
		&& _key->key == XDE_KEY_ESC)
		_this->fStopX("Application terminated by user.");
	return 0x0;
}
