#ifndef M3_M3_APP_H_
#define M3_M3_APP_H_

#include "MODULES/OS/XD_OS.h"
#include "MODULES/GFX_V2/XD_GFX.h"
#include "MODULES/RDL/XD_RDL.h"
#include "BASE/XD_WWW.h"
#include "M3_SCENE.h"

#include "M3_APP_DATA.h"

class M3App
{
	struct AppData
	{
		bln play;
		str reason;
		M3_SCENE* scene;

		void fInit()
		{
			play = false;
			reason = 0x0;
			scene = 0x0;
		}
	};

xd_data:
	M3_APP_DATA m_data;
	XD_WWW m_input_keys_handler;

xd_functional:
	M3App();
	~M3App();

	x_result fInitX();
	x_result fPlayX();
	x_result fStopX(str _reason);
	x_result fDestroyX();

xd_special:
	x_result sUpdateX();

xd_interface:
	static sip iKeyHandle(M3App* _this, nXD::OS::INPUT_KEY* _key);
};


#endif /* M3_M3_APP_H_ */
