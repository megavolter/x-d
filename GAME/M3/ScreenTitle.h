#ifndef M3_SCREENTITLE_H_
#define M3_SCREENTITLE_H_

#include "M3_SCENE.h"
#include "BASE/XD_MATH3D.h"

class ScreenTitle : public M3_SCENE
{
xd_data:
	XD_F44 m_view;
	nXD::nGFX::RENDER::VertexBuffer5 m_draw_buffer;

xd_special:
	void sSetUpDrawData();

xd_interface:
	static M3_SCENE* iCreate(M3_APP_DATA& _data)
	{
		M3_SCENE* result = new ScreenTitle(_data);
		result->fStateA().fMethodSetA(RCAST(XD_WWW::www, iInit)).fObjectSetA(result);
		return result;
	}
	static void iDestroy(M3_SCENE* _scene) { delete _scene; }

xd_functional:
	ScreenTitle(M3_APP_DATA& _data) : M3_SCENE(_data) {}

	x_result fInitX()
	{
		mDEV(mPFUNC);
		fStateA().fObjectSetA(this).fMethodSetA(RCAST(XD_WWW::www, iLoading));
		return A_A;
	}

	x_result fLoading();

	x_result fPlaying();
xd_interface:
	static sip iLoading(ScreenTitle* _this, void*)
	{
		return _this->fLoading();
	}

	static sip iPlaying(ScreenTitle* _this, void*)
	{
		return _this->fPlaying();
	}

	static sip iInit(ScreenTitle* _this, void*)
	{
		return _this->fInitX();
	}
};

#endif
