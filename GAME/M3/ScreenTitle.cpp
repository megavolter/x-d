/*
 * ScreenTitle.cpp
 *
 *  Created on: 31 июл. 2017 г.
 *      Author: uckuht
 */

#include "ScreenTitle.h"

#include "EXT/PROGRAMS_BANK/CASUAL_PROGRAMS_BANK.h"
#include "BASE/XD_FIGURES_3D.h"

void
ScreenTitle::sSetUpDrawData()
{
	m_draw_buffer.data = nXD::nGFX::RENDER::VertexBuffer5::iCreate(4);
	for (szt i = 0; i < 4; ++i)
	{
		m_draw_buffer.data[i].x = nXD::Figures3D::quad_pos[i][0];
		m_draw_buffer.data[i].y = nXD::Figures3D::quad_pos[i][1];
		m_draw_buffer.data[i].z = nXD::Figures3D::quad_pos[i][2];
		m_draw_buffer.data[i].u = nXD::Figures3D::quad_tex[i][0];
		m_draw_buffer.data[i].v = nXD::Figures3D::quad_tex[i][1];
	}

	m_data.gfx.fRenderA().fCompileX(m_draw_buffer);
}

x_result
ScreenTitle::fLoading()
{
	fl4 fow = 1;
	fl4 aspect = 1;
	fl4 near = -1;
	fl4 far = 1000;

	sSetUpDrawData();

	mat4_set_perspective(&m_view, fow, aspect, near, far);

	nXD::nGFX::SHADER::Program program;
	program.fClear();
	program.vertex = EXT_PROGRAMS_BANK::VERTEX::iMinimum();
	program.fragment = EXT_PROGRAMS_BANK::FRAGMENT::iMinimum();

	mXD_RESULTA(m_data.gfx.fShaderA().fCompileX(program));
	mXD_RESULTA(m_data.gfx.fShaderA().fSetX(program));

	m_data.gfx.fSettingsA();
	m_data.gfx.fRenderA();
	m_data.gfx.fFrameA();

	m_state.fMethodSetA(RCAST(XD_WWW::www, iPlaying));
	return A_A;
}

x_result
ScreenTitle::fPlaying()
{
	mXD_RESULTA(m_data.gfx.fRenderA().fSetX(m_draw_buffer));
	return A_A;
}

