
#include <iostream>

#include "BASE/CODE.h"
#include "BASE/TYPE.h"
#include "BASE/LANG.h"

#include "M3_APP.h"

x_result
gfPlay()
{
	M3App app;
	mBLOCK("App Init");
	mXD_RESULTA(app.fInitX());
	mBLOCK("App Play");
	mXD_RESULTA(app.fPlayX());
	mBLOCK("App destroy");
	mXD_RESULTA(app.fDestroyX());
	return A_A;
}

int
main()
{
	x_result return_status = gfPlay();
	return return_status == A_A? mXD_MAGIC("success",0) : mXD_MAGIC("fail", 1);
}
