/*
 * M3_APP_DATA.h
 *
 *  Created on: 23 июл. 2017 г.
 *      Author: uckuht
 */

#ifndef M3_M3_APP_DATA_H_
#define M3_M3_APP_DATA_H_

#include "MODULES/OS/XD_OS.h"
#include "MODULES/RDL/XD_RDL.h"
#include "MODULES/GFX_V2/XD_GFX.h"

class M3_SCENE;

struct M3_APP_DATA
{
	bln				playing;
	nXD::OS 		os;
	nXD::OS::WDGT_T wdgt;
	nXD::nGFX::GFX 	gfx;
	nXD::RDL		rdl;

	M3_SCENE*		scene;
	str		reason;
};

#endif /* M3_M3_APP_DATA_H_ */
