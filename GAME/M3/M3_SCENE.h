#ifndef M3_M3_SCENE_H_
#define M3_M3_SCENE_H_

#include "BASE/CODE.h"
#include "BASE/TYPE.h"

#include "MODULES/GFX_V2/XD_GFX.h"
#include "M3_APP_DATA.h"

class M3_SCENE
{
xd_shared:
	M3_APP_DATA& m_data;
	XD_WWW m_state;

xd_functional:
	M3_SCENE(M3_APP_DATA& _data);
	x_result fRun()
	{
		return m_state.fRunX();
	}

xd_accessors:
	XD_WWW& fStateA() { return m_state; }
};

#endif /* M3_M3_SCENE_H_ */
