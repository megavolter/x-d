#include "BASE/LANG.h"
#include "MODULES/OS/XD_OS.h"
#include "BASE/CAST.h"

#include "TEST_COMMON.h"

si4 os_test()
{
	mTEST_BEGIN("OS");

	nXD::OS os;
	nXD::OS::WDGT_T wdgt = nXD::OS::iWdgtCreate();

	mTEST_STEP("Window init");
	mXD_CHECK(nXD::OS::iWdgtInitX("window_name", 0,0, 500, 500, wdgt));

	mTEST_STEP("Window show");
	mXD_CHECK(nXD::OS::iWdgtShowX(true, wdgt));

	si4 test = 0;
	class OsTestListener
	{
	public:
		static sip run(XD_WWW* _lnr, nXD::OS::INPUT_KEY* _data)
		{
			if(_data->key == XDE_KEY_MOUSE_L)
			{
				*(si4*)_lnr->why = 0x1;
			}
			return A_A;
		}
	};

	XD_WWW& lnr = os.fInputKeyListenerA();
	lnr.what = RCAST(XD_WWW::www, OsTestListener::run);
	lnr.why = &test;
	lnr.who = &lnr;

	mTEST_STEP("Window update");
	mXD_RESULTA(os.fSyncX());

	mTEST_STEP("Window hide");
	mXD_RESULTA(nXD::OS::iWdgtShowX(false, wdgt));

	mTEST_STEP("Window destroy");
	mXD_RESULTA(nXD::OS::iWdgtDestroyX(wdgt));

	mTEST_END("OS");

	return O_O;
}
