#if defined(XD_TEST)

extern int base_test();
extern int os_test();
extern int render_test();

int main()
{
	if(base_test() || os_test() || render_test())
		return 0x1;

	return 0x0;
}

#endif
