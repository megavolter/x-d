#include "BASE/CODE.h"
#include "MODULES/OS/XD_OS.h"
#include "MODULES/GFX/XD_GFX.h"
#include "MODULES/TIME/XD_TIME.h"

#include "TEST_COMMON.h"

x_result render_test()
{
	mTEST_BEGIN("Render");
	nXD::OS	os;
	nXD::OS::WDGT_T wdgt = nXD::OS::iWdgtCreate();

	mTEST_STEP("Widget init");
	nXD::OS::iWdgtInitX("Render", 100, 100, 500, 500, wdgt);

	nXD::GFX gfx;
	sip dc = nXD::OS::iWdgtDescriptorGet(wdgt);
	mTEST_STEP("GFX init");
	mXD_RESULTA(gfx.fInitX(dc));

	mTEST_STEP("Window show");
	nXD::OS::iWdgtShowX(true, wdgt);

	si4 start_time = nXD::TIME::iGetCounter();
	fl4 time = 0;
	mTEST_STEP("Render loop");
	while(time < 5.f)
	{
		time = nXD::TIME::iCurrentSystemTime(start_time);
		gfx.fSceneOpenX();
		gfx.fClearColorSet(XD_COLOR(XD_COLOR::eNiceSky));
		gfx.fSceneClearX(true, false, false);
		gfx.fSceneCloseX();
		os.fSyncX();
	}
	mTEST_END("Render");

	return O_O;
}





