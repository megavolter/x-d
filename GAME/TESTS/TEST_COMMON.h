/*
 * TEST_COMMON.h
 *
 *  Created on: 12 нояб. 2016 г.
 *      Author: Dot
 */

#ifndef TESTS_TEST_COMMON_H_
#define TESTS_TEST_COMMON_H_

#include "BASE/LOG.h"

#define mTEST_BEGIN(text) \
	mNOTE(">>> Test {" << text << "} begin")

#define mTEST_STEP(text) \
	mNOTE("> Testing " << text)

#define mTEST_END(text) \
	mNOTE(">>> Test {" << text << "} end")

#endif /* TESTS_TEST_COMMON_H_ */
