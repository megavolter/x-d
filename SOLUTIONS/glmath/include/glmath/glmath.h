/*
 * $Header: /cvsroot/glmath/glmath/include/glmath/glmath.h,v 1.11 2003/03/25 15:49:36 msporny Exp $
 *
 * Copyright (C) 2000, Manu Sporny <msporny@vt.edu>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#ifndef GLMATH_H
#define GLMATH_H

/**
 * This include file is provided for convenience to include all include files
 * needed by the GLmath library.
 */
#include <glmath/GlmPoint2.h>
#include <glmath/GlmPoint3.h>
#include <glmath/GlmLine2.h>
#include <glmath/GlmVector3.h>
#include <glmath/GlmVertex3.h>
#include <glmath/GlmPlane.h>
#include <glmath/GlmFrustrum.h>
#include <glmath/GlmHermiteCurve2.h>
#include <glmath/GlmBezierCurve2.h>
#include <glmath/GlmHermiteSpline2.h>
#include <glmath/GlmBezierSpline2.h>
#include <glmath/GlmBezierSpline3.h>

#endif /* GLMATH_H */

