/*
 * $Id: GlmConfig.h,v 1.5 2003/03/25 15:49:36 msporny Exp $
 *
 * Copyright (C) 2000, Manu Sporny <msporny@vt.edu>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#ifndef GLMATHCONFIG_H
#define GLMATHCONFIG_H

using namespace std;

#include <ostream>
#include <istream>
//#include <ostream.h>
//#include <istream.h>
#include <iostream>

#define GLM_X 0
#define GLM_Y 1
#define GLM_Z 2

#endif /* GLMATHCONFIG_H */
