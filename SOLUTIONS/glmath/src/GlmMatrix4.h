/*
 * $Id: GlmMatrix4.h,v 1.5 2003/03/25 15:49:36 msporny Exp $
 *
 * Copyright (C) 2000, Manu Sporny <msporny@vt.edu>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#ifndef GLMMATRIX4_H
#define GLMMATRIX4_H

#include <glmath/GlmConfig.h>

/**
 * This class provides an interface for a 4 dimensional matrix
 * representation. OpenGL uses this matrix representation quite often
 * to represent rotations, scales and other such object
 * transformations.
 */
class GlmMatrix4
{
public:
   /**
    * Default constructor. The matrix is initialized to the Identity
    * matrix.
    */
   GlmMatrix4();

   /**
    * Constructor that takes a 4x4 or 1x16 array in row major order
    * and initializes the matrix data with it.
    *
    * @param mat the matrix to use to initialize this GlmMatrix4
    */
   GlmMatrix4(float mat[16]);
   
   /**
    * Constructor that takes 16 arguments to initialize the
    * matrix. Each cell in the matrix is ordered left to right, top to 
    * bottom starting at 0 and ending at 3. Therefore, row 3 column 1
    * would be cell20.
    *
    * @param cell00 - Matrix cell value for Row 1, Column 1
    * @param cell01 - Matrix cell value for Row 1, Column 2
    * @param cell02 - Matrix cell value for Row 1, Column 3
    * @param cell03 - Matrix cell value for Row 1, Column 4
    * @param cell10 - Matrix cell value for Row 2, Column 1
    * @param cell11 - Matrix cell value for Row 2, Column 2
    * @param cell12 - Matrix cell value for Row 2, Column 3
    * @param cell13 - Matrix cell value for Row 2, Column 4
    * @param cell20 - Matrix cell value for Row 3, Column 1
    * @param cell21 - Matrix cell value for Row 3, Column 2
    * @param cell22 - Matrix cell value for Row 3, Column 3
    * @param cell23 - Matrix cell value for Row 3, Column 4
    * @param cell30 - Matrix cell value for Row 4, Column 1
    * @param cell31 - Matrix cell value for Row 4, Column 2
    * @param cell32 - Matrix cell value for Row 4, Column 3
    * @param cell33 - Matrix cell value for Row 4, Column 4
    */
   GlmMatrix4(float cell00, float cell01, float cell02, float cell03,
              float cell10, float cell11, float cell12, float cell13,
              float cell20, float cell21, float cell22, float cell23,
              float cell30, float cell31, float cell32, float cell33);

   /**
    * Sets this matrix to the identity matrix.
    */
   void setIdentity();
   
   /**
    * Checks the current matrix for equality against the given matrix.
    *
    * @param mat the matrix to compare this matrix against
    * @return 1 if the matrixs are equal, or 0 if the matrices are not
    *         equal
    */
   int operator ==(const GlmMatrix4& mat);
   
   /**
    * Checks the current matrix for inequality against the given matrix.
    *
    * @param mat the matrix to compare this matrix against
    * @return 1 if the matrices are not equal, or 0 if the matrices are not
    *         equal
    */
   int operator !=(const GlmMatrix4& mat);

   /**
    * Add and assign operator.
    *
    * @param mat the matrix to add to this matrix
    * @return a reference to the modified GlmMatrix4
    */
   GlmMatrix4& operator +=(const GlmMatrix4& mat);

   /**
    * Subtract and assign operator.
    *
    * @param mat the matrix to subtract from this matrix
    * @return a reference to the modified GlmMatrix4
    */
   GlmMatrix4& operator -=(const GlmMatrix4& mat);
   
   /**
    * Divide by a matrix and assign operator.
    *
    * @param mat the matrix to divide this matrix by.
    * @return a reference to the modified GlmMatrix4
    */
   GlmMatrix4& operator /=(const GlmMatrix4& mat);

   /**
    * Multiply by a matrix and assign operator.
    *
    * @param mat the matrix to multiply this matrix by.
    * @return a reference to the modified GlmMatrix4
    */
   GlmMatrix4& operator *=(const GlmMatrix4& mat);

   /**
    * Operator that casts this object to a float array. This may be
    * used with OpenGL to specify a 4x4 OpenGL friendly matrix.
    * @return an array of 16 floats in column major order (which is
    *         what OpenGL prefers. This is a borrowed pointer, so
    *         DO NOT DELETE!
    */
   operator float*();

   /**
    * Stream operator to send this data to a stream. The matrix is
    * printed in row major order (opposite of what it is stored as).
    * The only reason that it is printed in row major order is because
    * people are used to that.
    *
    * @param outStream the output stream specified by whatever is to
    *                  the left of this operator.
    * @param mat the matrix object to send to the output stream
    * @return a reference to the modified output stream
    */
   friend ostream& operator<< (ostream& outStream, const GlmMatrix4& mat);

 private:
   static float mIdentity[16];
   float* mMatrix;
   float mScratchMatrix1[16];
   float mScratchMatrix2[16];
};

#endif /* GLMMATRIX4_H */
