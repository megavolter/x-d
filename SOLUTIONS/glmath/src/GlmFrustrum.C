/*
 * $Id: GlmFrustrum.C,v 1.2 2003/03/25 15:49:36 msporny Exp $
 *
 * Copyright (C) 2000, Manu Sporny  <msporny@vt.edu>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#include <glmath/GlmFrustrum.h>

/**
 * Default constructor
 */
GlmFrustrum::GlmFrustrum()
{
   mHorizontalFov = 45.0;
   mVerticalFov = 120.0;
}

/**
 * Virtual destructor
 */
GlmFrustrum::~GlmFrustrum()
{
}

/**
 * Gets the front clipping plane for this frustrum.
 *
 * @return the front clipping plane for this frustrum.
 */
GlmPlane& GlmFrustrum::getFrontClippingPlane()
{
   return mFrontCp;
}

/**
 * Gets the rear clipping plane for this frustrum.
 *
 * @return the rear clipping plane for this frustrum.
 */
GlmPlane& GlmFrustrum::getBackClippingPlane()
{
   return mBackCp;
}

/**
 * Gets the left clipping plane for this frustrum. The
 * clipping plane is selected by looking down the viewing vector
 * from the point of origin oriented in the same way that the
 * frustrum is oriented.
 *
 * @return the left clipping plane for this frustrum.
 */
GlmPlane& GlmFrustrum::getLeftClippingPlane()
{
   return mLeftCp;
}

/**
 * Gets the right clipping plane for this frustrum. The
 * clipping plane is selected by looking down the viewing vector
 * from the point of origin oriented in the same way that the
 * frustrum is oriented.
 *
 * @return the right clipping plane for this frustrum.
 */
GlmPlane& GlmFrustrum::getRightClippingPlane()
{
   return mRightCp;
}

/**
 * Gets the top clipping plane for this frustrum. The
 * clipping plane is selected by looking down the viewing vector
 * from the point of origin oriented in the same way that the
 * frustrum is oriented.
 *
 * @return the top clipping plane for this frustrum.
 */
GlmPlane& GlmFrustrum::getTopClippingPlane()
{
   return mTopCp;
}

/**
 * Gets the bottom clipping plane for this frustrum. The
 * clipping plane is selected by looking down the viewing vector
 * from the point of origin oriented in the same way that the
 * frustrum is oriented.
 *
 * @return the bottom clipping plane for this frustrum.
 */
GlmPlane& GlmFrustrum::getBottomClippingPlane()
{
   return mBottomCp;
}

/**
 * Gets the total horizontal field of view for this frustrum.
 *
 * @return the total horizontal field of view for this frustrum.
 */
float GlmFrustrum::getHorizontalFov()
{
   return mHorizontalFov;
}

/**
 * Gets the total vertical field of view for this frustrum.
 *
 * @return the total vertical field of view for this frustrum.
 */
float GlmFrustrum::getVerticalFov()
{
   return mVerticalFov;
}

/**
 * Gets the view vertex for this frustrum. The view vertex encodes
 * information for the frustrum such as position and orientation.
 *
 * @return the view vertex for this frustrum.
 */
GlmVertex3& GlmFrustrum::getViewVertex()
{
   return mViewVertex;
}

/**
 * Checks to see whether a point lies inside this frustrum.
 *
 * @param point the point to check for containment
 *
 * @return true if the point is inside this frustrum, false if the
 *         point lies outside the frustrum.
 */
bool GlmFrustrum::contains(GlmPoint3& point)
{
   return false;
}

/**
 * Checks to see whether a vertex lies inside this frustrum.
 *
 * @param vertex the vertex to check for containment
 *
 * @return true if the vertex is inside this frustrum, false if the
 *         vertex lies outside the frustrum.
 */
bool GlmFrustrum::contains(GlmVertex3& vertex)
{
   return false;
}

/**
 * Sets the front clipping plane for this frustrum.
 *
 * @param plane the plane to set as the front clipping plane for
 *              this frustrum
 */
void GlmFrustrum::setFrontClippingPlane(GlmPlane& plane)
{
   mFrontCp = plane;
}

/**
 * Sets the rear clipping plane for this frustrum.
 *
 * @param plane the plane to set as the rear clipping plane for
 *              this frustrum
 */
void GlmFrustrum::setBackClippingPlane(GlmPlane& plane)
{
   mBackCp = plane;
}

/**
 * Sets the left clipping plane for this frustrum. The
 * clipping plane is selected by looking down the viewing vector
 * from the point of origin oriented in the same way that the
 * frustrum is oriented.
 *
 * @param plane the plane to set as the left clipping plane for
 *              this frustrum
 */
void GlmFrustrum::setLeftClippingPlane(GlmPlane& plane)
{
   mLeftCp = plane;
}

/**
 * Sets the right clipping plane for this frustrum. The
 * clipping plane is selected by looking down the viewing vector
 * from the point of origin oriented in the same way that the
 * frustrum is oriented.
 *
 * @param plane the plane to set as the right clipping plane for
 *              this frustrum
 */
void GlmFrustrum::setRightClippingPlane(GlmPlane& plane)
{
   mRightCp = plane;
}

/**
 * Sets the top clipping plane for this frustrum. The
 * clipping plane is selected by looking down the viewing vector
 * from the point of origin oriented in the same way that the
 * frustrum is oriented.
 *
 * @param plane the plane to set as the right clipping plane for
 *              this frustrum
 */
void GlmFrustrum::setTopClippingPlane(GlmPlane& plane)
{
   mTopCp = plane;
}

/**
 * Sets the bottom clipping plane for this frustrum. The
 * clipping plane is selected by looking down the viewing vector
 * from the point of origin oriented in the same way that the
 * frustrum is oriented.
 *
 * @param plane the plane to set as the right clipping plane for
 *              this frustrum
 */
void GlmFrustrum::setBottomClippingPlane(GlmPlane& plane)
{
   mBottomCp = plane;
}

/**
 * Sets the total horizontal field of view for this frustrum.
 *
 * @param degrees a positive value for the total degree value
 *                greater than 0 and less than 180.
 * @return true if the degree value is between 0 and 180, false otherwise.
 */
bool GlmFrustrum::setHorizontalFov(float degrees)
{
   bool rval = false;

   if((degrees > -0.0) && (degrees < 180.0))
   {
      mHorizontalFov = degrees;
      rval = true;
   }

   return rval;
}

/**
 * Sets the total vertical field of view for this frustrum.
 *
 * @param degrees a positive value for the total degree value
 *                greater than 0 and less than 180.
 * @return true if the degree value is between 0 and 180, false otherwise.
 */
bool GlmFrustrum::setVerticalFov(float degrees)
{
   bool rval = false;

   if((degrees > -0.0) && (degrees < 180.0))
   {
      mVerticalFov = degrees;
      rval = true;
   }

   return rval;
}

/**
 * Sets the view vertex for this frustrum. The view vertex encodes
 * information for the frustrum such as position and orientation.
 *
 * @param vertex the to set for this frustrum. Both the vertex position
 *               and orientation information is used.
 */
void GlmFrustrum::setViewVertex(GlmVertex3& vertex)
{
   mViewVertex = vertex;
}
