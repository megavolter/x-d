/*
 * $Id: GlmBezierSpline2.h,v 1.2 2003/03/25 15:49:36 msporny Exp $
 *
 * Copyright (C) 2003, xRhino, Inc. <msporny@xrhino.com>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#ifndef GLMBEZIERSPLINE2_H
#define GLMBEZIERSPLINE2_H

#include <vector>
#include <glmath/GlmConfig.h>
#include <glmath/GlmPoint2.h>
#include <glmath/GlmPath2.h>

class GlmBezierSpline2 : public GlmPath2
{
 public:
   
   /**
    * Default constructor
    */
   GlmBezierSpline2();
   
   /**
    * Parameter-based constructor for Bezier splines. This constructor
    * sets the list of coefficients for the first Bezier geometry matrix.
    * For example, a Bezier geometry matrix (Gh) looks like so:
    * <p>
    * <pre>
    *       | P1x P1y |
    *  Gh = | P2x P2y |
    *       | P3x P3y |
    *       | P4x P4y |
    * </pre>
    * <p>
    *
    * @param p1x The P1x component of the Bezier geometry matrix
    * @param p1y The P1y component of the Bezier geometry matrix
    * @param p2x The P2x component of the Bezier geometry matrix
    * @param p2y The P2y component of the Bezier geometry matrix
    * @param p3x The P3x component of the Bezier geometry matrix
    * @param p3y The P3y component of the Bezier geometry matrix
    * @param p4x The P4x component of the Bezier geometry matrix
    * @param p4y The P4y component of the Bezier geometry matrix
    */
   GlmBezierSpline2(float p1x, float p1y, float p2x, float p2y,
                    float p3x, float p3y, float p4x, float p4y);
   
   /**
    * Virtual destructor
    */
   virtual ~GlmBezierSpline2();

   /**
    * Sets the list of coefficients for the first Bezier geometry matrix.
    * For example, a Bezier geometry matrix (Gh) looks like so:
    * <p>
    * <pre>
    *       | P1x P1y |
    *  Gh = | P2x P2y |
    *       | P3x P3y |
    *       | P4x P4y |
    * </pre>
    * <p>
    *
    * @param p1x The P1x component of the Bezier geometry matrix
    * @param p1y The P1y component of the Bezier geometry matrix
    * @param p2x The P2x component of the Bezier geometry matrix
    * @param p2y The P2y component of the Bezier geometry matrix
    * @param p3x The P3x component of the Bezier geometry matrix
    * @param p3y The P3y component of the Bezier geometry matrix
    * @param p4x The P4x component of the Bezier geometry matrix
    * @param p4y The P4y component of the Bezier geometry matrix
    */
   virtual void initSpline(float p1x, float p1y, float p2x, float p2y,
                           float p3x, float p3y, float p4x, float p4y);
   
   /**
    * Adds a new hermite spline segment to the end of the current
    * spline segment. P1x and P1y are set from the last segments
    * P4x and P4y values. Thus, the basis matrix looks like this:
    * <p>
    * <pre>
    *       | lastP4x lastP4y |
    *  Gh = | P2x     P2y     |
    *       | P3x     P3y     |
    *       | P4x     P4y     |
    * </pre>
    * <p>
    *
    * @param startingPercentage the percentage of the line this
    *                           spline segment will be responsible
    *                           for. For example, a value of 0.6 will
    *                           mean that the specified current spline segment
    *                           will be responsible for 60%-100% of
    *                           the whole spline.
    * @param p2x The P2x component of the Bezier geometry matrix
    * @param p2y The P2y component of the Bezier geometry matrix
    * @param p3x The P3x component of the Bezier geometry matrix
    * @param p3y The P3y component of the Bezier geometry matrix
    * @param p4x The P4x component of the Bezier geometry matrix
    * @param p4y The P4y component of the Bezier geometry matrix
    */
   virtual void addSplineSegment(float startingPercentage,
                                 float p2x, float p2y,
                                 float p3x, float p3y, float p4x, float p4y);

   /**
    * Gets the coefficients for the Bezier geometry matrix specified
    * by the hermiteIndex.
    * For example, the Bezier geometry matrix (Gh) might look like so:
    * <p>
    * <pre>
    *       | P1x P1y |
    *  Gh = | P2x P2y |
    *       | P3x P3y |
    *       | P4x P4y |
    * </pre>
    * <p>
    * @param hermiteIndex The spline segment to get the coefficients for
    * @param p1x The P1x component of the Bezier geometry matrix
    * @param p1y The P1y component of the Bezier geometry matrix
    * @param p2x The P2x component of the Bezier geometry matrix
    * @param p2y The P2y component of the Bezier geometry matrix
    * @param p3x The P3x component of the Bezier geometry matrix
    * @param p3y The P3y component of the Bezier geometry matrix
    * @param p4x The P4x component of the Bezier geometry matrix
    * @param p4y The P4y component of the Bezier geometry matrix
    */
   virtual void getSplineCoefficients(unsigned int hermiteIndex,
                                      float& p1x, float& p1y,
                                      float& p2x, float& p2y,
                                      float& p3x, float& p3y,
                                      float& p4x, float& p4y);
   /**
    * Gets the number of Bezier segments in this spline.
    *
    * @param The number of hermite segments in the spline.
    */
   virtual int getSplineSegments();
   
   /**
    * Sets the coefficients for the Bezier geometry matrix specified
    * by the hermiteIndex.
    * For example, the Bezier geometry matrix (Gh) might look like so:
    * <p>
    * <pre>
    *       | P1x P1y |
    *  Gh = | P2x P2y |
    *       | P3x P3y |
    *       | P4x P4y |
    * </pre>
    * <p>
    * @param hermiteIndex The spline segment to set the coefficients
    *                     for
    * @param percentage The starting percentage of the entire spline
    *                   length that this spline segment should be
    *                   responsible for
    * @param p1x The P1x component of the Bezier geometry matrix
    * @param p1y The P1y component of the Bezier geometry matrix
    * @param p2x The P2x component of the Bezier geometry matrix
    * @param p2y The P2y component of the Bezier geometry matrix
    * @param p3x The P3x component of the Bezier geometry matrix
    * @param p3y The P3y component of the Bezier geometry matrix
    * @param p4x The P4x component of the Bezier geometry matrix
    * @param p4y The P4y component of the Bezier geometry matrix
    */
   virtual void setSplineSegment(unsigned int hermiteIndex,
                                 float percentage,
                                 float p1x, float p1y,
                                 float p2x, float p2y,
                                 float p3x, float p3y,
                                 float p4x, float p4y);
   
   /**
    * Gets a calculated position as a function of a percentage of the total
    * distance for a path. If a line is 4 units long, given by the two
    * end-points (0, 0) and (4, 0), and the percentage given is 0.5
    * (50%), the position along the line would be (2, 0).
    *
    * @param percent the percentage of the path travelled
    * @return a point that is the calculated position along the path
    *         using the given percentage.
    */
   virtual GlmPoint2 getPositionByPercentage(float percent);
   
   /**
    * Stream operator to send this data to a stream. The path is
    * converted to a string that looks something like this: XXXXXX
    *
    * @param outStream the output stream specified by whatever is to
    *                  the left of this operator.
    * @param hs the hermite spline to send to the output stream
    * @return a reference to the modified output stream
    */
   friend ostream& operator<< (ostream& outStream,
                               const GlmBezierSpline2& bs);
   
 private:
   /**
    * The number of spline segments in this spline
    */
   unsigned int mSegments;

   /**
    * The active spline percentages for each one of the curve segments
    */
   float* mSplinePercentage;
   
   /**
    * The blending functions to use for this cubic polynomial curve
    */
   float* mXYCoefficients[2];
};

#endif /* GLMBEZIERSPLINE2_H */
