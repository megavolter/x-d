/*
 * $Header: /cvsroot/glmath/glmath/src/GlmPlane.h,v 1.4 2003/03/25 15:49:36 msporny Exp $
 *
 * Copyright (C) 2000, Manu Sporny <msporny@vt.edu>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#ifndef GLMPLANE_H
#define GLMPLANE_H

#include <glmath/GlmVertex3.h>

class GlmPlane
{
 public:

   /**
    * Default constructor
    */
   GlmPlane();
   
   /**
    * Constructs a plane object from the given position and
    * orientation data
    *
    * @param positionX The X position of the plane
    * @param positionY The Y position of the plane
    * @param positionZ The Z position of the plane
    * @param orientationX The X orientation of the plane
    * @param orientationY The Y orientation of the plane
    * @param orientationZ The Z orientation of the plane
    */
   GlmPlane(float positionX, float positionY, float positionZ,
            float orientationX, float orientationY, float orientationZ);

   /**
    * Virtual destructor
    */
   virtual ~GlmPlane();

   /**
    * Calculates the distance from a given point to the plane. It is
    * assumed that the point and the plane are in the same coordinate
    * system.
    *
    * @param point the point whose distance from the plane to
    *              calculate
    * @return a floating point value of the distance from the point to 
    *         the plane
    */
   float distanceTo(GlmPoint3 point);
   
 private:
   GlmVertex3 mVertex;
};

#endif /* GLMPLANE_H */

