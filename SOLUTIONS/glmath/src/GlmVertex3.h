/*
 * $Id: GlmVertex3.h,v 1.5 2003/03/25 15:49:37 msporny Exp $
 *
 * Copyright (C) 2000, Manu Sporny <msporny@vt.edu>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#ifndef GLMVERTEX_H
#define GLMVERTEX_H

#include <glmath/GlmConfig.h>
#include <glmath/GlmPoint3.h>
#include <glmath/GlmVector3.h>
#include <glmath/GlmMatrix4.h>

/**
 *:Defines a 3 dimensional spatial coordinate with position and
 * orientation. The position is given in X, Y, and Z coordinates
 * while the orientation is given in X, Y, and Z Euler angles.
 */
class GlmVertex3 : public GlmPoint3
{
public:
   /**
    * Default constructor
    */
   GlmVertex3();   

   /**
    * Constructs a vertex object from the given position and
    * orientation data
    *
    * @param positionX The X position of the vertex
    * @param positionY The Y position of the vertex
    * @param positionZ The Z position of the vertex
    * @param orientationX The X orientation of the vertex
    * @param orientationY The Y orientation of the vertex
    * @param orientationZ The Z orientation of the vertex
    */
   GlmVertex3(float positionX, float positionY, float positionZ,
              float orientationX, float orientationY, float orientationZ);

   /**
    * Constructs a vertex object from a given matrix object.
    *
    * @param matrix The matrix to use to construct the vertex
    */
   GlmVertex3(GlmMatrix4& matrix);

   /**
    * Gets the orientation of this vertex.
    *
    * @return the orientation of this vertex.
    */
   virtual GlmVector3& getOrientation();

   /**
    * Sets the orientation of this vertex.
    *
    * @param mOrientation the orientation to set this vertices
    *                     orientation to
    */
   virtual void setOrientation(GlmVector3& orientation);
   
 private:
   GlmVector3 mOrientation;
};

#endif /* GLMVERTEX_H */
