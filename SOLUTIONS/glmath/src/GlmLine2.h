/*
 * $Id: GlmLine2.h,v 1.3 2003/03/25 15:49:36 msporny Exp $
 *
 * Copyright (C) 2002, xRhino, Inc. <msporny@xrhino.com>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#ifndef GLMLINE2_H
#define GLMLINE2_H

#include <glmath/GlmConfig.h>
#include <glmath/GlmPoint2.h>
#include <glmath/GlmVector3.h>

class GlmLine2
{
 public:
   /**
    * Default constructor
    */
   GlmLine2();

   /**
    * Constructor that creates a line given all point information for
    * a given line segment.
    *
    * @param pAx the X position of the first point
    * @param pAy the Y position of the first point
    * @param pBx the X position of the second point
    * @param pBy the Y position of the second point
    */
   GlmLine2(float pAx, float pAy, float pBx, float pBy);

   /**
    * Constructor that creates a line given two GlmPoint2 objects.
    *
    * @param pointA the X,Y position of the first point
    * @param pointB the X,Y position of the second point
    */
   GlmLine2(GlmPoint2& pointA, GlmPoint2& pointB);

   /**
    * Virtual destructor
    */
   virtual ~GlmLine2();

   /**
    * Gets the first point of the line segment.
    *
    * @return the first point of the line segment, this is a borrowed
    *         reference, be careful with it!
    */
   virtual GlmPoint2& getPointA();

   /**
    * Gets the second point of the line segment.
    *
    * @return the second point of the line segment, this is a borrowed
    *         reference, be careful with it!
    */
   virtual GlmPoint2& getPointB();

   /**
    * Sets the first point of the line segment.
    *
    * @param point the point data to set point 1 to
    */
   virtual void setPointA(GlmPoint2& p);

   /**
    * Sets the second point of the line segment.
    *
    * @param point the point data to set point 2 to
    */
   virtual void setPointB(GlmPoint2& p);

   /**
    * Gets the length of the line segment in units.
    *
    * @return the length of the line segment
    */
   virtual float length();

   /**
    * Gets the perpendiculardistance from the line segment to a given
    * point.
    *
    * @param the point to use for the distance calculation
    * @return the distance to the point from the line segment
    */
   virtual float distance(GlmPoint2& p);
   
   /**
    * Checks the current line for equality against the given line
    *
    * @param l the line to compare this line against
    * @return 1 if the lines are equal, of 0 if the lines are not
    *         equal
    */
   int operator ==(const GlmLine2& l);
   
   /**
    * Checks the current line for inequality against the given line.
    *
    * @param p the line to compare this line against
    * @return 1 if the lines are not equal, or 0 if the lines are
    *         equal
    */
   int operator !=(const GlmLine2& l);
   
   /**
    * Stream operator to send this data to a stream. The line is
    * converted to a string that looks something like this:
    * "[(x1, y1), (x2, y2)]". Where (x1, y1) are the first points
    * coordinates and (x2, y2) are the second points coordinates
    *
    * @param outStream the output stream specified by whatever is to
    *                  the left of this operator.
    * @param line the line object to send to the output stream
    * @return a reference to the modified output stream
    */
   friend ostream& operator<< (ostream& outStream, const GlmLine2& line);
   
private:
   GlmPoint2 mPointA;
   GlmPoint2 mPointB;
   // Cached length of this line
   float mLength;
   // Whether or not the length cache is valid
   bool mLengthValid;
};

#endif /* GLMLINE2_H */
