/*
 * $Id: GlmHermiteCurve2.h,v 1.2 2003/03/25 15:49:36 msporny Exp $
 *
 * Copyright (C) 2003, xRhino, Inc. <msporny@xrhino.com>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#ifndef GLMHERMITECURVE2_H
#define GLMHERMITECURVE2_H

#include <vector>
#include <glmath/GlmConfig.h>
#include <glmath/GlmPoint2.h>
#include <glmath/GlmPath2.h>

class GlmHermiteCurve2 : public GlmPath2
{
 public:
   /**
    * The blending coefficients to use when blending the 2d curve
    * segment.
    */
   typedef struct HermiteCoefficient2Type
   {
      float x[4];
      float y[4];
   };

   /**
    * Default constructor
    */
   GlmHermiteCurve2();

   /**
    * Parameter-based constructor for Hermite curves. This constructor
    * sets the list of coefficients for the Hermite geometry matrix.
    * For example, a Hermite geometry matrix (Gh) looks like so:
    * <p>
    * <pre>
    *       | P1x P1y |
    *  Gh = | P4x P4y |
    *       | R1x R1y |
    *       | R4x R4y |
    * </pre>
    * <p>
    *
    * @param p1x The P1x component of the Hermite geometry matrix
    * @param p1y The P1y component of the Hermite geometry matrix
    * @param r1x The R1x component of the Hermite geometry matrix
    * @param r1y The R1y component of the Hermite geometry matrix
    * @param p4x The P4x component of the Hermite geometry matrix
    * @param p4y The P4y component of the Hermite geometry matrix
    * @param r4x The R4x component of the Hermite geometry matrix
    * @param r4y The R4y component of the Hermite geometry matrix
    */
   GlmHermiteCurve2(float p1x, float p1y, float r1x, float r1y,
                    float p4x, float p4y, float r4x, float r4y);
   /**
    * Virtual destructor
    */
   virtual ~GlmHermiteCurve2();

   /**
    * Sets the list of coefficients for the Hermite geometry matrix.
    * For example, a Hermite geometry matrix (Gh) looks like so:
    * <p>
    * <pre>
    *       | P1x P1y |
    *  Gh = | P4x P4y |
    *       | R1x R1y |
    *       | R4x R4y |
    * </pre>
    * <p>
    *
    * @param p1x The P1x component of the Hermite geometry matrix
    * @param p1y The P1y component of the Hermite geometry matrix
    * @param r1x The R1x component of the Hermite geometry matrix
    * @param r1y The R1y component of the Hermite geometry matrix
    * @param p4x The P4x component of the Hermite geometry matrix
    * @param p4y The P4y component of the Hermite geometry matrix
    * @param r4x The R4x component of the Hermite geometry matrix
    * @param r4y The R4y component of the Hermite geometry matrix
    */
   virtual void setCoefficients(float p1x, float p1y, float r1x, float r1y,
                                float p4x, float p4y, float r4x, float r4y);

   /**
    * Gets the list of coefficients for the Hermite geometry matrix.
    * For example, a Hermite geometry matrix (Gh) looks like so:
    * <p>
    * <pre>
    *       | P1x P1y |
    *  Gh = | P4x P4y |
    *       | R1x R1y |
    *       | R4x R4y |
    * </pre>
    * <p>
    * @param p1x The P1x component of the Hermite geometry matrix
    * @param p1y The P1y component of the Hermite geometry matrix
    * @param r1x The R1x component of the Hermite geometry matrix
    * @param r1y The R1y component of the Hermite geometry matrix
    * @param p4x The P4x component of the Hermite geometry matrix
    * @param p4y The P4y component of the Hermite geometry matrix
    * @param r4x The R4x component of the Hermite geometry matrix
    * @param r4y The R4y component of the Hermite geometry matrix
    */
   virtual void getCoefficients(float& p1x, float& p1y,
                                float& r1x, float& r1y,
                                float& p4x, float& p4y,
                                float& r4x, float& r4y);
   
   /**
    * Gets a calculated position as a function of a percentage of the total
    * distance for a path. If a line is 4 units long, given by the two
    * end-points (0, 0) and (4, 0), and the percentage given is 0.5
    * (50%), the position along the line would be (2, 0).
    *
    * @param percent the percentage of the path travelled
    * @return a point that is the calculated position along the path
    *         using the given percentage.
    */
   virtual GlmPoint2 getPositionByPercentage(float percent);
   
   /**
    * Stream operator to send this data to a stream. The path is
    * converted to a string that looks something like this: XXXXXX
    *
    * @param outStream the output stream specified by whatever is to
    *                  the left of this operator.
    * @param path the path object to send to the output stream
    * @return a reference to the modified output stream
    */
   friend ostream& operator<< (ostream& outStream,
                               const GlmHermiteCurve2& hc);
   
 private:
   /**
    * The blending functions to use for this cubic polynomial curve
    */
   HermiteCoefficient2Type mCoefficients;
};

#endif /* GLMHERMITECURVE2_H */
