/*
 * $Id: GlmColor4.h,v 1.4 2003/03/25 15:49:36 msporny Exp $
 *
 * Copyright (C) 2000, Manu Sporny <msporny@vt.edu>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#ifndef GLMCOLOR4_H
#define GLMCOLOR4_H

using namespace std;

#include <iostream>

#define GLM_R 0
#define GLM_G 1
#define GLM_B 2
#define GLM_A 3

class GlmColor4
{
 public:
   /**
    * Default constructor
    */
   GlmColor4();
   
   /**
    * Constructs a color object from the given red, green, blue, and
    * alpha values
    *
    * @param red the red component to use for this color
    * @param green the green component to use for this color
    * @param blue the blue component to use for this color
    * @param alpha the alpha component to use for this color
    */
   GlmColor4(float red, float green, float blue, float alpha);

   /**
    * Virtual destructor
    */
   virtual ~GlmColor4();

   /**
    * Sets the RGBA values of this color to the given values.
    *
    * @param red the red component to use for this color
    * @param green the green component to use for this color
    * @param blue the blue component to use for this color
    * @param alpha the alpha component to use for this color
    */
   void setRGBA(float red, float green, float blue, float alpha);
   
   /**
    * Gets the RGBA values of this color.
    *
    * @param red the red component of this color
    * @param green the green component of this color
    * @param blue the blue component of this color
    * @param alpha the alpha component of this color
    */
   void getRGBA(float& red, float& green, float& blue, float& alpha);

   /**
    * Checks the current color for equality against the given color.
    *
    * @param color the color to compare this color against
    * @return 1 if the colors are equal, of 0 if the colors are not
    *         equal
    */
   int operator ==(const GlmColor4& color);
   
   /**
    * Checks the current color for inequality against the given color.
    *
    * @param color the color to compare this color against
    * @return 1 if the colors are not equal, or 0 if the colors are
    *         equal
    */
   int operator !=(const GlmColor4& color);

   /**
    * Operator that casts this object to a float array. This may be
    * used with OpenGL to specify RGBA colors by calling glColor4fv
    * and passing this object as the first argument to the function.
    *
    * @return a 4 element array of floats with the following layout:
    *         [r, g, b, a]. This is a borrowed reference, do not delete!
    */
   operator float*();
   
   /**
    * Stream operator to send this data to a stream. The color is
    * converted to a string that looks something like this:
    * "(r, g, b, a)". r, g, b, a are the red, gree, blue, and alpha
    * values, respectively.
    *
    * @param outStream the output stream specified by whatever is to
    *                  the left of this operator.
    * @param color the color object to send to the output stream
    * @return a reference to the modified output stream
    */
   friend ostream& operator<< (ostream& outStream, const GlmColor4& color);
   
 protected:
   float mRgba[4];
};

#endif /* GLMCOLOR4_H */
