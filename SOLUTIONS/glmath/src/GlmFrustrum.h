/*
 * $Id: GlmFrustrum.h,v 1.2 2003/03/25 15:49:36 msporny Exp $
 *
 * Copyright (C) 2000, Manu Sporny  <msporny@vt.edu>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#ifndef GLMFRUSTRUM_H
#define GLMFRUSTRUM_H

#include <glmath/GlmPlane.h>

/**
 * This class encapsulates a 3D viewing frustrum. A viewing frustrum
 * usually defines an area of a 3D world that a user can see including 
 * the position of the user and field of view. The GlmFrustrum class
 * can calculate if certain objects are bounded by the clipping planes
 * that it is composed (aka visiblility determination). 
 *
 * @author Manu Sporny <msporny@vt.edu>
 */
class GlmFrustrum
{
 public:
   /**
    * Default constructor
    */
   GlmFrustrum();
   
   /**
    * Virtual destructor
    */
   virtual ~GlmFrustrum();

   /**
    * Gets the front clipping plane for this frustrum.
    *
    * @return the front clipping plane for this frustrum.
    */
   virtual GlmPlane& getFrontClippingPlane();

   /**
    * Gets the rear clipping plane for this frustrum.
    *
    * @return the rear clipping plane for this frustrum.
    */
   virtual GlmPlane& getBackClippingPlane();

   /**
    * Gets the left clipping plane for this frustrum. The
    * clipping plane is selected by looking down the viewing vector
    * from the point of origin oriented in the same way that the
    * frustrum is oriented.
    *
    * @return the left clipping plane for this frustrum.
    */
   virtual GlmPlane& getLeftClippingPlane();

   /**
    * Gets the right clipping plane for this frustrum. The
    * clipping plane is selected by looking down the viewing vector
    * from the point of origin oriented in the same way that the
    * frustrum is oriented.
    *
    * @return the right clipping plane for this frustrum.
    */
   virtual GlmPlane& getRightClippingPlane();

   /**
    * Gets the top clipping plane for this frustrum. The
    * clipping plane is selected by looking down the viewing vector
    * from the point of origin oriented in the same way that the
    * frustrum is oriented.
    *
    * @return the top clipping plane for this frustrum.
    */
   virtual GlmPlane& getTopClippingPlane();

   /**
    * Gets the bottom clipping plane for this frustrum. The
    * clipping plane is selected by looking down the viewing vector
    * from the point of origin oriented in the same way that the
    * frustrum is oriented.
    *
    * @return the bottom clipping plane for this frustrum.
    */
   virtual GlmPlane& getBottomClippingPlane();

   /**
    * Gets the total horizontal field of view for this frustrum.
    *
    * @return the total horizontal field of view for this frustrum.
    */
   virtual float getHorizontalFov();

   /**
    * Gets the total vertical field of view for this frustrum.
    *
    * @return the total vertical field of view for this frustrum.
    */
   virtual float getVerticalFov();

   /**
    * Gets the view vertex for this frustrum. The view vertex encodes
    * information for the frustrum such as position and orientation.
    *
    * @return the view vertex for this frustrum.
    */
   virtual GlmVertex3& getViewVertex();

   /**
    * Checks to see whether a point lies inside this frustrum.
    *
    * @param point the point to check for containment
    *
    * @return true if the point is inside this frustrum, false if the
    *         point lies outside the frustrum.
    */
   virtual bool contains(GlmPoint3& point);

   /**
    * Checks to see whether a vertex lies inside this frustrum.
    *
    * @param vertex the vertex to check for containment
    *
    * @return true if the vertex is inside this frustrum, false if the
    *         vertex lies outside the frustrum.
    */
   virtual bool contains(GlmVertex3& vertex);

   /**
    * Sets the front clipping plane for this frustrum.
    *
    * @param plane the plane to set as the front clipping plane for
    *              this frustrum
    */
   virtual void setFrontClippingPlane(GlmPlane& plane);

   /**
    * Sets the rear clipping plane for this frustrum.
    *
    * @param plane the plane to set as the rear clipping plane for
    *              this frustrum
    */
   virtual void setBackClippingPlane(GlmPlane& plane);

   /**
    * Sets the left clipping plane for this frustrum. The
    * clipping plane is selected by looking down the viewing vector
    * from the point of origin oriented in the same way that the
    * frustrum is oriented.
    *
    * @param plane the plane to set as the left clipping plane for
    *              this frustrum
    */
   virtual void setLeftClippingPlane(GlmPlane& plane);

   /**
    * Sets the right clipping plane for this frustrum. The
    * clipping plane is selected by looking down the viewing vector
    * from the point of origin oriented in the same way that the
    * frustrum is oriented.
    *
    * @param plane the plane to set as the right clipping plane for
    *              this frustrum
    */
   virtual void setRightClippingPlane(GlmPlane& plane);

   /**
    * Sets the top clipping plane for this frustrum. The
    * clipping plane is selected by looking down the viewing vector
    * from the point of origin oriented in the same way that the
    * frustrum is oriented.
    *
    * @param plane the plane to set as the right clipping plane for
    *              this frustrum
    */
   virtual void setTopClippingPlane(GlmPlane& plane);

   /**
    * Sets the bottom clipping plane for this frustrum. The
    * clipping plane is selected by looking down the viewing vector
    * from the point of origin oriented in the same way that the
    * frustrum is oriented.
    *
    * @param plane the plane to set as the right clipping plane for
    *              this frustrum
    */
   virtual void setBottomClippingPlane(GlmPlane& plane);

   /**
    * Sets the total horizontal field of view for this frustrum.
    *
    * @param degrees a positive value for the total degree value
    *                greater than 0 and less than 180.
    * @return true if the degree value is between 0 and 180, false otherwise.
    */
   virtual bool setHorizontalFov(float degrees);

   /**
    * Sets the total vertical field of view for this frustrum.
    *
    * @param degrees a positive value for the total degree value
    *                greater than 0 and less than 180.
    * @return true if the degree value is between 0 and 180, false otherwise.
    */
   virtual bool setVerticalFov(float degrees);

   /**
    * Sets the view vertex for this frustrum. The view vertex encodes
    * information for the frustrum such as position and orientation.
    *
    * @param vertex the to set for this frustrum. Both the vertex position
    *               and orientation information is used.
    */
   virtual void setViewVertex(GlmVertex3& vertex);

 private:
   GlmPlane mFrontCp;
   GlmPlane mBackCp;
   GlmPlane mLeftCp;
   GlmPlane mRightCp;
   GlmPlane mTopCp;
   GlmPlane mBottomCp;
   float mHorizontalFov;
   float mVerticalFov;
   GlmVertex3 mViewVertex;
};

#endif /* GLMFRUSTRUM_H */
