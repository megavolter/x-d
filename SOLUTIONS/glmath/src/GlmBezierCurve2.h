/*
 * $Id: GlmBezierCurve2.h,v 1.2 2003/03/25 15:49:36 msporny Exp $
 *
 * Copyright (C) 2003, xRhino, Inc. <msporny@xrhino.com>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#ifndef GLMBEZIERCURVE2_H
#define GLMBEZIERCURVE2_H

#include <vector>
#include <glmath/GlmConfig.h>
#include <glmath/GlmPoint2.h>
#include <glmath/GlmPath2.h>

class GlmBezierCurve2 : public GlmPath2
{
 public:
   /**
    * The blending coefficients to use when blending the 2d curve
    * segment.
    */
   typedef struct BezierCoefficient2Type
   {
      float x[4];
      float y[4];
   };

   /**
    * Default constructor
    */
   GlmBezierCurve2();

   /**
    * Parameter-based constructor for Bezier curves. This constructor
    * sets the list of coefficients for the Bezier geometry matrix.
    * For example, a Bezier geometry matrix (Gh) looks like so:
    * <p>
    * <pre>
    *       | P1x P1y |
    *  Gh = | P3x P3y |
    *       | R1x P2y |
    *       | P4x P4y |
    * </pre>
    * <p>
    *
    * @param p1x The P1x component of the Bezier geometry matrix
    * @param p1y The P1y component of the Bezier geometry matrix
    * @param p2x The P2x component of the Bezier geometry matrix
    * @param p2y The P2y component of the Bezier geometry matrix
    * @param p3x The P3x component of the Bezier geometry matrix
    * @param p3y The P3y component of the Bezier geometry matrix
    * @param p4x The P4x component of the Bezier geometry matrix
    * @param p4y The P4y component of the Bezier geometry matrix
    */
   GlmBezierCurve2(float p1x, float p1y, float p2x, float p2y,
                   float p3x, float p3y, float p4x, float p4y);
   
   /**
    * Virtual destructor
    */
   virtual ~GlmBezierCurve2();

   /**
    * Sets the list of coefficients for the Bezier geometry matrix.
    * For example, a Bezier geometry matrix (Gh) looks like so:
    * <p>
    * <pre>
    *       | P1x P1y |
    *  Gh = | P3x P3y |
    *       | P2x P2y |
    *       | P4x P4y |
    * </pre>
    * <p>
    *
    * @param p1x The P1x component of the Bezier geometry matrix
    * @param p1y The P1y component of the Bezier geometry matrix
    * @param p2x The P2x component of the Bezier geometry matrix
    * @param p2y The P2y component of the Bezier geometry matrix
    * @param p3x The P3x component of the Bezier geometry matrix
    * @param p3y The P3y component of the Bezier geometry matrix
    * @param p4x The P4x component of the Bezier geometry matrix
    * @param p4y The P4y component of the Bezier geometry matrix
    */
   virtual void setCoefficients(float p1x, float p1y, float p2x, float p2y,
                                float p3x, float p3y, float p4x, float p4y);

   /**
    * Gets the list of coefficients for the Bezier geometry matrix.
    * For example, a Bezier geometry matrix (Gh) looks like so:
    * <p>
    * <pre>
    *       | P1x P1y |
    *  Gh = | P3x P3y |
    *       | P2x P2y |
    *       | P4x P4y |
    * </pre>
    * <p>
    * @param p1x The P1x component of the Bezier geometry matrix
    * @param p1y The P1y component of the Bezier geometry matrix
    * @param p2x The P2x component of the Bezier geometry matrix
    * @param p2y The P2y component of the Bezier geometry matrix
    * @param p3x The P3x component of the Bezier geometry matrix
    * @param p3y The P3y component of the Bezier geometry matrix
    * @param p4x The P4x component of the Bezier geometry matrix
    * @param p4y The P4y component of the Bezier geometry matrix
    */
   virtual void getCoefficients(float& p1x, float& p1y,
                                float& p2x, float& p2y,
                                float& p3x, float& p3y,
                                float& p4x, float& p4y);
   
   /**
    * Gets a calculated position as a function of a percentage of the total
    * distance for a path. If a line is 4 units long, given by the two
    * end-points (0, 0) and (4, 0), and the percentage given is 0.5
    * (50%), the position along the line would be (2, 0).
    *
    * @param percent the percentage of the path travelled
    * @return a point that is the calculated position along the path
    *         using the given percentage.
    */
   virtual GlmPoint2 getPositionByPercentage(float percent);
   
   /**
    * Stream operator to send this data to a stream. The path is
    * converted to a string that looks something like this: XXXXXX
    *
    * @param outStream the output stream specified by whatever is to
    *                  the left of this operator.
    * @param path the path object to send to the output stream
    * @return a reference to the modified output stream
    */
   friend ostream& operator<< (ostream& outStream,
                               const GlmBezierCurve2& hc);
   
 private:
   /**
    * The blending functions to use for this cubic polynomial curve
    */
   BezierCoefficient2Type mCoefficients;
};

#endif /* GLMBEZIERCURVE2_H */
