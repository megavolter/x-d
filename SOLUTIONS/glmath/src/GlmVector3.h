/*
 * $Header: /cvsroot/glmath/glmath/src/GlmVector3.h,v 1.9 2003/03/25 15:49:37 msporny Exp $
 *
 * Copyright (C) 2000, Manu Sporny <msporny@vt.edu>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#ifndef GLMVECTOR3_H
#define GLMVECTOR3_H

#include <glmath/GlmConfig.h>

class GlmVector3
{
 public:
   /**
    * Default constructor
    */
   GlmVector3();
   
   /**
    * Constructor that creates a vector out of the 3 given magnitudes.
    *
    * @param i the magnitude of the vector in the X direction
    * @param j the magnitude of the vector in the Y direction
    * @param k the magnitude of the vector in the Z direction
    */
   GlmVector3(float i, float j, float k);

   /**
    * Gets the 3 magnitudes of this vector.
    *
    * @param i the magnitude of the vector in the X direction
    * @param j the magnitude of the vector in the Y direction
    * @param k the magnitude of the vector in the Z direction
    */
   virtual void getIJK(float& i, float& j, float& k);
   
   /**
    * Sets the 3 magnitudes of this vector.
    *
    * @param i the magnitude of the vector in the X direction
    * @param j the magnitude of the vector in the Y direction
    * @param k the magnitude of the vector in the Z direction
    */
   virtual void setIJK(float i, float j, float k);

   /**
    * Calculates the cross product between this vector and another
    * vector.
    *
    * @param v The vector to cross with this vector.
    * @return the resultant vector
    */
   virtual GlmVector3 cross(GlmVector3& v);

   /**
    * Calculates the dot product between this vector and another
    * vector.
    *
    * @param v The vector to cross with this vector.
    * @return the resulting dot product between the two vectors
    */
   virtual float dot(GlmVector3& v);

   /**
    * Calculates the length of this vector and returns it.
    *
    * @return the length of this vector
    */
   virtual float length();
   
   /**
    * Normalizes this vector.
    */
   virtual void normalize();
   
   /**
    * Checks the current vector for equality against the given vector.
    *
    * @param v the vector to compare this vector against
    * @return 1 if the vectors are equal, of 0 if the vectors are not
    *         equal
    */
   int operator ==(const GlmVector3& v);
   
   /**
    * Checks the current vector for inequality against the given vector.
    *
    * @param v the vector to compare this vector against
    * @return 1 if the vectors are not equal, or 0 if the vectors are not
    *         equal
    */
   int operator !=(const GlmVector3& v);

   /**
    * Add and assign operator.
    *
    * @param v the vector to add to this vector
    * @return a reference to the modified GlmVector3
    */
   GlmVector3& operator +=(const GlmVector3& v);

   /**
    * Subtract and assign operator.
    *
    * @param v the vector to subtract from this vector
    * @return a reference to the modified GlmVector3
    */
   GlmVector3& operator -=(const GlmVector3& v);
   
   /**
    * Divide by a scalar and assign operator.
    *
    * @param scalar the scalar to divide this vector by.
    * @return a reference to the modified GlmVector3
    */
   GlmVector3& operator /=(const float scalar);

   /**
    * Multiply by a scalar and assign operator.
    *
    * @param scalar the scalar to multiply this vector by.
    * @return a reference to the modified GlmVector3
    */
   GlmVector3& operator *=(const float scalar);
   
   /**
    * Operator that casts this object to a float array. This may be
    * used with OpenGL to specify 3d normals by calling glNormal3f
    * and passing this object as the argument.
    *
    * @return a 3 element array of floats with the following layout:
    *         [i, j, k]. This is a borrowed reference, do not delete!
    */
   operator float*();
   
   /**
    * Stream operator to send this data to a stream. The vector is
    * converted to a string that looks something like this:
    * "[i, j, k]". Where i, j, and k are the x, y and z magnitudes,
    * respectively.
    *
    * @param outStream the output stream specified by whatever is to
    *                  the left of this operator.
    * @param vector the vector object to send to the output stream
    * @return A reference to the modified output stream
    */
   friend ostream& operator<< (ostream& outStream,
                               const GlmVector3& vector);

 private:

   /**
    * Updates the length cache. Since finding the length of a vector
    * uses a very expensive square root function, we should cache
    * a length once it is calculated incase further calls to request
    * the length of the vector are issued.
    */
   void updateLengthCache();
   
   float mLengthCache;
   bool mLengthCacheValid;
   float mVector[3];
};

#endif /* GLMVECTOR3_H */
