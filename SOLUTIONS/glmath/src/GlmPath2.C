/*
 * $Id: GlmPath2.C,v 1.2 2003/03/25 15:49:36 msporny Exp $
 *
 * Copyright (C) 2002, xRhino, Inc. <msporny@xrhino.com>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#include <glmath/GlmPath2.h>

GlmPath2::GlmPath2()
{
}

GlmPath2::~GlmPath2()
{
}
