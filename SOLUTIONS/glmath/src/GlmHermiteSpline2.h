/*
 * $Id: GlmHermiteSpline2.h,v 1.3 2003/03/25 15:49:36 msporny Exp $
 *
 * Copyright (C) 2003, xRhino, Inc. <msporny@xrhino.com>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#ifndef GLMHERMITESPLINE2_H
#define GLMHERMITESPLINE2_H

#include <vector>
#include <glmath/GlmConfig.h>
#include <glmath/GlmPoint2.h>
#include <glmath/GlmPath2.h>

class GlmHermiteSpline2 : public GlmPath2
{
 public:

   /**
    * Default constructor
    */
   GlmHermiteSpline2();

   /**
    * Parameter-based constructor for Hermite splines. This constructor
    * sets the list of coefficients for the first Hermite geometry matrix.
    * For example, a Hermite geometry matrix (Gh) looks like so:
    * <p>
    * <pre>
    *       | P1x P1y |
    *  Gh = | P4x P4y |
    *       | R1x R1y |
    *       | R4x R4y |
    * </pre>
    * <p>
    *
    * @param p1x The P1x component of the Hermite geometry matrix
    * @param p1y The P1y component of the Hermite geometry matrix
    * @param r1x The R1x component of the Hermite geometry matrix
    * @param r1y The R1y component of the Hermite geometry matrix
    * @param p4x The P4x component of the Hermite geometry matrix
    * @param p4y The P4y component of the Hermite geometry matrix
    * @param r4x The R4x component of the Hermite geometry matrix
    * @param r4y The R4y component of the Hermite geometry matrix
    */
   GlmHermiteSpline2(float p1x, float p1y, float r1x, float r1y,
                     float p4x, float p4y, float r4x, float r4y);
   
   /**
    * Virtual destructor
    */
   virtual ~GlmHermiteSpline2();

   /**
    * Sets the list of coefficients for the first Hermite geometry matrix.
    * For example, a Hermite geometry matrix (Gh) looks like so:
    * <p>
    * <pre>
    *       | P1x P1y |
    *  Gh = | P4x P4y |
    *       | R1x R1y |
    *       | R4x R4y |
    * </pre>
    * <p>
    *
    * @param p1x The P1x component of the Hermite geometry matrix
    * @param p1y The P1y component of the Hermite geometry matrix
    * @param r1x The R1x component of the Hermite geometry matrix
    * @param r1y The R1y component of the Hermite geometry matrix
    * @param p4x The P4x component of the Hermite geometry matrix
    * @param p4y The P4y component of the Hermite geometry matrix
    * @param r4x The R4x component of the Hermite geometry matrix
    * @param r4y The R4y component of the Hermite geometry matrix
    */
   virtual void initSpline(float p1x, float p1y, float r1x, float r1y,
                           float p4x, float p4y, float r4x, float r4y);
   
   /**
    * Adds a new hermite spline segment to the end of the current
    * spline segment. P1x and P1y are used from the last segments
    * P4x and P4y values. Thus, the basis matrix looks like this:
    * <p>
    * <pre>
    *       | lastP1x lastP1y |
    *  Gh = | P4x     P4y     |
    *       | R1x     R1y     |
    *       | R4x     R4y     |
    * </pre>
    * <p>
    *
    * @param startingPercentage the percentage of the line this
    *                           spline segment will be responsible
    *                           for. For example, a value of 0.6 will
    *                           mean that the specified current spline segment
    *                           will be responsible for 60%-100% of
    *                           the whole spline.
    * @param r1x The R1x component of the Hermite geometry matrix
    * @param r1y The R1y component of the Hermite geometry matrix
    * @param p4x The P4x component of the Hermite geometry matrix
    * @param p4y The P4y component of the Hermite geometry matrix
    * @param r4x The R4x component of the Hermite geometry matrix
    * @param r4y The R4y component of the Hermite geometry matrix
    */
   virtual void addSplineSegment(float startingPercentage,
                                 float r1x, float r1y,
                                 float p4x, float p4y, float r4x, float r4y);

   /**
    * Gets the coefficients for the Hermite geometry matrix specified
    * by the hermiteIndex.
    * For example, the Hermite geometry matrix (Gh) might look like so:
    * <p>
    * <pre>
    *       | P1x P1y |
    *  Gh = | P4x P4y |
    *       | R1x R1y |
    *       | R4x R4y |
    * </pre>
    * <p>
    * @param hermiteIndex The spline segment to get the coefficients for
    * @param p1x The P1x component of the Hermite geometry matrix
    * @param p1y The P1y component of the Hermite geometry matrix
    * @param r1x The R1x component of the Hermite geometry matrix
    * @param r1y The R1y component of the Hermite geometry matrix
    * @param p4x The P4x component of the Hermite geometry matrix
    * @param p4y The P4y component of the Hermite geometry matrix
    * @param r4x The R4x component of the Hermite geometry matrix
    * @param r4y The R4y component of the Hermite geometry matrix
    */
   virtual void getSplineCoefficients(unsigned int hermiteIndex,
                                      float& p1x, float& p1y,
                                      float& r1x, float& r1y,
                                      float& p4x, float& p4y,
                                      float& r4x, float& r4y);
   /**
    * Gets the number of Hermite segments in this spline.
    *
    * @param The number of hermite segments in the spline.
    */
   virtual int getSplineSegments();
   
   /**
    * Sets the coefficients for the Hermite geometry matrix specified
    * by the hermiteIndex.
    * For example, the Hermite geometry matrix (Gh) might look like so:
    * <p>
    * <pre>
    *       | P1x P1y |
    *  Gh = | P4x P4y |
    *       | R1x R1y |
    *       | R4x R4y |
    * </pre>
    * <p>
    * @param hermiteIndex The spline segment to set the coefficients
    *                     for
    * @param percentage The starting percentage of the entire spline
    *                   length that this spline segment should be
    *                   responsible for
    * @param p1x The P1x component of the Hermite geometry matrix
    * @param p1y The P1y component of the Hermite geometry matrix
    * @param r1x The R1x component of the Hermite geometry matrix
    * @param r1y The R1y component of the Hermite geometry matrix
    * @param p4x The P4x component of the Hermite geometry matrix
    * @param p4y The P4y component of the Hermite geometry matrix
    * @param r4x The R4x component of the Hermite geometry matrix
    * @param r4y The R4y component of the Hermite geometry matrix
    */
   virtual void setSplineSegment(unsigned int hermiteIndex,
                                 float percentage,
                                 float p1x, float p1y,
                                 float r1x, float r1y,
                                 float p4x, float p4y,
                                 float r4x, float r4y);
   
   /**
    * Gets a calculated position as a function of a percentage of the total
    * distance for a path. If a line is 4 units long, given by the two
    * end-points (0, 0) and (4, 0), and the percentage given is 0.5
    * (50%), the position along the line would be (2, 0).
    *
    * @param percent the percentage of the path travelled
    * @return a point that is the calculated position along the path
    *         using the given percentage.
    */
   virtual GlmPoint2 getPositionByPercentage(float percent);
   
   /**
    * Stream operator to send this data to a stream. The path is
    * converted to a string that looks something like this: XXXXXX
    *
    * @param outStream the output stream specified by whatever is to
    *                  the left of this operator.
    * @param hs the hermite spline to send to the output stream
    * @return a reference to the modified output stream
    */
   friend ostream& operator<< (ostream& outStream,
                               const GlmHermiteSpline2& hs);
   
 private:
   /**
    * The number of spline segments in this spline
    */
   unsigned int mSegments;

   /**
    * The active spline percentages for each one of the curve segments
    */
   float* mSplinePercentage;
   
   /**
    * The blending functions to use for this cubic polynomial curve
    */
   float* mXYCoefficients[2];
};

#endif /* GLMHERMITESPLINE2_H */
