/*
 * $Header: /cvsroot/glmath/glmath/src/GlmPoint2.h,v 1.4 2003/03/25 15:49:36 msporny Exp $
 *
 * Copyright (C) 2000, Manu Sporny <msporny@vt.edu>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#ifndef GLMPOINT2_H
#define GLMPOINT2_H

#include <glmath/GlmConfig.h>

class GlmPoint2
{
   friend class GlmLine2;
   
 public:
   /**
    * Default constructor
    */
   GlmPoint2();

   /**
    * Constructor that creates a point out of the 3 given positions.
    *
    * @param positionX the Cartesian value of X
    * @param positionY the Cartesian value of Y
    */
   GlmPoint2(float positionX, float positionY);

   /**
    * Virtual destructor
    */
   virtual ~GlmPoint2();

   /**
    * Gets the Cartesian X and Y coordinates of this point.
    *
    * @param positionX the Cartesian value of X
    * @param positionY the Cartesian value of Y
    */
   virtual void getXY(float& positionX, float& positionY);

   /**
    * Sets the Cartesian X and Y coordinates of this point.
    *
    * @param positionX the Cartesian value of X
    * @param positionY the Cartesian value of Y
    */
   virtual void setXY(float positionX, float positionY);
   
   /**
    * Checks the current point for equality against the given point.
    *
    * @param p the point to compare this point against
    * @return 1 if the points are equal, of 0 if the points are not
    *         equal
    */
   int operator ==(const GlmPoint2& p);
   
   /**
    * Checks the current point for inequality against the given point.
    *
    * @param p the point to compare this point against
    * @return 1 if the points are not equal, or 0 if the points are
    *         equal
    */
   int operator !=(const GlmPoint2& p);

   /**
    * Add and assign operator.
    *
    * @param p the point to add to this point
    * @return a reference to the modified GlmPoint2
    */
   GlmPoint2& operator +=(const GlmPoint2& p);

   /**
    * Subtract and assign operator.
    *
    * @param p the point to subtract from this point
    * @return a reference to the modified GlmPoint2
    */
   GlmPoint2& operator -=(const GlmPoint2& p);
   
   /**
    * Divide by a scalar and assign operator.
    *
    * @param scalar the scalar to divide this point by.
    * @return a reference to the modified GlmPoint2
    */
   GlmPoint2& operator /=(const float scalar);

   /**
    * Multiply by a scalar and assign operator.
    *
    * @param scalar the scalar to multiply this point by.
    * @return a reference to the modified GlmPoint2
    */
   GlmPoint2& operator *=(const float scalar);
   
   /**
    * Operator that casts this object to a float array. This may be
    * used with OpenGL to specify 2d points by calling glVertex2f
    * and passing this object as the first argument to the function.
    *
    * @return a 2 element array of floats with the following layout:
    *         [x, y]. This is a borrowed reference, do not delete!
    */
   operator float*();
   
   /**
    * Stream operator to send this data to a stream. The point is
    * converted to a string that looks something like this:
    * "[x, y]". Where x and y are the x and y values, respectively.
    *
    * @param outStream the output stream specified by whatever is to
    *                  the left of this operator.
    * @param point the point object to send to the output stream
    * @return a reference to the modified output stream
    */
   friend ostream& operator<< (ostream& outStream, const GlmPoint2& point);
   
protected:
   float mPosition[2];
};

#endif /* GLMPOINT2_H */
