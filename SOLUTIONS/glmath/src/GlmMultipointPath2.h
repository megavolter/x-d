/*
 * $Id: GlmMultipointPath2.h,v 1.5 2003/03/25 15:49:36 msporny Exp $
 *
 * Copyright (C) 2002, xRhino, Inc. <msporny@xrhino.com>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#ifndef GLMMULTIPOINTPATH2_H
#define GLMMULTIPOINTPATH2_H

#include <vector>
#include <glmath/GlmConfig.h>
#include <glmath/GlmPoint2.h>
#include <glmath/GlmLine2.h>
#include <glmath/GlmPath2.h>

class GlmMultipointPath2 : public GlmPath2
{
 public:
   /**
    * Default constructor
    */
   GlmMultipointPath2();

   /**
    * Virtual destructor
    */
   virtual ~GlmMultipointPath2();

   /**
    * Adds a waypoint to this path given an x and y position.
    *
    * @param x the x position for the waypoint
    * @param y the y position for the waypoint
    */
   virtual void addWaypoint(float x, float y);

   /**
    * Adds a waypoint to this path given a GlmPoint2.
    *
    * @param waypoint a 2D point to append to the end of this path
    */
   virtual void addWaypoint(GlmPoint2& waypoint);
   
   /**
    * Gets all of the waypoints for this path.
    *
    * @return a vector containing all of the waypoints for this
    *         path. THIS IS A BORROWED REFERENCE, DON'T DO ANYTHING TO IT.
    */
   virtual const vector<GlmPoint2>& getWaypoints();

   /**
    * Gets a calculated position as a function of a percentage of the total
    * distance for a path. If a line is 4 units long, given by the two
    * end-points (0, 0) and (4, 0), and the percentage given is 0.5
    * (50%), the position along the line would be (2, 0).
    *
    * @param percent the percentage of the path travelled
    * @return a point that is the calculated position along the path
    *         using the given percentage.
    */
   virtual GlmPoint2 getPositionByPercentage(float percent);
   
   /**
    * Stream operator to send this data to a stream. The path is
    * converted to a string that looks something like this:
    * "[(x1, y1), (x2, y2), ..numpoints]". Where (x1, y1) are the first points
    * coordinates and (x2, y2) are the second points coordinates all
    * the way up to the total number of points in the path.
    *
    * @param outStream the output stream specified by whatever is to
    *                  the left of this operator.
    * @param path the path object to send to the output stream
    * @return a reference to the modified output stream
    */
   friend ostream& operator<< (ostream& outStream,
                               const GlmMultipointPath2& path);
   
 protected:
   // The raw point data for the multi-point path
   vector<GlmPoint2> mWaypoints;
   
 private:
   /**
    * Recalculates the length of the path. The path length is used to
    * calculate where on the path a particular point lies.
    */
   void recalculatePathLength();

   // Lines are used for the path calculation
   vector<GlmLine2> mPath;
   float mPathDistance;
};

#endif /* GLMMULTIPOINTPATH2_H */
