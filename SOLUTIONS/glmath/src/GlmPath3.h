/*
 * $Id: GlmPath3.h,v 1.2 2003/03/25 15:49:36 msporny Exp $
 *
 * Copyright (C) 2002, xRhino, Inc. <msporny@xrhino.com>
 *
 * This library is free software; you can redistribute and/or modify
 * it under the terms of the Q Public License as long as the code is
 * not incorporated into a proprietary product. A separate license is
 * available for use of this code with a proprietary system. For more
 * information on the license, please see /usr/share/doc/libGLmath/copyright.
 */
#ifndef GLMPATH3_H
#define GLMPATH3_H

#include <vector>
#include <glmath/GlmConfig.h>
#include <glmath/GlmPoint3.h>

class GlmPath3
{
 public:
   /**
    * Default constructor
    */
   GlmPath3();

   /**
    * Virtual destructor
    */
   virtual ~GlmPath3();

   /**
    * Gets a calculated position as a function of a percentage of the total
    * distance for a path. If a line is 4 units long, given by the two
    * end-points (0, 0) and (4, 0), and the percentage given is 0.5
    * (50%), the position along the line would be (2, 0).
    *
    * @param percent the percentage of the path travelled
    * @return a point that is the calculated position along the path
    *         using the given percentage.
    */
   virtual GlmPoint3 getPositionByPercentage(float percent) = 0;
};

#endif /* GLMPATH3_H */
