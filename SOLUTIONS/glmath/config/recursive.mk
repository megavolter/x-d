.PHONY: ${SUBDIRS}

${SUBDIRS}:
	@echo "Entering ${DIRPRFX}$@..."
	@${MAKE} -C $@ ${RECURSIVE_TARGET}
	@echo "Leaving ${DIRPRFX}$@..."

recursive: ${SUBDIRS}
