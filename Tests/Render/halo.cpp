#include "BASE/LANG.h"
#include "BASE/CAST.h"
#include "BASE/CODE.h"
#include "OS/XD_OS.h"
#include "BASE/XD_WWW.h"
#include "BASE/XD_I2.h"

using namespace nXD;
struct Game : public XD_WWW
{
	bln play;
	OS window;
	OS::WDGT_T widget;
	Game() : play(false), window(), widget(0x0) {}

xd_interface:
	static sip
	iKeyListen(Game* _this, void*)
	{
		mNOTE("Window closed by user");
		_this->play = false;
		return 0x0;
	}
xd_functional:
	int
	fGameStart()
	{
		if(play){
			mERRORP("Game allready played!");
			return X_X;
		}
		play = true;
		while(fUpdateX());
		mNOTE("Application terminated");
		return 0;
	}

	x_result
	fUpdateX()
	{
		window.fSyncX();
		return play;
	}
};

int main()
{
	Game game;
	game.widget = OS::iWdgtCreateP();
	XD_I2 screen_size(-1);
	OS::iScreenSizeGetX(screen_size);
	OS::iWdgtInitX("Game", screen_size.x/4, screen_size.y/4, screen_size.x/2, screen_size.y/2, game.widget);
	typedef XD_WWW_T<void, OS::INPUT_KEY>::www tt;
	game.window.fInputKeyListenerA().fObjectSetA(&game).fMethodSetA(RCAST(tt, Game::iKeyListen));
	OS::iWdgtShowX(true, game.widget);
	return game.fGameStart();
}
