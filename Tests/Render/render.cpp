#include "BASE/LANG.h"
#include "BASE/CAST.h"
#include "BASE/CODE.h"
#include "OS/XD_OS.h"
#include "BASE/XD_WWW.h"
#include "BASE/XD_I2.h"
#include "GFX/XD_GFX.h"
#include "BASE/XD_FIGURES_3D.h"
#include "XD_RDL.h"
#include "XD_GFX_DECODER.h"

si4 gen_texture[4*4];

str files[] = {
		"fx/v_5.glsl",		//0
		"fx/f_5.glsl",		//1
		"fx/f_5t.glsl",		//2
		"fx/v_5vwp.glsl",	//3
		"t/cloud.png",		//4
		"t/cube4.png",		//5
		"t/sphere5.png",	//6
		"t/sphere6.png",	//7
		"t/sphere7.png",	//8
		"t/sphere8.png",	//9
};

using namespace nXD;
struct Game : public XD_WWW
{
	bln play;
	bln load_done;

	OS window;
	OS::WDGT_T widget;
	GFX gfx;
	GFX::V5O* gf_object;
	RDL loader;
	GFX_DECODER decoder;

	XD_WWW data_customer;

	struct GraphicsObject
	{

	};

	Game() :
		play(false), load_done(false), window(), widget(0x0),
		gf_object(0x0)
	{
		data_customer.fSetA(this, RCAST(XD_WWW::www, iData), 0x0);
		loader.fSyncListenerA().fSetA(this, RCAST(XD_WWW::www, iAllDataLoaded), 0x0);
		for(szt i = 0; i < 4*4; ++i){
			gen_texture[i] = XD_COLOR::eNiceSky + 0x11111111;
		}
	}

xd_interface:
	static sip
	iKeyListen(Game* _this, OS::INPUT_KEY* _key)
	{
            mNOTE("Window closed by user");
            _this->play = false;
            return 0x0;
	}

	static sip
	iData(Game* _this, void*){
		mNOTE("Data");
		return 0x0;
	}

	static sip
	iAllDataLoaded(Game* _this, void*){
		mNOTE("All");
		if(A_A != _this->fShadersCompile()){
			mERRORP("Due shader compilation");
		}
		return 0x0;
	}

xd_functional:
	x_result
	fGameStart()
	{
		if(play){
			mERRORP(mBRACED("Game allready started!"));
			return X_X;
		}
		play = true;
		gfx.fFrameA().fClearColorSetX(XD_COLOR(XD_COLOR::COLORS::eNiceSky));
		sPrepareDrawingX();
		while(fUpdateX());
		mNOTE("Application terminated");
		return A_A;
    }

	x_result
	fUpdateX()
	{
		mXD_RESULTA(loader.fSyncX());
		mXD_RESULTA(window.fSyncX());
		gfx.fFrameA().fOpenX();
		gfx.fFrameA().fClearX(true, true, true);
		//mXD_RESULTA(sRenderObjectX());
		mXCALL(gfx.fFrameA().fShowX());
		gfx.fFrameA().fCloseX();
		return play;
	}

	x_result
	fShadersCompile()
	{
		mBLOCK("Load shaders");
		GFX::PROGRAM shader;
		XD_DATA& data = *loader.fDataGetP(files[0]);
		shader.vertex.fSet(data.fStrGetP(), data.data_size);

		data = *loader.fDataGetP(files[2]);
		shader.fragment.fSet(data.fStrGetP(), data.data_size);

		mBLOCK("Compile shaders v5");
		mXD_RESULTA(gfx.fShaderA().fCompileX(shader));
		mXD_RESULTA(gfx.fShaderA().fSetX(shader));

		data = *loader.fDataGetP(files[4]);
		GFX_DECODER::BITMAP bitmap(data.fDataGetP(), data.fSize());

		mBLOCK("Texture preparing");
		mXD_RESULTA(decoder.fInitX(bitmap));
		mXD_RESULTA(decoder.fDecodeX(bitmap));

		GFX::TEXTURE* texture = gfx.fSamplerA().fCreateP(
				bitmap.bmp, bitmap.bmp_size, bitmap.size.x, bitmap.size.y, bitmap.format);
		if(texture == 0x0){
			mERRORP("Texture creation");
			return X_X;
		}
		decoder.fClear(bitmap);
		mXCALL(gfx.fSamplerA().fSetX(texture, 0));

		if(mXD_MAGIC("Enable gen texture", false)){
			GFX::TEXTURE* g_texture = gfx.fSamplerA().fCreateP(gen_texture, 4*4*4, 4, 4, XDE_PIXEL_r8g8b8a8);
			if(texture == 0x0){
				mERRORP("Gen texture creation");
				return X_X;
			}
			mXCALL(gfx.fSamplerA().fSetX(g_texture, 0));
		}

		if(true)
			return A_A;

		GFX::PROGRAM shader_diffuse;
		data = *loader.fDataGetP(files[3]);
		shader_diffuse.fragment.fSet(data.fStrGetP(), data.fSize());
		data = *loader.fDataGetP(files[4]);
		shader_diffuse.vertex.fSet(data.fStrGetP(), data.fSize());

		mXD_RESULTA(gfx.fShaderA().fCompileX(shader_diffuse));
		mXD_RESULTA(gfx.fShaderA().fSetX(shader_diffuse));

		return A_A;
	}

	x_result
	fWindowInitX()
	{
		sip handler = OS::iWdgtDescriptorGet(widget);
		return gfx.fInitX(handler);
	}

	x_result
	sPrepareDrawingX()
	{
		gf_object = new GFX::V5O;
		gf_object->fAllocate(Figures3D::quad_vx_cnt, Figures3D::quad_ix_cnt);
		for(szt i = 0; i < Figures3D::quad_vx_cnt; ++i){//1
			V5& v = gf_object->fVertexA(i);
			v.fPositionSetA(
					Figures3D::quad_pos_n[i][0],
					Figures3D::quad_pos_n[i][1],
					Figures3D::quad_pos_n[i][2]).fTexelSetA(
							Figures3D::quad_tex[i][0],
							Figures3D::quad_tex[i][1]);
		}//1
		gf_object->vx_num = Figures3D::quad_vx_cnt;
		gf_object->ix_num = Figures3D::quad_ix_cnt;
		memcpy(gf_object->ix, Figures3D::quad_ix, gf_object->fIdxSize());
		return sCompileObjectX();
	}

	x_result
	sCompileObjectX()
	{
		return gfx.fRenderA().fCompileX(gf_object);
	}

	x_result
	sRenderObjectX()
	{
		mXCALL(gfx.fRenderA().fSetGeometryX(gf_object));
		return gfx.fRenderA().fRasteriseX();
	}

};

int main()
{
	Game game;
	XD_STRING working_directory;
	OS::iWorkingDirectoryGetX(working_directory);
	mNOTE("Application started in"<<mBRACED(working_directory.fGet()));
	working_directory.fAdd("/data/");
	OS::iCurentDirectorySetX(working_directory.fGet());
	mNOTE("New directory is"<<mBRACED(working_directory.fGet()));
	OS::iWorkingDirectoryGetX(working_directory);
	mNOTE(mBRACED(working_directory.fGet()));

	game.loader.fDataLoad(files[4], game.data_customer);
	game.loader.fDataLoad(files[0], game.data_customer);
	game.loader.fDataLoad(files[1], game.data_customer);
	game.loader.fDataLoad(files[2], game.data_customer);
	game.loader.fDataLoad(files[3], game.data_customer);
	game.widget = OS::iWdgtCreateP();

	XD_I2 screen_size(-1);
	OS::iScreenSizeGetX(screen_size);
	OS::iWdgtInitX("Game", screen_size.x/4, screen_size.y/4, screen_size.x/2, screen_size.y/2, game.widget);

	typedef XD_WWW_T<void, OS::INPUT_KEY>::www tt;
	game.window.fInputKeyListenerA().fObjectSetA(&game).fMethodSetA(RCAST(tt, Game::iKeyListen));
	sip dc = OS::iWdgtDescriptorGet(game.widget);
	mXD_CHECK(game.fWindowInitX() == A_A);

	OS::iWdgtShowX(true, game.widget);
	return game.fGameStart();

}
