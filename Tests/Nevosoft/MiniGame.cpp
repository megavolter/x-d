#include "MiniGame.hpp"
#include "BASE/LANG.h"
#include "BASE/CODE.h"
#include "BASE/TYPE.h"

#define CALLX(function) \
	assert(nXD::OK == function);

MiniGame::~MiniGame()
{
	xd_none;
}

MiniGameImpl::MiniGameImpl()
{
	size_t vertex_cnt = 4 + cColumns * cRows * 4;
	m_batcher.object.fAllocateA(vertex_cnt, 6 * vertex_cnt);
	m_selection.Drop();
}

MiniGameImpl::~MiniGameImpl()
{

}

void MiniGameImpl::Initialize()
{
	m_playing = false;
	nXD::BITMAP bitmap;
	CALLX(WindowSetupX(out bitmap));
	CALLX(GraphicsSetupX(out bitmap));
	CALLX(MakeGameObjectsX());
}

x_result MiniGameImpl::WindowSetupX(out nXD::BITMAP& _bitmap)
{
	mCODE_BLOCK("Window creation");
	nXD::DATA image;
	mXCALL(m_rdl.fInstantLoadX(out image, "data/image.png"));

	_bitmap.fSetA(image.fDataGetP(), image.data_size);
	mXCALL(nXD::GFX_DECODER::iInitX(_bitmap));
	mXCALL(nXD::GFX_DECODER::iDecodeX(_bitmap));

	m_os.fInputKeyListenerA().fObjectSetA(RCAST(void*, this))
							.fMethodSetA(RCAST( nXD::OS::www, &Input));
	m_window = nXD::OS::iWdgtCreateP();

	nXD::I2 screen_size;
	nXD::OS::iScreenSizeGetX(screen_size);
	mXCALL(nXD::OS::iWdgtInitX("nevosoft",
			mVAR("position x", screen_size.x/2 - _bitmap.size.x/2),
			mVAR("position y", screen_size.y/2 - _bitmap.size.y/2),
			mVAR("size x", _bitmap.size.x),
			mVAR("size y", _bitmap.size.y),
			mVAR("descriptor", m_window)
			));
	nXD::OS::iWdgtShowX(true, m_window);
	return nXD::OK;
}

x_result MiniGameImpl::GraphicsSetupX(out nXD::BITMAP& _bitmap)
{
	// Render.
	sip handler = nXD::OS::iWdgtDescriptorGet(m_window);
	m_gfx.fInitX(handler);
	m_gfx.fFrameA().fClearColorSetX(nXD::XD_COLOR::eMandarin);
	// Texture.
	mXCALL(m_gfx.fSamplerA()
			.fTextureCompileX(_bitmap, out m_batcher.texture));
	mXCALL(m_gfx.fSamplerA().fSetX(m_batcher.texture, 0));

	// Shader.
	nXD::DATA v_shader;
	mXCALL(m_rdl.fInstantLoadX(out v_shader, "data/vertex.glsl"));
	nXD::DATA f_shader;
	mXCALL(m_rdl.fInstantLoadX(out f_shader, "data/fragment.glsl"));

	m_batcher.program.vertex.fSet(v_shader.fStrGetP(), v_shader.fSize());
	m_batcher.program.fragment.fSet(f_shader.fStrGetP(), f_shader.fSize());

	mXCALL(m_gfx.fShaderA().fCompileX(m_batcher.program));
	mXCALL(m_gfx.fShaderA().fSetX(m_batcher.program));

	nXD::F44 camera;
	nXD::F4 border(
			mVAR("left", 0),
			mVAR("right", _bitmap.size.x),
			mVAR("top", 0),
			mVAR("bot", _bitmap.size.y)
			);

	nXD::F2 plane(0.001, 1000);
	camera.fOrthoA(border, plane);
	mXCALL(m_gfx.fShaderA().fVarX(camera, "_camera", 0));

	nXD::DATA completed;
	mXCALL(m_rdl.fInstantLoadX(completed, "data/completed.png"));
	nXD::BITMAP bitmap;
	bitmap.fSetA(completed.fDataGetP(), completed.fSize());
	mXCALL(nXD::GFX_DECODER::iInitX(bitmap));
	mXCALL(nXD::GFX_DECODER::iDecodeX(bitmap));
	mXCALL(m_gfx.fSamplerA()
				.fTextureCompileX(bitmap, out m_win.texture));

	nXD::F4 vertexes(
		mVAR("left", _bitmap.size.x/2 + bitmap.size.x/5),
		mVAR("right",_bitmap.size.x/2 - bitmap.size.x/5),
		mVAR("top", _bitmap.size.y/2 + bitmap.size.x/5),
		mVAR("bot", _bitmap.size.y/2 - bitmap.size.x/5)
		);

	nXD::F4 texels(1,0,1,0);
	m_win.object.fMakeQuadA(vertexes, texels, -0.9);
	mXCALL(m_gfx.fRenderA().fCompileX(m_win.object));
	bitmap.fFree();

	return nXD::OK;
}

x_result MiniGameImpl::MakeGameObjectsX()
{
	// Geometry.
	Rect vertex_offs = {
			0,
			0,
			SCAST(float, m_batcher.texture.size.x),
			SCAST(float, m_batcher.texture.size.y)
			};
	Rect texels_offs = {0,0,1,1};

	m_back.SetVertexesA(vertex_offs).SetTexelsA(texels_offs);

	nXD::F4 chip_size(
			mVAR("Chip width", m_batcher.texture.size.x/cColumns),
			mVAR("Chip height", m_batcher.texture.size.y/cRows),
			mVAR("Chip size U", 1.f / cColumns),
			mVAR("Chip size V", 1.f / cRows)
			);

	for(size_t col = 1, row = 1; ; ++col){
		if(col > cColumns){
			col = 1; row += 1;
			if(row > cRows) break;
		}
		Rect pos = {
				mVAR("X offset", (col-1) * chip_size.x),
				mVAR("Y offset", (row-1) * chip_size.y),
				mVAR("W offset", col * chip_size.x),
				mVAR("H offset", row * chip_size.y),
				};

		Rect tex = {
				mVAR("U ", (col-1) * chip_size.data[2]),
				mVAR("V ", (row-1) * chip_size.data[3]),
				mVAR("U'", col * chip_size.data[2]),
				mVAR("V'", row * chip_size.data[3]),
				};
		m_chips[col-1][row-1].SetVertexesA(pos).SetTexelsA(tex);
	}

	return nXD::OK;
}

x_result MiniGameImpl::UpdateX()
{
	m_completed = IsCompleted();
	return nXD::OK;
}

void MiniGameImpl::Batcher::Clear()
{
	v_cnt = i_cnt = q_cnt = 0;
}

x_result MiniGameImpl::Batcher::Add(const Chip& _object)
{
	const float z = -1;
	object.fVertexA(v_cnt++)
			.fPositionSetA(_object.vertex.left, _object.vertex.top, z)
			.fTexelSetA(_object.texels.left, _object.texels.top);
	object.fVertexA(v_cnt++)
			.fPositionSetA(_object.vertex.right, _object.vertex.top, z)
			.fTexelSetA(_object.texels.right, _object.texels.top);
	object.fVertexA(v_cnt++)
			.fPositionSetA(_object.vertex.right, _object.vertex.bottom, z)
			.fTexelSetA(_object.texels.right, _object.texels.bottom);
	object.fVertexA(v_cnt++)
			.fPositionSetA(_object.vertex.left, _object.vertex.bottom, z)
			.fTexelSetA(_object.texels.left, _object.texels.bottom);

	object.fIndexP()[i_cnt++] 	= 0 + q_cnt*4;
	object.fIndexP()[i_cnt++]	= 2 + q_cnt*4;
	object.fIndexP()[i_cnt++]	= 3 + q_cnt*4;
	object.fIndexP()[i_cnt++]	= 0 + q_cnt*4;
	object.fIndexP()[i_cnt++]	= 1 + q_cnt*4;
	object.fIndexP()[i_cnt++]	= 2 + q_cnt*4;
	q_cnt += 1;

	object.vx_num = v_cnt;
	object.ix_num = i_cnt+12;

	return nXD::OK;
}

x_result MiniGameImpl::PrerenderX()
{
	m_batcher.Clear();

	for(size_t r = 0; r < cRows; ++r){
		for(size_t c = 0; c < cColumns; ++c)
			m_batcher.Add(m_chips[r][c]);
	}
	mXCALL(m_gfx.fRenderA().fCompileX(m_batcher.object));
	return nXD::OK;
}

void MiniGameImpl::Render() const
{
	CALLX(m_gfx.fFrameCA().fClearX(true, true, true));
	CALLX(m_gfx.fSamplerCA().fSetX(m_batcher.texture, 0));
	CALLX(m_gfx.fRenderCA().fSetGeometryX(m_batcher.object));
	CALLX(m_gfx.fRenderCA().fRasterizeX());
	if(m_completed && m_show_win_panel)
	{
		CALLX(m_gfx.fSamplerCA().fSetX(m_win.texture, 0));
		CALLX(m_gfx.fRenderCA().fSetGeometryX(m_win.object));
		CALLX(m_gfx.fRenderCA().fRasterizeX());
	}
	CALLX(m_gfx.fFrameCA().fShowX());
}

x_result MiniGameImpl::FindChip(float x, float y)
{
	nXD::F2 chip_size(
			m_batcher.texture.size.x/cColumns,
			m_batcher.texture.size.y/cRows
	);
	m_selection.y = (x / chip_size.x);// [0 - cColumns]
	m_selection.x = cRows - y / chip_size.y;// [0 - cRows]

	if(m_selection.x > -1 && m_selection.x < cColumns
			&& m_selection.y > -1 && m_selection.y < cRows)
	{// Bounds ok.
		mDEV_VAR(m_selection.x);
		mDEV_VAR(m_selection.y);
		mDEV_VAR(x);
		mDEV_VAR(y);
		return nXD::OK;
	}

	return nXD::BAD;

}

void MiniGameImpl::Click(float x, float y)
{
	if(m_completed){
		Shuffle();
		return;
	}
	if(m_selection.IsSelected())
	{//1 Get previous selection.
		nXD::I2 selected = m_selection;
		FindChip(x,y);
		if(m_selection.IsSelected())
		{//2 Get curent selection
			Chip& l = m_chips[selected.y][selected.x];
			Chip& r = m_chips[m_selection.y][m_selection.x];
			if(&l != &r)// Same chip
				l.SwapTexels(r);
			m_selection.Drop();
			return;
		}//2
		else
		{//2 Selection fails.
			m_selection.Drop();
		}//2
	}//1
	else
	{//1 Find first chip
		FindChip(x,y);
	}//1
}

bool MiniGameImpl::IsCompleted() const
{
	// Check textels gradation.
	for(size_t i = 1; i < cRows*cColumns; ++i){
		size_t r = i/cRows;
		size_t c = i%cRows;
		const Chip& cursor = m_chips[r][c];
		if(c > 0){
			const Chip& compare = m_chips[r][c-1];
			if(cursor.texels.left < compare.texels.left)
				return false;
		}
		if(r > 0)
		{
			const Chip& compare = m_chips[r-1][c];
			if(cursor.texels.top < compare.texels.top)
				return false;
		}
	}
	return true;

	//Check columns.
}

x_result MiniGameImpl::PlayX()
{
	m_playing = true;
	m_completed = false;
	m_show_win_panel = false;

	while(m_playing)
	{
		mXCALL(m_os.fSyncX());
		mXCALL(UpdateX());
		mXCALL(PrerenderX());
		Render();
	}
	return nXD::OK;
}

void MiniGameImpl::Stop()
{
	m_playing = false;
}

void MiniGameImpl::OnKey(nXD::OS::INPUT_KEY* _data)
{
	if(_data->type == nXD::XDE_INPUT_DOWN)
	{//1
		switch(_data->key) {//2
			case nXD::XDE_KEY_ESC:
				Stop();
				break;
			case nXD::XDE_KEY_MOUSE_L: {//3
				nXD::F4 cursor_pos;
				nXD::OS::iWdgtCursorGet(m_window, cursor_pos);
				Click(	SCAST(float, cursor_pos.x - cursor_pos.z),
						SCAST(float, cursor_pos.y-cursor_pos.w));
			}//3
		}//2
	}//1
}

#include <random>
#include <ctime>
void MiniGameImpl::Shuffle()
{
	std::mt19937 gen(time(0));
	std::uniform_int_distribution<> rand_col(0, cColumns - 1);
	std::uniform_int_distribution<> rand_row(0, cRows - 1);
	// Swap each chip.
	for(size_t r = 0; r < cRows; ++r){
		for(size_t c = 0; c < cColumns; ++c)
			m_chips[r][c].SwapTexels(m_chips[rand_row(gen)][rand_col(gen)]);
	}
}
