#pragma once

#include "MiniGameInterface.h"
#include "OS/XD_OS.h"
#include "GFX/XD_GFX.h"
#include "RDL/XD_RDL.h"
#include "GFX_DECODER/XD_GFX_DECODER.h"
#include "BASE/LANG.h"
#include "GFX/Object.h"

class MiniGameImpl : public MiniGame, public nXD::XD_WWW
{
	struct Chip{
		Rect vertex;
		Rect texels;

		void SwapTexels(out Chip& _target){
			Rect temp = texels;
			texels = _target.texels;
			_target.texels = temp;
		}

		Chip& SetVertexesA(const Rect& _v){
			vertex = _v;
			return *this;
		}

		Chip& SetTexelsA(const Rect& _t){
			texels = _t;
			return *this;
		}
	};

	struct Batcher : public nXD::Object {
		// Verteces count.
		size_t v_cnt;
		// Indeces count.
		size_t i_cnt;
		// Quads count.
		size_t q_cnt;
		// Add cheap into DrawData.
		x_result Add(const Chip& _object);
		// Batcher must be clean befor each Render call.
		void Clear();

	};

	struct Selection : public nXD::I2 {
		void Drop() {fSet(-1,-1);}
		bool IsSelected() {return x != -1 || y != -1;}
		void Select(int x, int y) {fSet(x,y);};
	};

xd_data:
	// Window wraper.
	nXD::OS::WDGT_T m_window;
	// Operation system manager.
	nXD::OS m_os;
	// Render subsystem.
	nXD::GFX m_gfx;
	// Raw data loader. Async in future.
	nXD::RDL m_rdl;
	// Is game still playing. Used in Play func.
	bool m_playing;
	// Is puzzle complited.
	bool m_completed;
	// Show/hide win panel.
	bool m_show_win_panel;
	// Background. Disabled now.
	Chip m_back;
	// Chips array.
	Chip m_chips[cRows][cColumns];
	// Win panel. Disabled.
	nXD::Object m_win;
	// Draw batcher. Because Draw() is const, but for 3d engine its is a problem.
	Batcher m_batcher;
	// Curent selected chip handler.
	Selection m_selection;

xd_functional:
	MiniGameImpl();
	virtual ~MiniGameImpl();

	void Initialize() override; // called before start

	void Click(float x, float y) override; // called on mouse click ((x, y) are screen coordinates)

	bool IsCompleted() const override; // returns true when minigame successfully finished

	void Render() const override; // called to render one frame (must use global Render function)

xd_functional:
	// Setup application widget.
	x_result WindowSetupX(out nXD::BITMAP& _bitmap);
	// Setup graphics objects.
	x_result GraphicsSetupX(out nXD::BITMAP& _bitmap);
	// Update logic.
	x_result UpdateX();
	// Prepare to draw.
	x_result PrerenderX();
	// Main loop.
	x_result PlayX();
	// Create gameplay objects.
	x_result MakeGameObjectsX();
	// Write find result into m_selection.
	x_result FindChip(float x, float y);
	// Shiffle chips on field.
	void Shuffle();
	// Forgot last selected chip.
	void DropSelection();
	// Key event dispatcher.
	void OnKey(nXD::OS::INPUT_KEY* _data);
	// Break mail loop.
	void Stop();
	// Input callback.
	static sip Input(MiniGameImpl* _this, nXD::OS::INPUT_KEY* _data) { _this->OnKey(_data); }
};
