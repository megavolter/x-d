message(">>> Module Nevosoft")

find_package(OpenGL REQUIRED)
link_directories(${OPENGL_gl_LIBRARY})

add_definitions(-DXD_WIN)

ADD_MODULE(BASE)
ADD_MODULE(GFX)
ADD_MODULE(RDL)
ADD_MODULE(GFX_DECODER)
ADD_MODULE(TIME)
ADD_MODULE(OS)

message(MODULES_SRC ${MODULES_SRC} ${MODULES_INC})
message(MODULES_DIR ${MODULES_DIR})

set(PROJECT_SRC_DIR
	${XD_DIR}/Tests/Nevosoft
)

set(PROJECT_SRC
	${PROJECT_SRC_DIR}/main.cpp
	${PROJECT_SRC_DIR}/MiniGameInterface.h
	${PROJECT_SRC_DIR}/MiniGame.hpp
	${PROJECT_SRC_DIR}/MiniGame.cpp
)

set(CMAKE_BUILD_TYPE Debug)

add_executable(nevosoft 
	${PROJECT_SRC}
	${MODULES_SRC}
	${MODULES_INC}
)

include_directories(${MODULES_DIR})

target_link_libraries(nevosoft 
	${MODULES_LIB}
)