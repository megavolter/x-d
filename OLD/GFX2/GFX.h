#ifdef _NO_COMPILE
#ifndef GFX2_GFX_H_
#define GFX2_GFX_H_

#include "BASE/CODE.h"
#include "BASE/LANG.h"
#include "BASE/TYPE.h"

#include "BASE/XD_F4.h"

namespace nXD{ namespace nGFX{

class GFX_I
{
xd_functional:

};

class GFX_OLD
{
xd_open_types:

	struct V5
	{
		fl4 x, y, z, u, v;
		void fClear() { x=y=z=u=v=0;}
		void fSet(fl4 _data[5]) { x = _data[0]; y = _data[1]; z = _data[2]; u = _data[3]; v = _data[4]; }
		void fSet(fl4 _x, fl4 _y, fl4 _z, fl4 _u, fl4 _v);
	};

	struct Extra;
	struct Data
	{
		si4 data_type;
		bln fTypeOf(si4 _type);
	};
	typedef Data* DataT;
	typedef Extra* ExtraT;

	class GfxInterface
	{
		xd_shared:
		GFX::ExtraT m_extra;
		xd_functional:
		GfxInterface();
		void fInit(GFX::ExtraT _extra);
	};

	class Scene : public GfxInterface
	{
		xd_functional:
		x_result fClearColorSetX(const XD_F4& _color);
		x_result fPresentX();
		x_result fClearX(bln _color, bln _z, bln _stencil);
	};

	class Shader : public GfxInterface
	{
		xd_functional:
		DataT fProgrammCompileP(str _vertex, str _fragment);
		x_result fProgrammSetupX(DataT _programm);

	};

	class Rasterizer : public GfxInterface
	{
		xd_func_block("Dynamic"):
		x_result fDynamicDrawX(V5* _v, si2 _v_cnt, si2* _i, si2 _i_cnt) xd_low_perf;

		xd_func_block("Static"):
		DataT fStaticGetP();
		x_result fStaticSetX(DataT _data);
	};

xd_functional:
	GFX();
	x_result fInitX(sip _draw_context);
	x_result fDestroyX() { /*TODO: fDestroyX*/ return A_A;}

xd_accessors:
	Scene& fSceneA() { return m_scene; }
	Shader& fShaderA() { return m_shader; }
	Rasterizer& fRasterA() { return m_raster; }

xd_data:
	ExtraT m_extra;
	Scene m_scene;
	Shader m_shader;
	Rasterizer m_raster;
};

class GFX_EX : public GFX
{
xd_functional:
	GFX_EX();
};

}}

#endif /* GFX2_GFX_H_ */
#endif
