#include "MODULES/GFX2/GFX.h"
#include "MODULES/GFX2/win/GFX_Extra.h"

#include "BASE/CAST.h"

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "SOLUTIONS/_win/glext/glext.h"

#include "MODULES/OS/XD_OS.h"

namespace nXD { namespace nGFX {

#define mGL_CHECK() Gl::iGlCheckErrorX(__LINE__,__FUNCTION__)

struct GFX::Extra
{
	HGLRC   m_gl_context;
	HDC     m_draw_context;
	GLuint	m_v5_buffer_dynamic[2];
};

class Gl
{
	xd_functional:

	static
	void
	iGlInit()
	{
		glGetError();
	}


	static
	const char*
	iTranslateGlError(GLenum err_code)
	{
		#define mGL_ERROR(ERR) case(ERR): return #ERR; break

		switch(err_code)
		{
		mGL_ERROR(GL_INVALID_ENUM);
		mGL_ERROR(GL_INVALID_VALUE);
		mGL_ERROR(GL_INVALID_OPERATION);
		mGL_ERROR(GL_STACK_OVERFLOW);
		mGL_ERROR(GL_STACK_UNDERFLOW);
		mGL_ERROR(GL_OUT_OF_MEMORY);
		mGL_ERROR(GL_INVALID_FRAMEBUFFER_OPERATION);
		}
		mERRORF("unknown error code");
		return 0x0;

		#undef mGL_ERROR
	}

	static
	si4
	iGlCheckErrorX(int line, const char* function)
	{
		GLuint err_code = glGetError();
		if(err_code != GL_NO_ERROR)
		{
			mERROR("gl "<<iTranslateGlError(err_code)<<" in "<<function<<" "<<line);
			return A_A;
		}
		return O_O;
	}

	static
	str
	iGetVersion()
	{
		return 0x0;
	}
};

// Gfx.

GFX::GFX()
 : m_extra(0x0)
{}

x_result
GFX::fInitX(sip _draw_descriptor)
{

mBLOCK("Init draw surface");
	HWND hWnd = RCAST(HWND, _draw_descriptor);
	HDC draw_context = GetDC(hWnd);

	PIXELFORMATDESCRIPTOR   pfd = {
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_SUPPORT_OPENGL |
		PFD_DRAW_TO_WINDOW |
		PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,
		16,
		0,0,0,0,0,0,
		0,0,0,
		0,0,0,0,
		16,
		0,0,
		PFD_MAIN_PLANE,
		0,0,0,0,
		};

	si4 pixel_fmt = 0;
	pixel_fmt = ChoosePixelFormat(draw_context,&pfd);
	if(!SetPixelFormat(draw_context, pixel_fmt, &pfd))
	{
		mERRORF("set pixel format");
		return X_X;
	}

	HGLRC gl_context = wglCreateContext(draw_context);
	if(!gl_context)
	{
		XD_STRING error;
		OS::iGetSystemErrorX(error);
		mERRORF("OpenGl: context creation. System error: "<< mBRACED(error.fGet()));
		return X_X;
	}

	if(TRUE == wglMakeCurrent(draw_context, gl_context) && mGL_CHECK())
	{
		XD_STRING error;
		OS::iGetSystemErrorX(error);
		mERRORF(" OpenGl: makecurrent context. System error: " << mBRACED(error.fGet()));
		return X_X;
	}

mBLOCK("Getting GL version");
	const char* openGlVersion = RCAST(const char*, glGetString(GL_VERSION));
	if(0x0 == openGlVersion)
	{
		mGL_CHECK();
		XD_STRING error;
		OS::iGetSystemErrorX(error);
		mERRORP("OpenGl: check version failed. System error: " << mBRACED(error.fGet()));
		return X_X;
	}

mBLOCK("Version");
	int OpenGLVersion[2];
	glGetIntegerv(GL_MAJOR_VERSION, &OpenGLVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &OpenGLVersion[1]);
	mGL_CHECK();

	mNOTE( "XD_GFX started whith"
		<< mBRACED(openGlVersion)
		<< mSPASED("version")
		<< mBRACED(OpenGLVersion[0])<<" "
		<< mBRACED(OpenGLVersion[1])
	);

mBLOCK("Init subsystems");
	m_extra = new GFX::Extra;
	m_extra->m_draw_context = draw_context;
	m_extra->m_gl_context = gl_context;

	m_scene.fInit(m_extra);
	m_shader.fInit(m_extra);
	m_raster.fInit(m_extra);

mBLOCK("Tuning ogl");
	mREFACTOR_THIS();
	glDisable(GL_CULL_FACE);
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	mGL_CHECK();

mBLOCK("Init buffers");
	glGenBuffers(2, m_extra->m_v5_buffer_dynamic);
	mGL_CHECK();

mBLOCK("End");
	return A_A;
}

// Interface.

GFX::GfxInterface::GfxInterface()
	: m_extra(0x0)
{}

void
GFX::GfxInterface::fInit(ExtraT _extra){
	m_extra = _extra;
}

// Scene.

x_result
GFX::Scene::fClearColorSetX(const XD_F4& _color)
{
	glClearColor(_color.r, _color.g, _color.b, _color.a);
	mGL_CHECK();
	return A_A;
}

x_result
GFX::Scene::fClearX(bln _color, bln _z, bln _stencil)
{
	ui4 mask =
		(_color)	? GL_COLOR_BUFFER_BIT	: 0	|
		(_stencil)	? GL_STENCIL_BUFFER_BIT : 0 |
		(_z)		? GL_DEPTH_BUFFER_BIT	: 0	;

	glClear(mask);
	mGL_CHECK();
	return A_A;
}

x_result
GFX::Scene::fPresentX()
{
	BOOL result = SwapBuffers(m_extra->m_draw_context);
	if(TRUE != result)
	{
		mERRORF("swap error");
		return X_X;
	}
	return A_A;
}
// Shader.

// Rasterizer.
x_result
GFX::Rasterizer::fDynamicDrawX(V5* _v, si2 _v_cnt, si2* _i, si2 _i_cnt)
{
mCOMMENT("Vertexes");
	glBindBuffer(GL_ARRAY_BUFFER, m_extra->m_v5_buffer_dynamic[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(V5) * _v_cnt, _v, GL_DYNAMIC_DRAW);
	mGL_CHECK();

mCOMMENT("Indices");
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_extra->m_v5_buffer_dynamic[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(si2) * _i_cnt, _i, GL_DYNAMIC_DRAW);
	mGL_CHECK();

mCOMMENT("Drawing");
	glDrawElements(
		GL_TRIANGLES,
		_i_cnt,
		GL_UNSIGNED_SHORT,
		0
	);
	mGL_CHECK();
	return A_A;
}

// GfxEx.
GFX_EX::GFX_EX() {

}


}}// ns
