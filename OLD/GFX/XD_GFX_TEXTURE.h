#ifndef XD_GFX_TEXTURE_H
#define XD_GFX_TEXTURE_H

namespace nXD {

struct TEXTURE
{
	// Size in pixels.
	XD_I2		size;
	// Real used size in texture. Sometimes not equal whith size.
	XD_I2		real;
	// Effective space into texture.
	XD_F2		uses;
	// Texture pixel format
	XDE_PIXEL		frmt;
	// The bitmap data. Only for access to pixels. Default NULL.
	void*			btmp;
	szt				btmp_size;

xd_shared:
	struct  Extra;
	Extra*  extra;

xd_special:
	friend class GFX;
	TEXTURE() : size(), real(), uses(), frmt(XDE_PIXEL_NONE),
				btmp(0x0), btmp_size(0),
				extra(0) {}
	~TEXTURE();

xd_functional:
	XD_F2 fSizeF2() { return XD_F2(size.x, size.y); }
};


}// XD

#endif // XD_GFX_TEXTURE_H
