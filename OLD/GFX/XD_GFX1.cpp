#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "SOLUTIONS/_win/glext/glext.h"

#include "MODULES/GFX/XD_GFX.h"

#include "BASE/LANG.h"
#include "BASE/CAST.h"
#include "BASE/LOG.h"
#include "BASE/CODE.h"

#include "MODULES/OS/XD_OS.h"

namespace nXD{
//============================================================================//
#define mGL_CHECK() GL::iGlCheckErrorX(__LINE__,__FUNCTION__)
//============================================================================//
struct GFX::CONTEXT
{
	HGLRC   m_gl_context;
	HDC     m_draw_context;

	bool    m_scene_opened;
	bool    m_target_opened;

	CONTEXT() : m_gl_context(0), m_draw_context(0),
		m_scene_opened(false), m_target_opened(false) {}
};
//============================================================================//
struct GFX::TEXTURE::Extra
{
	GLuint texture;
};
//============================================================================//
struct GFX::DATA5::Extra
{
	GLuint vx_buffer;
	GLuint ix_buffer;
};
//============================================================================//
xd_namespace GL
{
	static
	void
	iGlInit()
	{
		glGetError();
	}

	#define mGL_ERROR(ERR) case(ERR): return #ERR; break
	static
	const char*
	iTranslateGlError(GLenum err_code)
	{
		switch(err_code)
		{
		mGL_ERROR(GL_INVALID_ENUM);
		mGL_ERROR(GL_INVALID_VALUE);
		mGL_ERROR(GL_INVALID_OPERATION);
		mGL_ERROR(GL_STACK_OVERFLOW);
		mGL_ERROR(GL_STACK_UNDERFLOW);
		mGL_ERROR(GL_OUT_OF_MEMORY);
		mGL_ERROR(GL_INVALID_FRAMEBUFFER_OPERATION);
		}
		mERRORF("unknown error code");
		return 0x0;
	}
	#undef mGL_ERROR

	static
	si4
	iGlCheckErrorX(int line, const char* function)
	{
		GLuint err_code = glGetError();
		if(err_code != GL_NO_ERROR)
		{
			mERROR("gl "<<iTranslateGlError(err_code)<<" in "<<function<<" "<<line);
			return A_A;
		}
		return O_O;
	}

	static
	str
	iGetVersion()
	{
		return 0x0;
	}

};

//============================================================================//
GFX::GFX()
	: m_context(0x0)
{}

//============================================================================//
si4
GFX::fInitX(si4 _draw_descriptor)
{
	mDEBUGONLY( && _draw_descriptor == 0 )
	{
		mXD_FAIL("Bad draw descriptor aka HWND");
	}
	mBLOCK("Creating Window");
	m_context = new GFX::CONTEXT;

	HWND hWnd = RCAST(HWND, _draw_descriptor);
	m_context->m_draw_context = GetDC(hWnd);

	mBLOCK("Prepare pixel format descriptor");
	PIXELFORMATDESCRIPTOR   pfd = {
	sizeof(PIXELFORMATDESCRIPTOR),
	1,
	PFD_SUPPORT_OPENGL |
	PFD_DRAW_TO_WINDOW |
	PFD_DOUBLEBUFFER,
	PFD_TYPE_RGBA,
	16,
	0,0,0,0,0,0,
	0,0,0,
	0,0,0,0,
	16,
	0,0,
	PFD_MAIN_PLANE,
	0,0,0,0,
	};

	mBLOCK("Choosing pixel format");
	si4 pixel_fmt = 0;
	pixel_fmt = ChoosePixelFormat(m_context->m_draw_context,&pfd);
	if(!SetPixelFormat(m_context->m_draw_context, pixel_fmt, &pfd))
	{
		mERRORF("set pixel format");
		return X_X;
	}

	mBLOCK("Creation GL context");
	m_context->m_gl_context = wglCreateContext(m_context->m_draw_context);
	if(!m_context->m_gl_context)
	{
		XD_STRING error;
		OS::iGetSystemErrorX(error);
		mERRORF("OpenGl: context creation. System error: "<< mBRACED(error.fGet()));
		return X_X;
	}
	if(TRUE == wglMakeCurrent(m_context->m_draw_context, m_context->m_gl_context) && mGL_CHECK())
	{
		XD_STRING error;
		OS::iGetSystemErrorX(error);
		mERRORF(" OpenGl: makecurrent context. System error: " << mBRACED(error.fGet()));
		return X_X;
	}

	mBLOCK("Getting GL version");
	const char* openGlVersion = RCAST(const char*, glGetString(GL_VERSION));
	if(0x0 == openGlVersion)
	{
		mGL_CHECK();
		XD_STRING error;
		OS::iGetSystemErrorX(error);
		mERRORP("OpenGl: check version failed. System error: " << mBRACED(error.fGet()));
		return X_X;
	}

	mREFACTOR_THIS();
	glDisable(GL_CULL_FACE);

	int OpenGLVersion[2];
	glGetIntegerv(GL_MAJOR_VERSION, &OpenGLVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &OpenGLVersion[1]);

	mNOTE("XD_GFX started whith"
		<<mBRACED(openGlVersion)<<" version "
		<<mBRACED(OpenGLVersion[0])<<" "
		<<mBRACED(OpenGLVersion[1])
	);

	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	return A_A;
}
//============================================================================//
void
GFX::fClearColorSet(const XD_F4& color)
{
	glClearColor(color.r, color.g, color.b, color.a); mGL_CHECK();
}
//============================================================================//
si4
GFX::fSceneOpenX()
{
	if(false == m_context->m_scene_opened)
	{
		m_context->m_scene_opened = true;
		return A_A;
	}
	mERROR("scene allready opened");
	return X_X;
}
//============================================================================//
si4
GFX::fSceneClearX(bool _color, bool _stencil, bool _depth)
{
	if(fIsSceneOpened())
	{
		ui4 mask =
				(_color)	? GL_COLOR_BUFFER_BIT	: 0	|
				(_stencil)	? GL_STENCIL_BUFFER_BIT : 0 |
				(_depth)	? GL_DEPTH_BUFFER_BIT	: 0	;

		glClear(mask);mGL_CHECK();
		return A_A;
	}
	mERROR("scene must be opened");
	return X_X;
}
//============================================================================//
si4
GFX::fSceneCloseX()
{
	if(fIsSceneOpened())
	{//1
		BOOL res = SwapBuffers(m_context->m_draw_context);
		if(TRUE != res)
		{
			mERRORF("swap error");
			return X_X;
		}
		m_context->m_scene_opened = false;
		return A_A;
	}//1
	mERROR("scene must be opened");
	return X_X;
}
//============================================================================//
bool
GFX::fIsSceneOpened()
{
	return m_context->m_scene_opened;
}
//============================================================================//
// Texture.
//============================================================================//
GLenum
gTranslatePixelFormat(XDE_PIXEL fmt)
{
	switch(fmt)
	{
	case XDE_PIXEL_NONE:
	case XDE_PIXEL_:
		mWARNINGF("translated none pixel format");
		break;
	case XDE_PIXEL_r8g8b8a8:
		return GL_RGBA;
		break;
	case XDE_PIXEL_r8g8b8:
		return GL_RGB;
	}
	mERRORF("translated bad pixel format");
	return 0;
}
//============================================================================//
GFX::TEXTURE*
GFX::fTextureCreateP(void* data, szt data_size, si4 w, si4 h, XDE_PIXEL fmt)
{
	mIGNOREP(data_size);
	GLuint tex;
	glGenTextures(1, &tex); mGL_CHECK();
	glBindTexture(GL_TEXTURE_2D, tex); mGL_CHECK();

	mGL_CHECK();

	glTexImage2D(
		GL_TEXTURE_2D,
		0,
		gTranslatePixelFormat(fmt),
		w,
		h,
		0,
		gTranslatePixelFormat(fmt),
		GL_UNSIGNED_BYTE,
		data
	);

	if(mGL_CHECK())
	{
		mERRORF("error texture upload");
		return 0x0;
	}

	GFX::TEXTURE* tex_ptr = new GFX::TEXTURE;
	tex_ptr->size.fSet(w,h);
	tex_ptr->real.fSet(w,h);
	tex_ptr->uses.fSet(1.f,1.f);
	tex_ptr->extra = new GFX::TEXTURE::Extra;
	tex_ptr->extra->texture = tex;

	return tex_ptr;
}
//============================================================================//
si4
GFX::fTextureSetX(const GFX::TEXTURE* _texture, const char* _name, szt _chanel)
{
	mTODO("Shader sampler attach");
	mIGNOREP(_name);
	mIGNOREP(_chanel);

	if(_texture && _texture->extra)
	{
		glBindTexture(GL_TEXTURE_2D, _texture->extra->texture);
		if(mGL_CHECK())
		{
			mERRORF("texture binding");
			return X_X;
		}
		return A_A;
	}
	else
	{
		mERRORF("bad texture gained");
	}
	return X_X;
}
//============================================================================//
si4
GFX::fTextureQualitySetX(si4 _lvl)
{
	GLint quality = (_lvl)? GL_LINEAR : GL_NEAREST;
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, quality);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, quality);

	return (mGL_CHECK() == A_A)? X_X : A_A;
}
//============================================================================//
GFX::TEXTURE::~TEXTURE()
{
	if(btmp_size)
	{
		delete SCAST(ui1* ,btmp);
		btmp = 0x0;
	}
	if(extra)
	{
		mERRORF("video data must be released before");
	}
}
//============================================================================//
// Data5.
//============================================================================//
si4
GFX::DATA5::fInitX(V5* _v, szt _v_cnt, ui2* _i, szt _i_cnt)
{
	if(!_v || !_i)
	{
		mERRORF("bad data ptr");
		return X_X;
	}
	v_data_size = sizeof(GFX::V5) * _v_cnt;
	if(v_data_size)
	{// 1
		if(v_data)
		{// 2
			if(v_data_cnt != _v_cnt)
			{// 3
				delete v_data;
				v_data = new GFX::V5[v_data_size];
			}// 3
		}// 2
		else {
			v_data = new GFX::V5[v_data_size];
		}

		memcpy(v_data, _v, v_data_size);
	}// 1
	v_data_cnt = _v_cnt;

	i_data_size = sizeof(ui2) * _i_cnt;
	if(i_data_size)
	{// 1
		if(i_data)
		{//2
			if ( i_data_cnt != _i_cnt)
			{// 3
				delete i_data;
				i_data = new ui2[i_data_size];
			}// 3
		}//2
		else {
			i_data = new ui2[i_data_size];
		}
		memcpy(i_data, _i, sizeof(ui2) * _i_cnt);
	}// 1
	i_data_cnt = _i_cnt;

	return A_A;
}
//============================================================================//
si4
GFX::DATA5::sSetLocalX(V5* vx, szt vxcnt, ui2* ix, szt ixcnt)
{// No need to delete datas.
	if(!vx || !ix)
	{
		mERRORF("bad data ptr");
		return X_X;
	}
	v_data = vx;
	v_data_cnt = vxcnt;
	i_data = ix;
	i_data_cnt = ixcnt;

	return A_A;
}
//============================================================================//
si4
GFX::DATA5::fQuadInitX(fl4 sizex, fl4 sizey, fl4 x, fl4 y)
{
mBLOCK("Create quad geometry");
	GFX::V5 quad[4];
	quad[0].fSetPos(x,y,0);
	quad[0].fSetTex(0,0);

	quad[1].fSetPos(x + sizex, y, 0);
	quad[1].fSetTex(1,0);

	quad[2].fSetPos(x + sizex, y + sizey, 0);
	quad[2].fSetTex(1,1);

	quad[3].fSetPos(x, y + sizey,0);
	quad[3].fSetTex(0,1);

mBLOCK("Indexes");
	ui2 iarray[6] = {0,1,2,0,2,3};
	return fInitX(quad, 4, iarray, 6);
}
//============================================================================//
si4
GFX::DATA5::fQuadInitX(const XD_F4& _size, const XD_F4& _frame)
{
	GFX::V5 quad[4];
	if(false)
	{
		quad[0].fSetPos(_size.z, _size.w, 0);
		quad[0].fSetTex(_frame.x, _frame.y);

		quad[1].fSetPos(_size.x + _size.z, _size.w, 0);
		quad[1].fSetTex(_frame.z, _frame.y);

		quad[2].fSetPos(_size.x + _size.z, _size.y + _size.w, 0);
		quad[2].fSetTex(_frame.z, _frame.w);

		quad[3].fSetPos(_size.z, _size.y + _size.w, 0);
		quad[3].fSetTex(_frame.x, _frame.w);
	}
	else
	{
		quad[0].fSetPos(-_size.z, -_size.w, 0);
		quad[0].fSetTex(_frame.x, _frame.w);

		quad[1].fSetPos(-_size.z, _size.w, 0);
		quad[1].fSetTex(_frame.y, _frame.y);

		quad[2].fSetPos(_size.z, _size.w, 0);
		quad[2].fSetTex(_frame.z, _frame.y);

		quad[3].fSetPos(_size.z, -_size.w, 0);
		quad[3].fSetTex(_frame.z, _frame.w);
	}
	ui2 iquad[6] = {0,1,2,0,2,3,};
	return fInitX(quad, 4, iquad, 6);
}
//============================================================================//
bool
GFX::DATA5::fIsEmpty()
{
	return v_data == 0x0;
}
//============================================================================//
bool
GFX::DATA5::fIsComposed()
{
	return extra != 0x0;
}
//============================================================================//
si4
GFX::DATA5::fRelease()
{
	if(v_data_size)
	{
		delete v_data;
		v_data = 0x0;
	}
	if(i_data_size)
	{
		delete i_data;
		i_data = 0x0;
	}
	return A_A;
}
//============================================================================//
GFX::DATA5::~DATA5()
{
	if(extra)
	{
		mWARNING("data5 destroyed whithout video memory releasing");
	}
}
//============================================================================//
si4
GFX::fData5ComposeX(GFX::DATA5* raster)
{
	if(raster && !raster->fIsEmpty() && !raster->fIsComposed())
	{
		GLuint x[2];
		glGenBuffers(2, x);
		mBLOCK("Vertex");
		{
			glBindBuffer(GL_ARRAY_BUFFER, x[0]); mGL_CHECK();
			glBufferData(GL_ARRAY_BUFFER,
				sizeof(V5) * raster->v_data_cnt, raster->v_data, GL_DYNAMIC_DRAW
			); mGL_CHECK();
		}
		mBLOCK("Index");
		{
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, x[1]); mGL_CHECK();
			glBufferData(GL_ELEMENT_ARRAY_BUFFER,
				sizeof(ui2) * raster->i_data_cnt, raster->i_data,GL_DYNAMIC_DRAW
			); mGL_CHECK();
		}

		raster->extra = new DATA5::Extra;

		raster->extra->vx_buffer = x[0];
		raster->extra->ix_buffer = x[1];

		return A_A;
	}
	else
	{
		mERRORF("data empty or allredy composed");
	}

	return X_X;
}
//============================================================================//
void
GFX::fDataDestroy(DATA5* data)
{
	if(data && !data->fIsEmpty() && data->fIsComposed())
	{
		GFX::DATA5::Extra* extra_ptr = data->extra;
		glDeleteBuffers(1, &extra_ptr->vx_buffer);
		glDeleteBuffers(1, &extra_ptr->ix_buffer);
		delete data->extra;
		data->extra = 0x0;
	}
}
//============================================================================//
bool
GFX::fIsDataComposed(const GFX::DATA5* data)
{
	return NULL != data->extra;
}
//============================================================================//
GLint
gGetCurrentShader()
{
	GLint result;
	glGetIntegerv(GL_CURRENT_PROGRAM, &result);
	return result;
}
//============================================================================//
si4
GFX::fData5SetX(const GFX::DATA5* raster)
{
	if(!raster || !raster->extra)
	{
		mERRORF("bad raster data");
		return X_X;
	}
	else
	{//1
		if(fIsDataComposed(raster))
		{//2
			DATA5::Extra* extra = raster->extra;
		mCOMMENT("Shader");
			GLint shader = gGetCurrentShader();
			if(shader < 0)
			{//3
				mERROR("current shader");
				return X_X;
			}//3
		mCOMMENT("Vertex buffer");
			{//3
				glBindBuffer(GL_ARRAY_BUFFER, extra->vx_buffer);
				// Pos.
				GLint a_pos = glGetAttribLocation(shader, "a_pos");
				if(a_pos >=0)
				{//4
					glVertexAttribPointer(
						a_pos, 3, GL_FLOAT, GL_FALSE, sizeof(V5), 0
					);
					mGL_CHECK();
					glEnableVertexAttribArray(0);
				}//4
				else
				{//4
					mERROR("shader attrib a_pos");
					return X_X;
				}//4
				// Tex.
				GLint a_tex = glGetAttribLocation(shader, "a_tex");
				if(a_tex >=0)
				{//4
					glVertexAttribPointer(
						a_tex, 2, GL_FLOAT, GL_FALSE, sizeof(V5),
						RCAST(void*, sizeof(fl4) * 3)
					);
					mGL_CHECK();
					glEnableVertexAttribArray(1);
				}//4
			}//3
		mCOMMENT("Index buffer");
			{//3
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, extra->ix_buffer);
				mGL_CHECK();
			}//3
			return A_A;
		}//2
		else
		{//2
			mERRORF("data must be composed");
		}//2
	}//1
	return X_X;
}
//============================================================================//
si4
GFX::fRasterizeX(const GFX::DATA5* curent_raster)
{
	mGL_CHECK();
	glDrawElements(
		GL_TRIANGLES,
		curent_raster->i_data_cnt,
		GL_UNSIGNED_SHORT,
		0
	);
	if(mGL_CHECK())
	{
		mERRORF("drawing elements");
		return X_X;
	}
	return A_A;
}
//============================================================================//
si4
gGetCompileStatus(ui4 compila)
{
	GLint status;
	glGetShaderiv(compila, GL_COMPILE_STATUS, &status);

	if(GL_FALSE == status)
	{
		GLint len;
		glGetShaderiv(compila, GL_INFO_LOG_LENGTH, &len);
		if(len)
		{
			static char log_text[256];
			glGetShaderInfoLog(compila, 256, &len, SCAST(GLchar*, log_text));
			mERROR(log_text);
		}
		return X_X;
	}
	return true;
}
//============================================================================//
//  PROGRAM.
//============================================================================//
struct GFX::PROGRAM::Extra
{
	GLuint program;
};
//============================================================================//
si4
gProgramError(GLuint shader)
{
	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	if(status == GL_FALSE)
	{
		GLint log_length;
		glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &log_length );

		if( log_length > 0 )
		{
			unsigned char *log = new unsigned char[log_length];
			glGetShaderInfoLog( shader, log_length, &log_length,
								(GLchar*)&log[0] );
			mERRORF(log);
			delete[] log;
		}
		return X_X;
	}
	return A_A;
}
//============================================================================//
GFX::PROGRAM*
GFX::fProgramCreateP(const char* vertex, const char* fragment)
{
	if(vertex && fragment)
	{//1 Vertex.
		GLuint vs = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vs, 1, &vertex, 0);
		glCompileShader(vs);
		if(!mGL_CHECK() && A_A == gProgramError(vs))
		{//2 Fragment.
			GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
			glShaderSource(fs, 1, &fragment, 0);
			glCompileShader(fs);
			if(!mGL_CHECK() && A_A == gProgramError(fs))
			{//3 Programm.
				GLuint program = glCreateProgram();
				glAttachShader(program, vs);
				glAttachShader(program, fs);
				if(!mGL_CHECK())
				{//4 Linking.
					glLinkProgram(program);
					if(!mGL_CHECK())
					{//5 Finish.
						glBindAttribLocation(program, 0, "a_pos");
						glBindAttribLocation(program, 1, "a_tex");

						PROGRAM* result = new PROGRAM;
						result->vertex      = vertex;
						result->fragment    = fragment;
						PROGRAM::Extra* res_ext = new PROGRAM::Extra;
						res_ext->program = program;
						result->extra = res_ext;
						return result;
					}//5
					mERRORF("linking programm error");
					return NULL;
				}//4
				mERRORF("shader program attach");
				return NULL;
			}//3
			mERRORF("fragment shader compile");
			return NULL;
		}//2
		mERRORF("vertex shader compile");
		return NULL;
	}//1
	return NULL;
}

//============================================================================//
si4
GFX::fProgramSetX(const GFX::PROGRAM* _programm)
{
	mGL_CHECK();
	if(_programm)
		if(_programm->extra)
		{
			glUseProgram(_programm->extra->program);
		}
		else
		{
			mERRORF("Shader not assembled");
			return X_X;
		}
	else
		glUseProgram(0);

	if(mGL_CHECK())
	{
		mERRORF("setup shader program");
		return X_X;
	}

	return A_A;
}
//============================================================================//
si4
GFX::fProgramVfX(fl4 value, const char* name, szt reg)
{
	mIGNOREP(reg);
	GLint cur_shade = gGetCurrentShader();
	GLint unic = glGetUniformLocation(cur_shade, name);
	if(unic > 0)
	{
		glUniform1f(unic, value);
		if(mGL_CHECK())
		{
			mERRORF("");
		}
		return A_A;
	}
	else
	{
		mERRORF("bad program uniform "<< name);
	}
	return X_X;
}
//============================================================================//
si4
GFX::fProgramVf4X(const XD_F4& value, str name, szt reg)
{
	mIGNOREP(reg);
	GLint   cur_shade = gGetCurrentShader();
	GLint   unic = glGetUniformLocation(cur_shade, name);
	if(unic > 0)
	{
		glUniform4fv(unic, 1, value.data);
		if(mGL_CHECK())
		{
			mERRORF("");
		}
		return A_A;
	}
	else
	{
		mERRORF("Bad program uniform "<< name);
	}
	return X_X;
}
//============================================================================//
si4
GFX::fProgramVf44X(const XD_F44& value, const char* name, szt reg)
{
	mGL_CHECK();
	mIGNOREP(reg);
	GLint cur_shade = gGetCurrentShader();
	GLint unic = glGetUniformLocation(cur_shade, name);
	if(unic > 0)
	{
		glUniformMatrix4fv(unic, 1, false, value.data);
		if(mGL_CHECK())
		{
			mERRORF("");
		}
		return A_A;
	}
	else
	{
		mERRORF("bad program uniform parameter "<< name);
	}
	return X_X;
}
//============================================================================//
// Settings.
//============================================================================//
si4
GFX::fBlendX(XDE_BLEND blend)
{
	mGL_CHECK();
	switch(blend)
	{
	case XDE_BLEND_NONE:
	case XDE_BLEND_ONE:
		glDisable(GL_BLEND);
		break;
	case XDE_BLEND_NORMAL:
		glEnable(GL_BLEND);
		glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
		break;
	default:
		break;
	}
	if(mGL_CHECK())
	{
		mERRORF("bleng mode setup");
		return X_X;
	}
	return A_A;
}
//============================================================================//
si4
GFX::fDepthX(bool depth)
{
	if(depth)
	{
		glEnable(GL_DEPTH_TEST);
	}
	else
	{
		glDisable(GL_DEPTH_TEST);
	}
	if(mGL_CHECK())
	{
		mERRORF("depth test set");
		return X_X;
	}
	return A_A;
}
//============================================================================//
}//ns
