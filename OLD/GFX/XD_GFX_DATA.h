#ifndef GFX_XD_GFX_DATA_H_
#define GFX_XD_GFX_DATA_H_

namespace nXD
{

struct V5
{
	fl4 x,y,z;
	fl4 u,v;
public:
	void fSetPos(fl4 v_x, fl4 v_y, fl4 v_z)
	{
		x = v_x;
		y = v_y;
		z = v_z;
	}

	void fSetTex(fl4 v_u, fl4 v_v)
	{
		u = v_u;
		v = v_v;
	}
};

struct DATA5
{
	V5*		v_data;
	szt		v_data_cnt;
	szt		v_data_size;
	ui2*	i_data;
	szt		i_data_cnt;
	szt		i_data_size;
	si4		draw_type;

xd_data:
	struct	Extra;
	Extra*	extra;

public:
	DATA5() :
		v_data(0x0), v_data_cnt(0), v_data_size(0),
		i_data(0x0), i_data_cnt(0), i_data_size(0),
		draw_type(0), extra(0x0)
		{}
	~DATA5();

	si4		fInitX(V5* _v, szt _v_cnt, ui2* _i, szt _i_cnt);
	si4		fQuadInitX(fl4 sizex, fl4 sizey, fl4 x, fl4 y);
	si4		fQuadInitX(const XD_F4& _size, const XD_F4& _frame);
	bool	fIsEmpty();
	bool	fIsComposed();
	si4		fRelease();
	si4		sSetLocalX(V5* vx, szt vxcnt, ui2* ix, szt ixcnt);
};

}//ns

#endif /* GFX_XD_GFX_DATA_H_ */
