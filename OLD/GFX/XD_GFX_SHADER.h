#ifndef _XD_GFX_SHADER_H
#define _XD_GFX_SHADER_H

namespace nXD
{

struct PROGRAM
{
	friend class GFX;
	const char* vertex;
	const char* fragment;

public:
	struct  Extra;
	Extra*  extra;

private:
	PROGRAM(): vertex(0x0), fragment(0x0), extra(0x0) {}
};

}//XD

#endif // XD_GFX_SHADER_H
