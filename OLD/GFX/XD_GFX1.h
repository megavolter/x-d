#ifndef _XD_GFX_H_INCLUDED
#define _XD_GFX_H_INCLUDED
//============================================================================//
// This file a part of project VSGE.
// Look LISENCE.MD for more details.
// Author: UCKUHT
// Date: 2013-05-19
// Desc: simple graphics interface.
//============================================================================//
#include "BASE/LANG.h"
#include "BASE/TYPE.h"
#include "BASE/XD_POINT2.h"
#include "BASE/XD_VECTOR2.h"
#include "BASE/XD_COLOR.h"
#include "BASE/XD_F44.h"

#include "MODULES/GFX/XD_GFX_FRAME.h"
#include "MODULES/GFX/XD_GFX_RENDER.h"
#include "MODULES/GFX/XD_GFX_SETTINGS.h"
#include "MODULES/GFX/XD_GFX_SHADER.h"
#include "MODULES/GFX/XD_GFX_TEXTURE.h"
#include "MODULES/GFX/XD_GFX_DATA.h"

namespace nXD{

class GFX
{
xd_open_types:

xd_functional:
				GFX();

	si4			fInitX(si4 _draw_descriptor);
	si4			fSceneOpenX();
	si4			fSceneCloseX();
	si4			fSceneClearX(bool _color, bool _stencil, bool _depth);
	void		fClearColorSet(const XD_F4& color);
	bool		fIsSceneOpened();

	si4			fData5ComposeX(DATA5* data);
	bool		fIsDataComposed(const DATA5* data);
	si4			fData5SetX(const DATA5* raster);
	static void fDataDestroy(DATA5* data);

	si4			fBlendX(XDE_BLEND blend);
	si4			fDepthX(bool depth);

	PROGRAM*	fProgramCreateP(const char* vertex, const char* fragment);
	si4			fProgramSetX(const PROGRAM* _programmm);
	si4			fProgramDestroyX(PROGRAM* prog);
	si4			fProgramVfX(fl4 value, const char* name, szt reg);
	si4			fProgramVf4X(const XD_F4& value, str name, szt reg);
	si4			fProgramVf44X(const XD_F44& value, const char* name, szt reg);

	// Create texture.
	TEXTURE*	fTextureCreateP(
					void* data, szt data_size, si4 w, si4 h, XDE_PIXEL fmt);
	// Set up texture.
	si4			fTextureSetX(
					const TEXTURE* _texture, const char* _name, szt _chanel);
	// Destroys texture.
	static void fTextureDestroy(TEXTURE* texture);
	si4			fTextureQualitySetX(si4 _lvl);

	// Draw promotives.
	si4			fRasterizeX(const nXD::DATA5* curent_raster);

xd_data:
	struct			CONTEXT;
	CONTEXT*		m_context;

	GFX_RENDER		m_render;
	GFX_SETTINGS	m_settings;
	nXD::PROGRAM	m_shader;
	FRAME			m_frame;
	TEXTURE			m_texture;

xd_special:
	CONTEXT*	sContextGetP();
	void		sContextSet(CONTEXT* context);
};

}//ns


#endif // _XD_GFX_H_INCLUDED
