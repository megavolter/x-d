#ifndef _XD_GFX_FRAME_H
#define _XD_GFX_FRAME_H

#include "BASE/CODE.h"
#include "BASE/TYPE.h"


namespace nXD {

class FRAME
{
xd_functional:
	si4		fFrameBeginX();
	si4		fFrameEndX();

	si4		fFrameClearColor(const XD_COLOR& _color);
	si4		fDrawX();
};

}

#endif // XD_GFX_FRAME_H
