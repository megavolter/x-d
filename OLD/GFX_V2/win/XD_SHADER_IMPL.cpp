#include "XD_GFX_IMPL.h"
#include "MODULES/GFX_V2/XD_GFX_SHADER.h"

namespace nXD {

void
nGFX::SHADER::Program::Sysspec::fProgramSet(GLuint _value)
{
	this->program = _value;
}

x_result
nGFX::SHADER::fInitX()
{
	m_extra = SHADER::Extra::iCreate();
	return A_A;
}

x_result
nGFX::SHADER::fSetX(const nGFX::SHADER::Program& _program)
{
	GLuint program = _program.sysspec->fProgramGet();
	glUseProgram(program);
	mXD_RESULTA(GL::iErrorX());
	this->m_extra->fShaderSet(_program);
	return A_A;
}

x_result
nGFX::SHADER::fSetValueX(const XD_F44& _value, str _name)
{
	return fSetValueX(_value, _name, this->m_extra->fShaderGet());
}

x_result
nGFX::SHADER::fSetValueX(const XD_F44& _value, str _name, const Program& _program)
{
	GLint uni_loc = glGetUniformLocation(_program.sysspec->fProgramGet(), _name);
	if(uni_loc < 0){
		mERRORP("Can't find uniform location named = " << mBRACED(_name));
		return X_X;
	}

	glUniformMatrix4fv(uni_loc, 1, false, _value.data);
	mXD_RESULTA(GL::iErrorX());

	return A_A;
}


x_result
nGFX::SHADER::fCompileX(Program& _result)
{
	if(_result.vertex == 0x0 || _result.fragment == 0x0)
	{
		return X_X;
	}

mBLOCK("Compile vertex shader");
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &_result.vertex, 0);
	glCompileShader(vertex_shader);
	if(A_A != GL::iShaderCheck(vertex_shader))
	{
		glDeleteShader(vertex_shader);
		mERRORP("Can't compile vertex shader");
	}

mBLOCK("Checking vertex shader compilation result");
	mXD_RESULTA(GL::iErrorX());

mBLOCK("Compile fragment shader");
	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &_result.fragment, 0);
	glCompileShader(fragment_shader);
	if(A_A != GL::iShaderCheck(fragment_shader))
	{
		glDeleteShader(vertex_shader);
		glDeleteShader(fragment_shader);
		mERRORP("Can't compile fragment shader");
	}

mBLOCK("Cooking the program");
	GLuint program = glCreateProgram();
	glAttachShader(program, vertex_shader);
	glAttachShader(program, fragment_shader);
	mXD_RESULTA(GL::iErrorX());
	glLinkProgram(program);
	mXD_RESULTA(GL::iErrorX());

mBLOCK("Binding shader attributes");
	mREFACTOR_THIS("Rm bindings from here");
	glBindAttribLocation(program, 0, "a_pos");
	glBindAttribLocation(program, 1, "a_tex");
	mXD_RESULTA(GL::iErrorX());

mBLOCK("Set up sysspec program");
	_result.sysspec = Program::Sysspec::iCreateP();
	_result.sysspec->fProgramSet(program);

	return A_A;
}

}//ns


