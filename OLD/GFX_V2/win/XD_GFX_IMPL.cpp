#include <MODULES/GFX_V2/win/XD_GFX_IMPL.h>
#include "BASE/CODE.h"
#include "BASE/CAST.h"

#include "MODULES/OS/XD_OS.h"

namespace nXD { namespace nGFX {


GFX::GFX()
	: m_extra(0x0)
{}

x_result
GFX::fVersionX(XD_STRING& _result)
{
	GL::iErrorX();
	str gl_version = RCAST(str, glGetString(GL_VERSION));
	if(gl_version != 0) {
		_result.fAllocate(mXD_MAGIC("Random size", 200));
		_result << "Graphics module:" << mBRACED("XD_GFX_V2:glext")
				<< mSBRACED(gl_version);
		return A_A;
	}
	_result.fSetLocal("OpenGl version error");
	GL::iErrorX();
	return X_X;
}

x_result
GFX::fInitX(sip _draw_descriptor)
{
	mXD_CHECK(_draw_descriptor != 0x0);
	HWND hWnd = RCAST(HWND, _draw_descriptor);
	HDC draw_context = GetDC(hWnd);
	mXD_CHECK(draw_context != 0x0);

	mBLOCK("Prepare pixel format descriptor");
	PIXELFORMATDESCRIPTOR pfd = {
	sizeof(PIXELFORMATDESCRIPTOR),
	1,
	PFD_SUPPORT_OPENGL |
	PFD_DRAW_TO_WINDOW |
	PFD_DOUBLEBUFFER,
	PFD_TYPE_RGBA,
	16,
	0,0,0,0,0,0,
	0,0,0,
	0,0,0,0,
	16,
	0,0,
	PFD_MAIN_PLANE,
	0,0,0,0,
	};

	mBLOCK("Choosing pixel format");
	si4 pixel_fmt = 0;
	pixel_fmt = ChoosePixelFormat(draw_context,&pfd);
	if(!SetPixelFormat(draw_context, pixel_fmt, &pfd))
	{
		mERRORF("Set pixel format");
		return X_X;
	}

	mBLOCK("Creation GL context");
	HGLRC context = wglCreateContext(draw_context);
	if(!context)
	{
		XD_STRING error;
		OS::iGetSystemErrorX(error);
		mERRORF("OpenGl: context creation. System error: "
				<< mBRACED(error.fGet()));
		return X_X;
	}
	if(TRUE == wglMakeCurrent(draw_context, context)
			&& (A_A != GL::iErrorX())
	)
	{
		XD_STRING error;
		OS::iGetSystemErrorX(error);
		mERRORF(" OpenGl: makecurrent context. System error: "
				<< mBRACED(error.fGet()));
		return X_X;
	}

	IGFX::Extra* extra = IGFX::Extra::iCreate();
	extra->draw_context = draw_context;
	extra->render_context = context;
	m_extra = extra;

	mXD_RESULTA( fFrameA().fInitX(m_extra) );
	mXD_RESULTA( fShaderA().fInitX() );

	return A_A;
}

x_result
GFX::fDestroyX()
{
	wglDeleteContext(m_extra->render_context);
	GFX::Extra::iDestroy(m_extra);
	return A_A;
}

FRAME&
GFX::fFrameA()
{
	return m_frame;
}

x_result
FRAME::fBeginX()
{
	mXD_CHECK_ERR(
			m_opened != false,
			"Frame is allready opened",
			return X_X
	);
	m_opened = true;
	return A_A;
}

x_result
FRAME::fEndX()
{
	mXD_CHECK_ERR(
			m_opened != true,
			"Frame didn't opened",
			return X_X
	);
	m_opened = false;
	return A_A;
}

x_result
FRAME::fClearColorX(const XD_COLOR& _color)
{
	glClearColor(_color.r, _color.g, _color.b, _color.a);
	GL::iErrorX();
	return A_A;
}

x_result
FRAME::fClearX(bln _color, bln _stencil, bln _depth)
{
	if(!m_opened) {
		mERRORP("Scene not opened");
		return X_X;
	}
	ui4 mask =
		(_color)	? GL_COLOR_BUFFER_BIT	: 0	|
		(_stencil)	? GL_STENCIL_BUFFER_BIT : 0 |
		(_depth)	? GL_DEPTH_BUFFER_BIT	: 0	;
	glClear(mask);
	return A_A;
}

x_result
FRAME::fDrawX()
{
	BOOL res = SwapBuffers(m_extra->draw_context);
	if(TRUE != res)
	{
		mERRORF("Present frame");
		return X_X;
	}
	return A_A;
}

x_result
FRAME::fInitX(IGFX::Extra* _extra)
{
	m_extra = _extra;
	m_opened = false;
	return A_A;
}

x_result
RENDER::fCompileX(RENDER::VertexBuffer5& _target)
{
	return A_A;
}

x_result
RENDER::fSetX(RENDER::VertexBuffer5& _terget)
{
	return A_A;
}

}}//ns
