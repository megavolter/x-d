#ifndef GFX_V2_WIN_XD_GFX_IMPL_H_
#define GFX_V2_WIN_XD_GFX_IMPL_H_

#include "MODULES/GFX_V2/XD_GFX.h"
#include "MODULES/GFX_V2/XD_GFX_SHADER.h"

#include "windows.h"
#include "GL/gl.h"
#include "GL/glu.h"

#include "SOLUTIONS/_win/glext/glext.h"

namespace nXD { namespace nGFX {

class GL
{
xd_interface:
	static
	x_result
	iShaderCheck(GLint shader)
	{
		mBLOCK("Check shader error");
		GLint status = 0x0;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
		if(GL_FALSE == status)
		{
			const szt log_size = 300;
			GLchar log[300];
			GLint log_len = 0;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_len);
			glGetShaderInfoLog(shader, log_size, &log_len, log);
			mERROR(log);
			return X_X;
		}
		return A_A;
	}

	static
	x_result
	iErrorX()
	{
		str error = iCheckError();
		if( error != 0x0) {
			mERROR(error);
			return X_X;
		}
		return A_A;
	}

	static
	str
	iCheckError()
	{
		GLenum gl_error = glGetError();
		switch(gl_error)
		{
		case GL_NO_ERROR: return 0x0;
		mXD_CASE_TRANS(GL_INVALID_ENUM);
		mXD_CASE_TRANS(GL_INVALID_VALUE);
		mXD_CASE_TRANS(GL_INVALID_INDEX);
		mXD_CASE_TRANS(GL_INVALID_OPERATION);
		mXD_CASE_TRANS(GL_STACK_OVERFLOW);
		mXD_CASE_TRANS(GL_OUT_OF_MEMORY);
		mXD_CASE_TRANS(GL_INVALID_FRAMEBUFFER_OPERATION);
		default: mERRORP("Unknown GlInternal error: " << mBRACED(gl_error) );
		}
		return 0x0;
	}
};

struct IGFX::Extra
{
	HGLRC	render_context;
	HDC		draw_context;

xd_functional:
	void
	fDrop()
	{
		render_context = 0;
		draw_context = 0;
	}

xd_interface:
	static Extra* iCreate() { return new Extra; }
	static void iDestroy(Extra* _extra) { delete _extra; }

};

struct nGFX::SHADER::Extra
{
xd_data:
	SHADER::Program curent_shader;

xd_functional:
	void
	fShaderSet(const SHADER::Program& _shader) {
		curent_shader = _shader;
	}

	SHADER::Program&
	fShaderGet() {
		return curent_shader;
	}

xd_interface:
	static
	SHADER::Extra*
	iCreate() {
		return new SHADER::Extra;
	}

	static
	void
	iDestroy(SHADER::Extra* _extra) {
		delete _extra;
	}

};

struct SHADER::Program::Sysspec
{
xd_interface:
	static
	Sysspec* iCreateP() {
		return new Sysspec;
	}

	static
	void iDestroy(Sysspec* _target) {
		delete _target;
	}

xd_functional:
	GLuint fProgramGet() {
		return program;
	}

	void
	fProgramSet(GLuint _value);

xd_data:
	GLuint program;
};

struct RENDER::VertexBuffer5::Sysspec
{
xd_interface:
	Sysspec* iCreapeP() {
		return new Sysspec;
	}
	void iDesproy(Sysspec* _target) {
		delete _target;
	}
};

} }// ns


#endif /* GFX_V2_WIN_XD_GFX_IMPL_H_ */
