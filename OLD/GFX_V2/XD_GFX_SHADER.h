#ifndef _XD_GFX_SHADER_H
#define _XD_GFX_SHADER_H

#include "BASE/XD_F4.h"
#include "BASE/XD_F44.h"

namespace nXD { namespace nGFX {

class SHADER
{
xd_special:
	struct Extra;
	typedef Extra* ExtraT;

xd_open_types:
	struct Program
	{
		str vertex;
		str fragment;

		struct Sysspec;
		Sysspec* sysspec;

		void
		fClear()
		{
			sysspec = 0x0;
			vertex = 0x0;
			fragment = 0x0;
		}
	};

xd_functional:
	SHADER() : m_extra(0x0) {};
	x_result fInitX();
	x_result fCompileX(Program& _result);
	x_result fSetX(const Program& _program);

	x_result fSetValueX(const fl4& _value, str _name);
	x_result fSetValueX(const XD_F44& _value, str _name);
	x_result fSetValueX(const XD_F4& _value, str _name);

	x_result fSetValueX(const fl4& _value, str _name, const Program& _program);
	x_result fSetValueX(const XD_F44& _value, str _name, const Program& _program);
	x_result fSetValueX(const XD_F4& _value, str _name, const Program& _program);

xd_data:
	ExtraT m_extra;
};

} }//XD

#endif // XD_GFX_SHADER_H
