#ifndef _XD_GFX_FRAME_H
#define _XD_GFX_FRAME_H

#include "BASE/CODE.h"
#include "BASE/TYPE.h"

#include "XD_GFX.h"

namespace nXD { namespace nGFX {

class FRAME
{
xd_data:
	IGFX::Extra*		m_extra;
	bln				m_opened;

xd_functional:
	x_result	fInitX(IGFX::Extra* _extra);

	x_result	fBeginX();
	x_result	fEndX();

	x_result	fClearColorX(const XD_COLOR& _color);
	x_result	fClearX(bln _color, bln _stencil, bln _depth);
	x_result	fDrawX();
};

} }

#endif // XD_GFX_FRAME_H
