#ifndef _GFX_RENDER_H
#define _GFX_RENDER_H

namespace nXD { namespace nGFX {


class RENDER
{
xd_open_types:
	struct VertexData5
	{
		fl4 x,y,z,u,v;

		void
		fClear()
		{
			x=y=z=u=v=0;
		}
	};

	struct VertexBuffer5
	{
		struct Sysspec;
		Sysspec* sysspec;

		VertexData5* data;
		szt data_cnt;
		ui2 *idx;
		szt idx_cnt;

		void
		fDrop() {
			data = 0x0;
			idx = 0x0;
			data_cnt = idx_cnt = 0;
			sysspec = 0x0;
		}

		static
		VertexData5* iCreate(szt _cnt);

		static
		void fDestroy(VertexData5* _target);
	};

	x_result fCompileX(VertexBuffer5& _target);
	x_result fSetX(VertexBuffer5& _target);
};

} }// XD

#endif // GFX_RENDER_H
