#ifndef _XD_GFX_H
#define _XD_GFX_H

#include "BASE/LANG.h"
#include "BASE/CODE.h"
#include "BASE/TYPE.h"
#include "BASE/XD_COLOR.h"
#include "BASE/STRINGS/XD_STRING.h"

namespace nXD { namespace nGFX {

class IGFX
{
xd_open_types:
	struct Extra;
};

}}

#include "MODULES/GFX_V2/XD_GFX_SHADER.h"
#include "MODULES/GFX_V2/XD_GFX_SETTINGS.h"
#include "MODULES/GFX_V2/XD_GFX_TEXTURE.h"
#include "MODULES/GFX_V2/XD_GFX_FRAME.h"
#include "MODULES/GFX_V2/XD_GFX_RENDER.h"

namespace nXD { namespace nGFX {

class GFX : public IGFX
{
xd_accessors:
	nGFX::SHADER& fShaderA() { return m_shader; }
	nGFX::RENDER& fRenderA() { return m_render; }
	nGFX::SETTINGS& fSettingsA() {return m_settings; }
	nGFX::TEXTURE& fTextureA();
	nGFX::FRAME& fFrameA();
	IGFX::Extra* fExtraA() {return m_extra;}
xd_functional:
	GFX();
	x_result fVersionX(XD_STRING& _result);
	x_result fInitX(sip _draw_descriptor);
	x_result fDestroyX();

xd_data:
	IGFX::Extra* m_extra;
	nGFX::SHADER m_shader;
	nGFX::RENDER m_render;
	nGFX::SETTINGS m_settings;
	nGFX::TEXTURE m_texture;
	nGFX::FRAME m_frame;
};

}}// XD

#endif // XD_GFX_H
