#include "BASE/CODE.h"
#include "BASE/LANG.h"
#include "Common/App.hpp"

#include "BASE/Lern/InitializerList.hpp"
using namespace nXD;

int
main()
{
	DoAllINeed a();
	mNOTE("Editor started");
	M3_App app;
	M3_App::Init init = {
		mVAR("project name",    "M3 Editor"),
		mVAR("window size", 	nXD::F2(0.8,0.8)),
		mVAR("size delta",  	nXD::I2(0,0)),
		mVAR("pos",             nXD::F2(0.5, 0.5)),
		mVAR("pos delta",       nXD::I2(0,0))
	};
	mXCALL(app.fStartX(init));
	mXCALL(app.fDestroyX());
	mNOTE("Editor shutdown");
	return A_A;
}
