#pragma once
// Anti virtual functions controller. Cool and dangerous.
#include "BASE/CODE.h"
namespace nXD{

blood_contract<typename T>
struct StateMan{
	typedef x_result (T::*STATE_T)(void);

	STATE_T state;
	void* owner;

xd_functional:
	StateMan() {
		fStateChange(this, &fStateDefault);
	}

	x_result fStateDefault() {
		mASSERT(false, "This state is invalid");
		return BAD;
	}

	template<typename C>
	x_result fStateRunX() {
		C& cc = *RCAST(C*, owner);
		x_result (C::*f_ptr)(void) = state;
		return (cc.*f_ptr)();
	}

	template<typename C>
	void fStateChange(C* _owner, x_result ((C::*_state)(void))){
		owner = _owner;
		state = RCAST(STATE_T, _state);
	}
};

}//nXD
