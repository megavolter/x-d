#include "App.hpp"
#include "BASE/CAST.h"
#include "BASE/TYPE.h"
#include "Editor/Usage/SceneTestFrame.hpp"

using namespace nXD;

M3_App::M3_App()
{

}

M3_App::~M3_App()
{

}

x_result
M3_App::sWidgetSetupX(const Init& _init)
{
	si4 x = 100, y = 100, w = 100, h = 100;

	nXD::I2 screen_size;
	mXCALL(OS::iScreenSizeGetX(screen_size));

	w = screen_size.x * _init.wnd_size.x + _init.wnd_size_delta.x;
	h = screen_size.y * _init.wnd_size.y + _init.wnd_size_delta.y;
	x = screen_size.x * _init.wnd_position.x + _init.wnd_offset.x - w/2;
	y = screen_size.y * _init.wnd_position.y + _init.wnd_offset.y - h/2;

	m_data.wdgt = OS::iWdgtCreateP();
	mXCALL(OS::iWdgtInitX(
		mVAR("window name", _init.project_name),
		mVAR("from x", x),
		mVAR("from y", y),
		mVAR("size w", w),
		mVAR("size h", h),
		mVAR(wdgt, m_data.wdgt)
	));

	return A_A;
}

x_result
M3_App::sEnvirontmentSetupX()
{
	m_data.os.fInputKeyListenerA()
		.fMethodSetA(RCAST(OS::www, iEventKey))
		.fObjectSetA(RCAST(void*, this));

	return A_A;
}

x_result
M3_App::sGraphicsSetupX()
{
	sip descriptor = OS::iWdgtDescriptorGet(m_data.wdgt);
	mXD_CHECK(descriptor);
	mXCALL(m_data.gfx.fInitX(descriptor));
	m_data.gfx.fFrameA().fClearColorSetX(XD_COLOR::eBlack);
	return A_A;
}

x_result
M3_App::fStartX(const Init& _init)
{
	m_init = _init;

	mXCALL(sWidgetSetupX(_init));
	mXCALL(sEnvirontmentSetupX());

	mXCALL(OS::iWdgtShowX(mVAR("show", true), m_data.wdgt));
	mXCALL(sGraphicsSetupX());

	mXCALL(sSceneSetupX());

	mCOMMENT("Little infinit loop");
	m_data.play = true;
	while(m_data.play){
		mXCALL(fTickX());
	}

	return A_A;
}

x_result
M3_App::fStopX(str _reason)
{
	m_data.play = false;
	mNOTE("Application terminated. Reason: "<<mBRACED(_reason));
	return A_A;
}

x_result
M3_App::fTickX()
{
	m_data.os.fSyncX();
	mXCALL(fDrawX());
	return A_A;
}

x_result
M3_App::fDrawX()
{
	mXCALL(m_data.gfx.fFrameA().fOpenX());
	mXCALL(m_data.gfx.fFrameA().fClearX(true, true, true));
	mXCALL(m_scene->fDrawX());
	mXCALL(m_data.gfx.fFrameA().fShowX());
	mXCALL(m_data.gfx.fFrameA().fCloseX());
	return A_A;
}

x_result
M3_App::fDestroyX()
{
	mXCALL(OS::iWdgtShowX(mVAR("show", false), m_data.wdgt));
	mXCALL(OS::iWdgtDestroyX(m_data.wdgt));
	return A_A;
}

sip
M3_App::iEventKey(M3_App* _this, OS::INPUT_KEY* _key)
{
	if(_key->type == XDE_INPUT_DOWN && _key->key == XDE_KEY_ESC){
		mXCALL(_this->fStopX("Player termination"));
	}
	return 0x0;
}

x_result
M3_App::sSceneSetupX()
{
	m_scene = SceneTestFrame::iCreateP();
	return m_scene->fInitX(&m_data);
}

