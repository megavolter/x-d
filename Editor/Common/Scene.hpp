#pragma once
#include "BASE/TYPE.h"
#include "BASE/LANG.h"
#include "BASE/CODE.h"
#include "Editor/Common/App.hpp"

namespace nXD{
class Scene
{
xd_shared:
	M3_App::Data* m_data;

xd_functional:
	x_result fInitX(M3_App::Data* _data){
		m_data = _data;
		return fvInitX();
	}

	x_result fDrawX() {
		mXCALL(m_data->rdl.fSyncX());
		return fvDrawX();
	}

private:
	virtual
	x_result fvInitX() = 0;

	virtual
	x_result fvDrawX() = 0;
};
}// nEd
