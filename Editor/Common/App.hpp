#pragma once

#include "BASE/XD_WWW.h"
#include "OS/XD_OS.h"
#include "BASE/XD_F2.h"
#include "BASE/XD_I2.h"
#include "GFX/XD_GFX.h"
#include "RDL/XD_RDL.h"

namespace nXD{
	class Scene;
}

class M3_App
{
xd_open_types:
	struct Init {
		str project_name;
		nXD::F2 wnd_size;
		nXD::I2 wnd_size_delta;
		nXD::F2 wnd_position;
		nXD::I2 wnd_offset;
	};

	struct Data	{
		bln play;
		nXD::OS os;
		nXD::OS::WDGT_T wdgt;
		nXD::GFX gfx;
		nXD::RDL rdl;
	};

xd_data:
	Init 		m_init;
	Data 		m_data;
	nXD::Scene* m_scene;

xd_functional:
	M3_App();
	~M3_App();
	x_result fStartX(const Init& _init);
	x_result fStopX(str _reason);
	x_result fTickX();
	x_result fDrawX();
	x_result fDestroyX();

xd_special:
	x_result sWidgetSetupX(const Init& _init);
	x_result sEnvirontmentSetupX();
	x_result sGraphicsSetupX();
	x_result sSceneSetupX();

xd_interface:
	static sip	iEventKey(M3_App*,nXD::OS::INPUT_KEY* _key);
};
