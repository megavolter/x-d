find_package(OpenGL REQUIRED)
link_directories(${OPENGL_gl_LIBRARY})

ADD_MODULE(BASE)
ADD_MODULE(GFX)
ADD_MODULE(RDL)
ADD_MODULE(GFX_DECODER)
ADD_MODULE(TIME)
ADD_MODULE(OS)

message(MODULES_SRC ${MODULES_SRC} ${MODULES_INC})
message(MODULES_DIR ${MODULES_DIR})

set(PROJECT_SRC_DIR
	${XD_DIR}/Editor/
)

set(PROJECT_SRC
	${PROJECT_SRC_DIR}/main.cpp
	
	${PROJECT_SRC_DIR}/Common/App.hpp
	${PROJECT_SRC_DIR}/Common/App.cpp
	${PROJECT_SRC_DIR}/Common/Scene.cpp
	
	${PROJECT_SRC_DIR}/Usage/SceneTestFrame.hpp
	${PROJECT_SRC_DIR}/Usage/SceneTestFrame.cpp
)

set(CMAKE_BUILD_TYPE Debug)

add_executable(m3_editor 
	${PROJECT_SRC}
	${MODULES_SRC}
	${MODULES_INC}
)

include_directories(${MODULES_DIR})

target_link_libraries(m3_editor 
	${MODULES_LIB}
)