#include "SceneTestFrame.hpp"
#include "BASE/CODE.h"
#include "BASE/XD_FIGURES_3D.h"
#include "BASE/LANG.h"
#include "GFX_DECODER/XD_GFX_DECODER.h"
#include "BASE/XD_MATH3D.h"

using namespace nXD;
SceneTestFrame::SceneTestFrame()
	: Scene(), StateMan()
{
	m_name.fSet("This is a valid name");
	m_load_listener.fMethodSetA(RCAST(XD_WWW::www, iAssetLoaded))
                .fObjectSetA(this);
	m_scene_loaded = false;
	m_object.fDrop();
}

static str res_name[] = {
		"data/t/cube4.png",
		"data/fx/f_5t.glsl",
		"data/fx/v_5.glsl",
		"data/fx/v_5_pos.glsl",
};

sip
SceneTestFrame::iAssetLoaded
(SceneTestFrame* _this, const nXD::RDL::DATA_ON_LOAD* _data)
{
	str asset_name = _data->path;
	mNOTE("Asset "<<mBRACED(asset_name)<<((_data->loaded)?"loaded":"failed"));
	return 0x0;
}

x_result
SceneTestFrame::fStateInitX()
{
	fStateChange(this, &fStateWaitX);
	mCODE_BLOCK("Setup RDL") {
		m_data->rdl.fSyncListenerA().fObjectSetA(this);
		m_data->rdl.fSyncListenerA().fMethodSetA(RCAST(XD_WWW::www, RCAST(XD_WWW::www, iPostInit)));
		for(szt i = 0; i < mVAR("Array size", sizeof(res_name)/sizeof(void*)); ++i)
			m_data->rdl.fDataLoad(res_name[i], m_load_listener);
	};
	mCODE_BLOCK("Set up root transform"){
        nXD::F4 bounds = {
            mVAR("Left", -100),
            mVAR("Right", 100),
            mVAR("Top", 100),
            mVAR("Bottom", -100)
        };
        
        nXD::F2 plane = {
            mVAR("Near", 0.001),
            mVAR("Far", 1000)
        };
        
		m_camera.fOrthoA(bounds, plane);
	};
	return A_A;
}

x_result
SceneTestFrame::fStatePostInitX()
{
	fStateChange(this, &fStateDrawX);
	SceneTestFrame* _this = this;
	nXD::Object& object = _this->m_object;

	mCODE_BLOCK("Shader"); {
		nXD::DATA& vx = _this->m_data->rdl.fDataGetA(res_name[3]);
		nXD::DATA& fx = _this->m_data->rdl.fDataGetA(res_name[1]);

		_this->m_object.program.vertex.fSet(vx.fStrGetP(), vx.fSize());
		_this->m_object.program.fragment.fSet(fx.fStrGetP(), fx.fSize());
		mXCALL(_this->m_data->gfx.fShaderA().fCompileX(_this->m_object.program));
	};

	mCODE_BLOCK("Texture"); {
		nXD::RDL& rdl = _this->m_data->rdl;
		nXD::DATA& tex_data = *rdl.fDataGetP(res_name[0]);

		nXD::BITMAP btmp; btmp.fSetA(tex_data.data, tex_data.data_size);
		mXCALL(nXD::GFX_DECODER::iInitX(btmp));
		mXCALL(nXD::GFX_DECODER::iDecodeX(btmp));

		_this->m_object.texture = *_this->m_data->gfx.fSamplerA().fCreateP(
				mVAR("data ptr", btmp.bmp),
				mVAR("data size", btmp.bmp_size),
				mVAR("img w", btmp.size.x),
				mVAR("img h", btmp.size.y),
				mVAR("pixel format", XDE_PIXEL_r8g8b8a8)
		);
		nXD::GFX_DECODER::iClear(btmp);
	};

	mCODE_BLOCK("Geometry"); {
		_this->m_object.object.fAllocateA(mVAR("vertex", 4), mVAR("index", 6));
		_this->m_object.object.vx_num = nXD::Figures3D::quad_vx_cnt;
		for(szt i = 0; i < nXD::Figures3D::quad_vx_cnt; ++i){
			_this->m_object.object.fVertexA(i).fPositionSetA(
					nXD::Figures3D::quad_pos_n[i][0] * object.texture.size.x * 0.5,
					nXD::Figures3D::quad_pos_n[i][1] * object.texture.size.y * 0.5,
					nXD::Figures3D::quad_pos_n[i][2]
			).fTexelSetA(
				nXD::Figures3D::quad_tex[i][0],
				nXD::Figures3D::quad_tex[i][1]
			);
		}
		ui2 ids[] = {0,2,1,0,3,2,};
		_this->m_object.object.fIndecesCpyA(
				nXD::Figures3D::quad_ix,
				//ids,
				nXD::Figures3D::quad_ix_cnt
		);
		mXCALL(_this->m_data->gfx.fRenderA().fCompileX(_this->m_object.object));
	};

	mCODE_BLOCK("Transform"); {
		fl4 fov = 45.f;
		nXD::I2 wdgt_size = nXD::OS::iWdgtSizeA(m_data->wdgt);
		fl4 aspect = wdgt_size.x / wdgt_size.y;
		m_camera.fPerspectiveEX(1, aspect,-1, 100, -1);
	};

	_this->m_scene_loaded = true;
	return 0x0;
}

x_result
SceneTestFrame::fStateDrawX()
{
	if(m_scene_loaded == true){
		mXCALL(m_data->gfx.fRenderA().fSetGeometryX(m_object.object));
		mXCALL(m_data->gfx.fShaderA().fSetX(m_object.program));
		mXCALL(m_data->gfx.fSamplerA().fSetX(m_object.texture, 0));
		mXCALL(m_data->gfx.fShaderA().fVarX(m_camera, "_transform", 0));
		mXCALL(m_data->gfx.fRenderA().fRasterizeX());
		m_object.transform.fRotationSetA(0.1);
	}
	return A_A;
}
