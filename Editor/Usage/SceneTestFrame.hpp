#pragma once
#include "Editor/Common/Scene.hpp"
#include "GFX/Object.h"
#include "BASE/LANG.h"
#include "GFX/V5.h"
#include "Editor/Common/StateMan.hpp"

class SceneTestFrame : public nXD::Scene , public nXD::StateMan<SceneTestFrame>
{
xd_data:
	XD_STRING		m_name;
	nXD::Object		m_object;
	nXD::XD_WWW		m_load_listener;
	bln				m_scene_loaded;
	nXD::TransformF	m_root_transform;
	nXD::F44		m_camera;

xd_interface:
	SceneTestFrame();

	static SceneTestFrame*
	iCreateP(){
		return new SceneTestFrame;
	}

	static void
	iDestroy(SceneTestFrame* _target) {
		::delete _target;
	}

xd_func_block("States"):
	x_result fStateInitX();
	x_result fStatePostInitX();
	x_result fStateDrawX();
	x_result fStateWaitX() {return nXD::OK;}

xd_interface:
	static sip
	iPostInit(SceneTestFrame* _this, void*) {
		_this->fStateChange(_this, &fStatePostInitX);
		return 0x0;
	}

	static sip
	iAssetLoaded(SceneTestFrame* _, const nXD::RDL::DATA_ON_LOAD* _data);

xd_func_block("override"):
	virtual x_result
	fvInitX() xd_override {
		fStateChange(this, &fStateInitX);
		return nXD::A_A;
	}

	virtual x_result
	fvDrawX() xd_override {
		return fStateRunX<SceneTestFrame>();
	}
};
